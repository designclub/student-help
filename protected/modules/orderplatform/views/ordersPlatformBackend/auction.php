<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Заказы') => ['/orderplatform/ordersPlatformBackend/list'],
    Yii::t('OrderplatformModule.orderplatform', 'Аукцион заказов'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Аукцион заказов');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Аукцион заказов'); ?>
    </h1>
</div>


<div class="row list-orders">
		<?php $this->widget(
			'bootstrap.widgets.TbListView',
			array(
				'dataProvider'  => $model->auction(),
				'template' => '{items}',
				'viewData' => ['dialog'=>$dialog],
				'itemView'      => '_order-auction',
				'emptyText'		=> 'В настоящий момент доступных заказов нет'
			)
		); ?>
</div>

<?php $this->widget('application.modules.orderplatform.widgets.DialogWidget'); ?>
<?php $this->widget('application.modules.orderplatform.widgets.OrderRelyWidget'); ?>