<?php

/**
 * This is the model class for table "{{user_balance}}".
 *
 * The followings are the available columns in table '{{user_balance}}':
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $date
 * @property string $number_bill
 * @property string $money
 * @property string $balance
 * @property string $comment
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $operation
 * @property integer $status
 */
class UserBalance extends yupe\models\YModel
{
	
	
	const OPERATION_PAY_ORDER = 1;
	
	const OPERATION_REPLENISHMENT_BALANCE = 2;
		
	const OPERATION_TRANSFER_MONEY = 3;
	
	const OPERATION_RESERVE_ORDER = 4;
	
	const OPERATION_RETURN_RESERVE_ORDER = 5;
	
	const STATUS_ERROR = 0;
	
	const STATUS_OK = 1;
	
	const STATUS_DURING = 2;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_balance}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('money, balance, comment, operation, status', 'required'),
			array('user_id, order_id, operation, status', 'numerical', 'integerOnly'=>true),
			array('number_bill', 'length', 'max'=>255),
			array('money, balance', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_create, date_update, date, number_bill, money, balance, comment, user_id, order_id, operation, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
	
	public function beforeSave(){
		
        $this->date_update = new CDbExpression('NOW()');
        $this->date = new CDbExpression('NOW()');

        if ($this->getIsNewRecord()) {
            $this->date_create = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_create' => 'Дата добавления',
			'date_update' => 'Дата изменения',
			'date' => 'Дата',
			'number_bill' => 'Номер счета',
			'money' => 'Сумма',
			'balance' => 'Баланс',
			'comment' => 'Комментарий',
			'user_id' => 'Пользователь',
			'order_id' => 'Заказ',
			'operation' => 'Операция',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('date_update',$this->date_update,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('number_bill',$this->number_bill,true);
		$criteria->compare('money',$this->money,true);
		$criteria->compare('balance',$this->balance,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('operation',$this->operation);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	    /**
     * @return array
     */
    public function getOperationsList()
    {
        return [
            self::OPERATION_RESERVE_ORDER => 'Зарезервировано за заказ',
            self::OPERATION_TRANSFER_MONEY => 'Начислено за заказ',
            self::OPERATION_REPLENISHMENT_BALANCE => 'Пополнение баланса',
            self::OPERATION_PAY_ORDER => 'Оплачено за заказ'
        ];
    }

    /**
     * @return mixed|string
     */
    public function getOperation()
    {
        $data = $this->getOperationsList();

        return isset($data[$this->operation]) ? $data[$this->operation] : $this->comment;
    }
	
	public function getBalanceUser($user){
		$criteria=new CDbCriteria;
		$criteria->condition = 'user_id = :user AND status = :status';
		$criteria->params = [':user'=>$user, ':status' => self::STATUS_OK];
		$criteria->order = 'date DESC';
		$criteria->limit = 1;
		$balance = self::model()->find($criteria);
		
		if (empty($balance->balance))
			return 0;
		else
			return($balance->balance);
	}
	
	public function getBalance($user = null){
		if (is_null($user))
			$user = Yii::app()->getUser()->getId();
		else
			$user = $user;
		
		$criteria=new CDbCriteria;
		$criteria->condition = 'user_id = :user AND status = :status';
		$criteria->params = [':user'=>$user, ':status' => self::STATUS_OK];
		$criteria->order = 'date DESC';
		$criteria->limit = 1;
		$balance = self::model()->find($criteria);
		
		if (empty($balance->balance))
			return 0;
		else
			return($balance->balance);
	}
	
	public function addBalance($sumBalance){
		
		$currentBalance = $this->getBalance(Yii::app()->getUser()->getId());
		
		$balance = new self;
		$balance->money = $sumBalance;
		$balance->balance = $currentBalance + $sumBalance;
		$balance->comment = 'Пополнение баланса';
		$balance->user_id = Yii::app()->getUser()->getId();
		$balance->operation = self::OPERATION_REPLENISHMENT_BALANCE;
		$balance->status = self::STATUS_DURING;
		$balance->save();
		
		return($balance);
	}
	
	//резервирование баланса для заказа
	public function reserveBalanceOrder($id_order, $price){
		
		$currentBalance = $this->getBalance(Yii::app()->getUser()->getId());
		
		$balance = new self;
		$balance->money = $price;
		$balance->balance = $currentBalance - $price;
		$balance->order_id = $id_order;
		$balance->comment = 'Зарезервировано на заказ № '.$id_order;
		$balance->user_id = Yii::app()->getUser()->getId();
		$balance->operation = self::OPERATION_RESERVE_ORDER;
		$balance->status = self::STATUS_OK;
		$balance->save();

		return($balance);
	}
	
	public function transferMoneyAuthor(OrdersPlatform $order){

		$money = $order->cost - $order->cost * ($this->getCommission($order->author_id)/100);

		$currentBalance = $this->getBalance($order->author_id);
		
		$balance = new self;
		$balance->money = $money;
		$balance->balance = $currentBalance + $money;
		$balance->order_id = $order->id;
		$balance->comment = 'Начислено на заказ № '.$order->id;
		$balance->user_id = $order->author_id;
		$balance->operation = self::OPERATION_TRANSFER_MONEY;
		$balance->status = self::STATUS_OK;
		$balance->save();

		return($balance);
	}
	
	public function acceptMoneyClient($id_client, $id_order){
		
		$criteria=new CDbCriteria;
		$criteria->condition = 'user_id = :user AND order_id = :order_id';
		$criteria->params = [':user'=>$id_client, ':order_id' => $id_order];
		$criteria->limit = 1;
		
		$balance = self::model()->find($criteria);
		$balance->operation = self::OPERATION_PAY_ORDER;
		$balance->status = self::STATUS_OK;
		$balance->save();

		return($balance);
		
	}
	
	public function historyBalances(){
		$criteria=new CDbCriteria;
		$criteria->condition = 'user_id = :user AND status = :success AND money <> 0';
		$criteria->params = [':user'=>Yii::app()->getUser()->getId(), ':success' => self::STATUS_OK];
		$criteria->order = 'date_create DESC';
	 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
                'pageSize'=>10,
            ),
		));
		
		return($history);
	}
	
	public function getCommission($id_user = null){
		
		if (is_null($id_user))
			$id = Yii::app()->getUser()->getId();
		else
			$id = $id_user;
		
		$user = User::model()->findByPk($id);
 
		switch($user->type_account){
				
			//client
			case 1:
				$comission = CommissionClients::model()->getCommissionClient($id);
			break;
			
			//author
			case 2:
				$comission = CommissionAuthor::model()->getCommissionAuthor($id);
			break;
				
			//default
			default:
				$comission = 100;
			break;
		}
		
		return $comission;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserBalance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
