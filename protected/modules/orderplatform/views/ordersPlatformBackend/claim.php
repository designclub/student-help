<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OrderplatformModule.orderplatform', 'Мои заказы') => ['/orderplatform/ordersPlatformBackend/list'],
    Yii::t('OrderplatformModule.orderplatform', 'Претензия по заказу'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Претензия по заказу');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Претензия по заказу № ') . $order; ?>
    </h1>
</div>

<?=  $this->renderPartial('_claim', ['model' => $model]); ?>