<?php
 
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Претензии по заказам') => ['/orderplatform/admin/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Решение по претензии'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Претензии по заказам - решение');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Решение по претензии'); ?>
    </h1>
</div>
