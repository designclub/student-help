<?php $this->beginContent($this->yupe->getBackendLayoutAlias("main")); ?>

<div class="row">
	<div class="col-sm-12">
		<?php
		if ( count( $this->breadcrumbs ) ) {
			$this->widget(
				'bootstrap.widgets.TbBreadcrumbs', [
					'homeLink' => CHtml::link( Yii::t( 'YupeModule.yupe', 'Home' ), [ '/yupe/backend/index' ] ),
					'links' => $this->breadcrumbs,
				]
			);
		}
		?>
	</div>
	<!-- breadcrumbs -->
	<?php $this->widget('bootstrap.widgets.TbAlert'); ?>
</div>

 
<div class="row">

	<?php if(Yii::app()->user->getProfile()->type_account == 1 || Yii::app()->user->getProfile()->type_account == 2): ?>
		<div class="content-site user-panel">
			<?php $this->renderPartial('application.modules.yupe.views.layouts._user', ['content' => $content]); ?>
		</div>
	<?php else: ?>

		<div id="content">
			<div class="<?= $this->hideSidebar ? 'col-sm-12' : 'col-sm-12 col-md-9 col-lg-10'; ?>">
				<?= $content; ?>
			</div>
		</div>

	<?php endif; ?>

</div>
 
<?php $this->endContent(); ?>