<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Программы проверки') => ['/orderplatform/uniqueCheckBackend/index'],
    $model->name_check_unique => ['/orderplatform/uniqueCheckBackend/view', 'id' => $model->id],
    Yii::t('OrderplatformModule.orderplatform', 'Редактирование'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Программы проверки - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Программами проверки'), 'url' => ['/orderplatform/uniqueCheckBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Программу проверки'), 'url' => ['/orderplatform/uniqueCheckBackend/create']],
    ['label' => Yii::t('OrderplatformModule.orderplatform', 'Программа проверки') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Редактирование Программы проверки'), 'url' => [
        '/orderplatform/uniqueCheckBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Просмотреть Программу проверки'), 'url' => [
        '/orderplatform/uniqueCheckBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Удалить Программу проверки'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/orderplatform/uniqueCheckBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('OrderplatformModule.orderplatform', 'Вы уверены, что хотите удалить Программу проверки?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Редактирование') . ' ' . Yii::t('OrderplatformModule.orderplatform', 'Программы проверки'); ?>        <br/>
        <small>&laquo;<?=  $model->name_check_unique; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>