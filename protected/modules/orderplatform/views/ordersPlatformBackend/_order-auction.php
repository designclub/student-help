<div class="block-order">
	<div class="row">
		<div class="col-lg-12">
			<h5><?= $data->title; ?></h5>
		</div>
	</div>
	<div class="row data-order">
		<div class="col-lg-3">
			<?= $data->subject->name_subject; ?>
		</div>
		<div class="col-lg-3">
			<?= $data->type->name_type; ?>
		</div>
		<div class="col-lg-6">
			Бюджет: <?= round($data->budget, 0); ?> руб.
		</div>
	</div>

	<div class="row options-order">
		<div class="col-lg-3">
			<i class="fa fa-clock-o"></i> 
			Срок: <?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->target_date); ?>
		</div>		
		
		<div class="col-lg-4">
			<i class="fa fa-info" aria-hidden="true"></i>
			Уникальность: <?= $data->uniqueness; ?>% (<?= $data->uniqCheck->name_check_unique; ?>)
		</div>		
		<div class="col-lg-4">
			<i class="fa fa-user" aria-hidden="true"></i>
			Заказчик: <?= $data->client->nick_name; ?>
		</div>
		<div class="col-lg-1">
			<?= CHtml::link('<i class="fa fa-comments-o" aria-hidden="true"></i>', '#', ['class'=>'link-dialog', 'data-user-to'=>$data->client->id, 'data-order' => $data->id, 'data-toggle'=>'modal', 'data-target'=>'#dialogModal', 'link'=>Yii::app()->createAbsoluteUrl('/backend/orderplatform/dialog/dialogOrder')]); ?>
		</div>
	</div>
	
	<div class="row text-order">
		<div class="col-lg-12">
			<?= CHtml::link('Описание заказа', '#', ['class'=>'link-more']);?>
			<div class="more-text hide">
				<?= $data->text; ?>
				<div class="files-order">
					<?php if (count($data->filesForOrders()) > 0): ?>
						<?php foreach($data->filesForOrders() as $file): ?>
						<?= CHtml::link($file->name, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->fileName, ['target' => '_blank', 'class'=>'btn']); ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
	<?php $rate = RatesOrder::model()->getRateAuthor($data->id); ?>
	
	<div class="row buttons-order rates-block rates-block-order">
		<?php if($data->status->id == 5): ?>
			<div class="col-lg-6">
				<?= CHtml::link(($rate == false) ? 'Сделать ставку' : 'Изменить ставку', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/rely', ['id'=>$data->id]), ['class'=>'btn btn-success link-rely', 'data-order'=>$data->id, 'data-toggle'=>'modal', 'data-target'=>'#orderRelyModal']); ?>
			</div>
			<?php if (count($rate) > 0 && $rate != false): ?>
				<div class="col-lg-6">
					 <h3>Ваша ставка: <span id="order-id-<?= $data->id; ?>"><?= $rate->rate; ?></span> руб.</h3>
					 <label class="label label-margin label-info">Ваш заработок составит: <?=round($rate->rate - $rate->rate * UserBalance::model()->getCommission()/100, 2); ?> руб.</label>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>

</div>