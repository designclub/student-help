<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

	<link href="https://fonts.googleapis.com/css?family=Arimo:400,700|Ubuntu:300,400,500,700&amp;subset=cyrillic-ext" rel="stylesheet">

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <?php

		Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.css');

		Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/bootstrap-notify.js');
		Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.li-translit.js');
    ?>

    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>


    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>
<?php //echo $content; ?>
 <div class="container-site">

 	<div class="header">
 		<div class="content-site">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xs-12 logo">
					<?php if (Yii::app()->hasModule('contentblock')): ?>
						<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo']); ?>
					<?php endif; ?>
				</div>
				<div class="col-sm-2 col-xs-12 link-top">
					<?php if (Yii::app()->hasModule('contentblock')): ?>
						<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'link-top']); ?>
					<?php endif; ?>
				</div>
<!--				<div class="col-sm-2 col-xs-12 link-top">
					Ваш город <span>Оренбург</span>
				</div>-->
				<div class="col-sm-3 col-xs-12 phone">
					<?php if (Yii::app()->hasModule('contentblock')): ?>
						<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'phone']); ?>
					<?php endif; ?>
				</div>
				<div class="col-sm-3 col-xs-12 login">
					<?php if (true === Yii::app()->getUser()->getIsGuest()): ?>
						<?= CHtml::link('Регистрация', CHtml::normalizeUrl(['/user/account/registration'])); ?> | <?= CHtml::link('Войти', CHtml::normalizeUrl(['/user/account/login'])); ?>
					<?php else: ?>
						<?= CHtml::link('Личный кабинет', CHtml::normalizeUrl(['/backend/index'])); ?> | <?= CHtml::link('Выход', CHtml::normalizeUrl(['/user/account/logout'])); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="menu-header">
					<?php if (Yii::app()->hasModule('menu')): ?>
						<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'head-menu','layout' =>'menu']); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-sm-6 col-sm-offset-6 head-text">
				<?php if (Yii::app()->hasModule('contentblock')): ?>
					<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'head-text']); ?>
				<?php endif; ?>
			</div>
		</div>
 	</div><!-- end header -->

 	<div class="statistic-panel hidden-xs">
 		<div class="content-site">
			<div class="row">
				<div class="col-sm-3 col-sm-offset-3 block-item">
					<p><label>4.9</label>Средняя оценка нашей компании</p>
				</div>
				<div class="col-sm-3 block-item">
					<p><label>80751</label>Оказанных<br/>консультаций</p>
				</div>
				<div class="col-sm-3 block-item">
					<p><label>100204</label>Довольных<br/>клиента</p>
				</div>
			</div>
 		</div>
 	</div><!-- end statistic-panel -->

 	<div class="slider-block">
 		<div class="content-site">
			<?php if (Yii::app()->hasModule('contentblock')): ?>
				<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'order-title']); ?>
			<?php endif; ?>
		</div>
 	</div><!-- end slider-works -->

 	<div class="how-work">
 		<div class="content-site">
			<?php if (Yii::app()->hasModule('contentblock')): ?>
				<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'how-works']); ?>
			<?php endif; ?>
			<div class="block-works">
				<?php if (Yii::app()->hasModule('news')): ?>
					<?php $this->widget("application.modules.news.widgets.LastNewsWidget", ['categories'=>1, 'view'=>'work']); ?>
				<?php endif; ?>
			</div>
 		</div>
 	</div><!-- end how-works -->

  	<div class="slider-block">
  		<div class="content-site">
			<?php if (Yii::app()->hasModule('contentblock')): ?>
				<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'author-title']); ?>
			<?php endif; ?>
		</div>
 	</div><!-- end slider-works -->

	<div class="comment-block">
		<div class="content-site">
			<?php if (Yii::app()->hasModule('news')): ?>
				<?php $this->widget("application.modules.news.widgets.CarouselNewsWidget", ['categories'=>2, 'view'=>'reviews']);?>
			<?php endif; ?>
		</div>
	</div><!-- end comment-works -->

	<div class="pluses-block">
		<div class="content-site">
			<?php if (Yii::app()->hasModule('contentblock')): ?>
				<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'pluses']); ?>
			<?php endif; ?>
			<div class="pluses">
				<?php if (Yii::app()->hasModule('news')): ?>
					<?php $this->widget("application.modules.news.widgets.LastNewsWidget", ['categories'=>3, 'view'=>'pluses']); ?>
				<?php endif; ?>
			</div>
		</div>
	</div><!-- end comment-works -->

	<div class="bottom-block hidden-xs">
		<div class="content-site">
			<div class="row">
				<div class="col-sm-3 b-menu">
					<?php if (Yii::app()->hasModule('menu')): ?>
						<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'left-bottom-menu', 'layout' => 'menu']); ?>
					<?php endif; ?>
				</div>
				<div class="col-sm-3 b-menu">
					<?php if (Yii::app()->hasModule('menu')): ?>
						<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'middle-bottom-menu', 'layout' => 'menu']); ?>
					<?php endif; ?>
				</div>
				<div class="col-sm-2 b-menu">
					<?php if (Yii::app()->hasModule('menu')): ?>
						<?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'right-bottom-menu', 'layout' => 'menu']); ?>
					<?php endif; ?>
				</div>
				<div class="col-sm-4">

				</div>
			</div>
		</div>
	</div><!-- end bottom-works -->

 	<?php $this->renderPartial('//layouts/_footer'); ?>

 </div><!-- end container-site -->

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

</body>
</html>