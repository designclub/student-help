<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Мои заказы') => ['/orderplatform/ordersPlatformBackend/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Управление заказами'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Мои заказы');

?>
<div class="row page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Мои заказы'); ?>
    </h1>
</div>

<div class="row list-orders">
		<?php $this->widget(
			'bootstrap.widgets.TbListView',
			array(
				'dataProvider'  => $model->search(),
				'template' => '{items}',
				'itemView'      => '_order',
				'emptyText'		=> 'В настоящий момент у вас нет созданных заказов'
			)
		); ?>
</div>
