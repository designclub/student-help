<?php
/**
 * Файл настроек для модуля orderplatform
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2018 amyLabs && Yupe! team
 * @package yupe.modules.orderplatform.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.orderplatform.OrderplatformModule',
    ],
    'import'    => [
        'application.modules.orderplatform.OrderplatformModule',
        'application.modules.orderplatform.models.*',
        'application.modules.orderplatform.components.*',
        'application.modules.user.models.*',
	],
     'component' => [
		'tinkoffPayment' => [
			'class' => 'application.modules.orderplatform.components.TinkoffPayment',
			'api_url' => 'https://securepay.tinkoff.ru/v2/',
			'terminalKey' => '1526033949376DEMO',
			'secretKey' => 'p95y1kohimkwovyw',
			//'terminalKey' => '1526033949376',
			//*'secretKey' => 'b4tca6uahkpkgnfx',
		],
		'sberbankPayment' =>[
			'class' => 'application.modules.orderplatform.components.SberbankPayment',
			'userName' 	=> 'student-api',
			'password' 	=> 'student', //тестовый пароль
			//'password' 	=> '', //боевой пароль
			'returnUrl' 	=> 'backend/orderplatform/userBalance/successPayment',
			'failUrl' 		=> 'backend/orderplatform/userBalance/failPayment',
			'apiUrl'       => 'https://3dsec.sberbank.ru/payment', //тестовый режим
			//'apiUrl'       => 'https://securepayments.sberbank.ru/payment', //боевой режим
		],
		'platformRepository' => [
            'class' => 'application.modules.orderplatform.components.PlatformRepository'
        ],
		'eventManager' => [
				'class' => 'yupe\components\EventManager',
				'events' => [
					'platform.order.rely' => [
						['OrderListener', 'onOrderRely'],
					],    
					'platform.order.apply.author' => [
						['OrderListener', 'onOrderAuthorApply'],
					],     
					'platform.order.confirm.author' => [
						['OrderListener', 'onOrderAuthorConfirm'],
					],    
					'platform.order.cancel.author' => [
						['OrderListener', 'onOrderAuthorCancel'],
					],				
					'platform.order.check' => [
						['OrderListener', 'onOrderCheck'],
					],				
					'platform.order.correction' => [
						['OrderListener', 'onOrderCorrection'],
					],
					'platform.order.apply' => [
						['OrderListener', 'onOrderApply'],
					],
					'platform.order.dispute' => [
						['OrderListener', 'onOrderDispute'],
					],
					'platform.order.delay' => [
						['OrderListener', 'onOrderDelay'],
					],
				],
			],
	],
    'rules'     => [
        '/orderplatform' => 'orderplatform/orderplatform/index',
    ],
];