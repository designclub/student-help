<?php
/**
 * Отображение для виджета YAdminPanel:
 *
 * @category YupeView
 * @package  yupe
 * @author   AKulikov <tuxuls@gmail.com>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.1
 * @link     http://yupe.ru
 *
 **/
$mainAssets = Yii::app()->getAssetManager()->publish(
    Yii::getPathOfAlias('application.modules.yupe.views.assets'),  false, -1, YII_DEBUG
);

$isAdmin = Yii::app()->getUser()->checkAccess('admin');
$canUpdateUser = Yii::app()->getUser()->checkAccess('User.UserBackend.Update');

$user = User::model()->findByPk(Yii::app()->getUser()->getId());

foreach ($modules as &$item) {
    $item['linkOptions'] = ['title' => $item['label']];
    $item['label'] = CHtml::tag('span', ['class' => 'hidden-sm'], $item['label']);
}

$this->widget(
    'bootstrap.widgets.TbNavbar',
    [
        'fluid'    => true,
        'fixed'    => 'top',
        'brand'    => CHtml::image(
            $mainAssets . '/img/logo-account.png',
            CHtml::encode(Yii::app()->name),
            [
                'width'  => '160',
                'height' => '50',
                'title'  => CHtml::encode(Yii::app()->name),
            ]
        ),
        'brandUrl' => CHtml::normalizeUrl(["/yupe/backend/index"]),
		'htmlOptions'    => [ 'class' => (Yii::app()->user->getProfile()->type_account == 1 ||  Yii::app()->user->getProfile()->type_account == 2) ? 'menu-navbar' : 'menu-navbar menu-navbar-admin' ],
        'items'    => [
            [
                'class' => 'bootstrap.widgets.TbMenu',
                'type'  => 'navbar',
                'encodeLabel' => false,
                'items' =>[
					[
						'icon' => 'fa fa-play-circle',
						'label' => 'Как это работает',
						'url' => Yii::app()->createAbsoluteUrl('/user/user/index'),
						'visible' => (Yii::app()->user->getProfile()->type_account == 1 ||  Yii::app()->user->getProfile()->type_account == 2) ? true : false,
						'linkOptions' => ['class'=>'howwork']
					],
					[
						'label' => 'Заказать работу',
						'url' => Yii::app()->createAbsoluteUrl('/backend/orderplatform/ordersPlatform/create'),
						'visible' => Yii::app()->user->getProfile()->type_account == 1 ? true : false,
						'linkOptions' => ['class'=>'orderLink'],
					],					
					[
						'label' => 'Найти заказ',
						'url' => Yii::app()->createAbsoluteUrl('/backend/orderplatform/ordersPlatform/auction'),
						'visible' => Yii::app()->user->getProfile()->type_account == 2 ? true : false,
						'htmlOptions' => ['class'=>'orderLink']
					],
					[
						'label' => 'Мои заказы',
						'url' => Yii::app()->createAbsoluteUrl('/backend/orderplatform/ordersPlatform/list'),
						'visible' => (Yii::app()->user->getProfile()->type_account == 1) ? true : false,
						'htmlOptions' => ['class'=>'order']
					]
				]
            ],
/*			'<form class="navbar-search pull-left" action=""><input type="text" class="search-query" placeholder="Поиск готовых работ"></form>',*/
			[
                'class' => 'bootstrap.widgets.TbMenu',
                'type'  => 'navbar',
                'encodeLabel' => false,
                'items' => (Yii::app()->user->getProfile()->type_account == 2 || Yii::app()->user->getProfile()->type_account == 1) ? [] : $modules
            ],
            [
                'class'       => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => ['class' => 'navbar-right visible-xs hidden-sm hidden-md visible-lg header-panel'],
                'type'        => 'navbar',
                'encodeLabel' => false,
                'items'       => array_merge(
                    [
                        /*[
                            'icon' => 'fa fa-fw fa-question-circle',
                            'label' => CHtml::tag(
                                    'span',
                                    ['class' => 'hidden-sm hidden-md hidden-lg'],
                                    Yii::t('YupeModule.yupe', 'Help')
                                ),
                            'url' => CHtml::normalizeUrl(['/yupe/backend/help']),
                            'items' => [
                                [
                                    'icon' => 'fa fa-fw fa-globe',
                                    'label' => Yii::t('YupeModule.yupe', 'Official site'),
                                    'url' => 'http://yupe.ru?from=help',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-book',
                                    'label' => Yii::t('YupeModule.yupe', 'Official docs'),
                                    'url' => 'http://docs.yupe.ru/?from=help',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-th-large',
                                    'label' => Yii::t('YupeModule.yupe', 'Additional modules'),
                                    'url' => 'http://yupe.ru/store?from=help',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-comment',
                                    'label' => Yii::t('YupeModule.yupe', 'Forum'),
                                    'url' => 'http://talk.yupe.ru/?from=help',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-comment',
                                    'label' => Yii::t('YupeModule.yupe', 'Chat'),
                                    'url' => 'http://gitter.im/yupe/yupe',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-github',
                                    'label' => Yii::t('YupeModule.yupe', 'Community on github'),
                                    'url' => 'https://github.com/yupe/yupe',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-thumbs-up',
                                    'label' => Yii::t('YupeModule.yupe', 'Order development and support'),
                                    'url' => 'http://yupe.ru/service/development?from=help-support',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-warning',
                                    'label' => Yii::t('YupeModule.yupe', 'Report a bug'),
                                    'url' => 'http://yupe.ru/contacts?from=panel',
                                    'linkOptions' => ['target' => '_blank'],
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-question-circle',
                                    'label' => Yii::t('YupeModule.yupe', 'About Yupe!'),
                                    'url' => ['/yupe/backend/help'],
                                ],
                            ]
                        ],
                        [
                            'icon' => 'fa fa-fw fa-home',
                            'label' => CHtml::tag(
                                    'span',
                                    ['class' => 'hidden-sm hidden-md hidden-lg'],
                                    Yii::t('YupeModule.yupe', 'Go home')
                                ),
                            'url' => Yii::app()->createAbsoluteUrl('/')
                        ],*/
                        [
                            'label' =>CHtml::image(Yii::app()->user->getProfile()->getAvatar(), '', ['class'=>'avatar-menu']),
                            'items' => [
                                [
                                    'icon' => 'fa fa-fw fa-cog',
                                    'label' => Yii::t('YupeModule.yupe', 'Profile'),
                                    'url' => ($isAdmin || $canUpdateUser) ?
                                        CHtml::normalizeUrl(
                                            (['/user/userBackend/update', 'id' => Yii::app()->getUser()->getId()])
                                        ) 
                                        : 
                                        Yii::app()->createAbsoluteUrl('/user/profile/profile'),
                                ],
                                [
                                    'icon' => 'fa fa-fw fa-power-off',
                                    'label' => Yii::t('YupeModule.yupe', 'Exit'),
                                    'url' => CHtml::normalizeUrl(['/user/account/logout']),
                                ],
                            ],
                        ],
                    ]
                ),
            ],
	[
                'class'       => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => ['class' => 'navbar-right visible-xs hidden-sm hidden-md visible-lg navbar-balance'],
                'type'        => 'navbar',
                'encodeLabel' => false,
                'items'       => [
					[
						'label' => 'Ваш баланс: ' .round(UserBalance::model()->getBalanceUser(Yii::app()->getUser()->getId()), 0) . ' руб.',
						'url' => '#',
						'visible' => Yii::app()->user->getProfile()->type_account != 0 ? true : false,
						'linkOptions' => ['class' => 'btn btn-balance'] 
					],					
					[
						'label' => 'Пополнить',
						'url' => Yii::app()->createUrl('/orderplatform/userBalanceBackend/addBalance'),
						'visible' => Yii::app()->user->getProfile()->type_account != 0 &&  Yii::app()->user->getProfile()->type_account == 1 ? true : false,
						'linkOptions' => ['class' => 'btn btn-addbalance orderLink'] 
					],
					[
						'label' => 'Вывести деньги',
						'url' => Yii::app()->createUrl('/orderplatform/userBalanceBackend/returnBalance'),
						'visible' => Yii::app()->user->getProfile()->type_account != 0 &&  Yii::app()->user->getProfile()->type_account == 2 ? true : false,
						'linkOptions' => ['class' => 'btn btn-addbalance orderLink'] 
					],					
				]
            ],
        ],
    ]
); ?>

<script type="text/javascript">
    $(document).ready(function () {
        var url = window.location.href;
        $('.navbar .nav li').removeClass('active');
        $('.nav a').filter(function () {
            return this.getAttribute("href") != '#' && this.href == url;
        }).parents('li').addClass('active');
    });
</script>
