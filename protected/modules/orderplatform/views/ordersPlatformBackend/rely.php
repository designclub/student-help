﻿	<div class="row text-order">
		<div class="col-lg-12">
			<?= CHtml::link('Список ставок', '#', ['class'=>'link-more-rely']);?>
			<div class="more-text-rely">
				<div class="row">
					<div class="col-lg-4">
						Дата ставки: <?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->dialog->create_date); ?>
					</div>
					<div class="col-lg-4">
						<div class="row">
							<?php if ($user->type_account == 1): ?>	
								<div class="col-lg-12">
									Автор: <?= $data->dialog->user->nick_name; ?>
								</div>
					 		<?php endif; ?>
					 		<div class="col-lg-12">
								Ставка: <?= $data->dialog->budget; ?> руб.
							</div>
						</div>
					</div>
					<?php if ($user->type_account == 1): ?>			
					<div class="col-lg-4">
						<?= CHtml::link('Принять ставку', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/confirm', ['id'=>$data->id, 'author'=>$data->dialog->user_id]), ['class'=>'btn btn-success']); ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>	