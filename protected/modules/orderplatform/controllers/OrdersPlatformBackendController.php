<?php
/**
 * Класс OrdersPlatformBackendController:
 *
 *   @category Yupe\yupe\components\controllers\BackController
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class OrdersPlatformBackendController extends\ yupe\ components\ controllers\ BackController {

	public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['auction'], 'roles' => ['Orderplatform.OrderplatformBackend.Auction']],
            ['allow', 'actions' => ['index'], 'roles' => ['Orderplatform.OrderplatformBackend.Index']],
            ['allow', 'actions' => ['create'], 'roles' => ['Orderplatform.OrderplatformBackend.Create']],
            ['allow', 'actions' => ['delete'], 'roles' => ['Orderplatform.OrderplatformBackend.Delete']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Orderplatform.OrderplatformBackend.Update']],
            ['allow', 'actions' => ['list'], 'roles' => ['Orderplatform.OrderplatformBackend.List']],
            ['allow', 'actions' => ['toAuction'], 'roles' => ['Orderplatform.OrderplatformBackend.ToAuction']],
            ['allow', 'actions' => ['rely'], 'roles' => ['Orderplatform.OrderplatformBackend.Rely']],
            ['allow', 'actions' => ['relyAccept'], 'roles' => ['Orderplatform.OrderplatformBackend.RelyAccept']],
            ['allow', 'actions' => ['confirmOrder'], 'roles' => ['Orderplatform.OrderplatformBackend.ConfirmOrder']],
            ['allow', 'actions' => ['cancelOrder'], 'roles' => ['Orderplatform.OrderplatformBackend.CancelOrder']],
            ['allow', 'actions' => ['makeOrder'], 'roles' => ['Orderplatform.OrderplatformBackend.MakeOrder']],
            ['allow', 'actions' => ['acceptOrder'], 'roles' => ['Orderplatform.OrderplatformBackend.AcceptOrder']],
            ['allow', 'actions' => ['correctionOrder'], 'roles' => ['Orderplatform.OrderplatformBackend.CorrectionOrder']],
            ['allow', 'actions' => ['returnBalance'], 'roles' => ['Orderplatform.OrderplatformBackend.ReturnBalance']],
            ['allow', 'actions' => ['claim'], 'roles' => ['Orderplatform.OrderplatformBackend.Claim']],
            ['allow', 'actions' => ['statistics'], 'roles' => ['Orderplatform.OrderplatformBackend.Statistics']],
            ['deny'],
        ];
    }

	/**
	 * Создание нового заказа
	 * Если создание прошло успешно - перенаправляет на список заказов
	 *
	 * @return void
	 */
	public function actionCreate(){
		$model = new OrdersPlatform;
		if ( Yii::app()->getRequest()->getPost( 'OrdersPlatform' ) !== null ) {
			$model->setAttributes( Yii::app()->getRequest()->getPost( 'OrdersPlatform' ) );
			 if(!empty($_POST['OrdersPlatform']['target_date']))
				 $model->target_date = ($model->target_date != '0000-00-00 00:00:00') ? Yii::app()->dateFormatter->format("y-MM-dd", $model->target_date) : new CDbExpression('NOW()');
			if ( $model->save() ) {
				Yii::app()->user->setFlash(
					yupe\ widgets\ YFlashMessages::SUCCESS_MESSAGE,
					Yii::t( 'OrderplatformModule.orderplatform', 'Заказ размещен' )
				);

				$this->redirect(['list', 'type'=>'auction']);
			}
		}
		$this->render( 'create', [
			'model' => $model,
		]);
	}

	/**
	 * Редактирование.
	 *
	 * @param integer $id Идинтификатор 1 для редактирования
	 *
	 * @return void
	 */
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id );

		if ($model->status_id >= StatusOrder::STATUS_AUTHOR)
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.' ) );

		if ( Yii::app()->getRequest()->getPost( 'OrdersPlatform' ) !== null ) {
			$model->setAttributes( Yii::app()->getRequest()->getPost( 'OrdersPlatform' ) );
			$model->target_date = $model->target_date != '0000-00-00 00:00:00' ? Yii::app()->dateFormatter->format("y-MM-dd", $model->target_date) : new CDbExpression('NOW()');
			if ( $model->save() ) {
				Yii::app()->user->setFlash(
					yupe\ widgets\ YFlashMessages::SUCCESS_MESSAGE,
					Yii::t( 'OrderplatformModule.orderplatform', 'Запись обновлена!' )
				);

				$this->redirect(['list', 'type'=>'auction']);
			}
		}
		$this->render( 'update', [ 'model' => $model ] );
	}
	
	public function actionIndex(){
		$model = new OrdersPlatform( 'search' );
		$model->unsetAttributes();
		if ( Yii::app()->getRequest()->getParam( 'OrdersPlatform' ) !== null )
			$model->setAttributes( Yii::app()->getRequest()->getParam( 'OrdersPlatform' ) );
		$this->render( 'index', [ 'model' => $model ] );
	}

	//просмотр заказов в зависимости от их статуса
	public function actionList($type='all'){

		OrdersPlatform::model()->OrdersToDelay();
			
		$model = new OrdersPlatform;
		$user = User::model()->findByPk(Yii::app()->getUser()->getId());

		switch($type){
			case 'all':
				$data = $model::model()->listOrdersAll($user);
			break;
			case 'finish':
				$data = $model::model()->listOrdersFinish();
			break;
			case 'auction':
				$data = $model::model()->listOrdersAuction();
			break;
			case 'correction':
				$data = $model::model()->listOrdersCorrection();
			break;
			case 'author':
				$data = $model::model()->listOrdersForSendAuthors();
			break;
			case 'work':
				$data = $model::model()->listOrdersForAuthors();
			break;			
			case 'check':
				$data = $model::model()->listOrdersCheck();
			break;
			case 'archive':
				$data = $model::model()->listOrdersArchive();
			break;
			default:
				$data = $model::model()->listOrdersAll($user);
			break;
		}

		$this->render( 'list', [ 'model' => $model, 'user' => $user, 'data' => $data] );
	}

	//отправка заказа в аукцион
	public function actionToAuction($id){
		if (!is_null($id)){
			$this->loadModel($id)->toAuction();
		}
		$this->redirect(['auction']);
	}

	//аукцион
	public function actionAuction(){
		$model = new OrdersPlatform;
		$dialog = new DialogOrderUsers;
		
		$dialog->setScenario('rely');

		$model->unsetAttributes();

		if ( Yii::app()->getRequest()->getParam( 'OrdersPlatform' ) !== null )
			$model->setAttributes( Yii::app()->getRequest()->getParam( 'OrdersPlatform' ) );

		$this->render('auction', ['model' => $model, 'dialog' => $dialog]);
	}

	//ставка - вывод окна с формой ставки и описанием заказа
	public function actionRely(){

		$model = new RatesOrder;

		if ( Yii::app()->getRequest()->getParam( 'id' ) !== null ) {

			$order = $this->loadModel(Yii::app()->getRequest()->getParam( 'id' ));
			if ($order){

				$rate = RatesOrder::model()->findByAttributes(['order_id' => Yii::app()->getRequest()->getParam( 'id' ), 'author_id' => Yii::app()->getUser()->getId()]);
				if ($rate){
					$model = $rate;
				}
				else{
					$model->order_id = Yii::app()->getRequest()->getParam( 'id' );
					$model->author_id = Yii::app()->getUser()->getId();
				}

				$this->renderPartial('_rely', ['model' => $model, 'order' => $order], false, true);
				Yii::app()->end();
			}
		}
		elseif(Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('RatesOrder')){

			$rate = RatesOrder::model()->findByAttributes(['order_id' => Yii::app()->getRequest()->getPost('RatesOrder')['order_id'], 'author_id' => Yii::app()->getRequest()->getPost('RatesOrder')['author_id']]);

			$model->setAttributes(Yii::app()->getRequest()->getPost('RatesOrder'));
			if ($rate){
				$rate->rate = Yii::app()->getRequest()->getPost('RatesOrder')['rate'];
				$rate->save();
 				echo CJSON::encode($rate);
				Yii::app()->end();
			}
			else{
				if($model->save()){
					echo CJSON::encode($model);
					Yii::app()->end();
				}
			}
		}
		else
			Yii::app()->ajax->failure('Заказ не найден');

		$this->redirect(['auction']);
	}
	
	/**
	* принятие клиентом ставки автора по заказу
	* сохраняем событие в лог
	**/
	public function actionRelyAccept(){
		OrdersPlatform::model()->confirmRate(Yii::app()->getRequest()->getParam('rate'));
	}
	
	//принятие заказа в работу автором
	public function actionConfirmOrder($id){
		OrdersPlatform::model()->confirmOrder(Yii::app()->request->getParam('id'));
		echo CJSON::encode(Yii::app()->request->getParam('id'));
		Yii::app()->end();
	}
	
	//отказ автора от заказа
	public function actionCancelOrder($id){
		OrdersPlatform::model()->cancelOrder(Yii::app()->request->getParam('id'));
	}
	
	//удаление заказа
	public function actionDelete($id){
		OrdersPlatform::model()->deleteOrder(Yii::app()->request->getParam('id'));
	}
	
	/*
	* исполнение заказа
	*
	*
	*/
	public function actionMakeOrder($id){
		
		$model = $this->loadModel($id);
		
		$solution = new SolutionsOrder;
		
		if ( Yii::app()->getRequest()->getPost('SolutionsOrder') !== null ) {
			$solution->setAttributes( Yii::app()->getRequest()->getPost( 'SolutionsOrder' ) );
			$solution->order_id = $model->id;
			
			if ($solution->save()){
				
				$model->status_id = StatusOrder::STATUS_CHECK;
				$date_target = new DateTime($model->target_date);
				$model->target_date = $date_target->modify('+20 day')->format('Y-m-d H:i:s');
				$model->save();
				
				if((Yii::app()->getRequest()->getParam('correction'))){
					$correction = OrderCorrection::model()->findByPk(Yii::app()->getRequest()->getParam('correction'));
					$correction->status=1;
					$correction->save();
				}
				
				$this->redirect(['list', 'type'=>'auction']);
			}
		}
		
		$this->render( 'solution', ['model' => $model, 'solution' => $solution]);
		
	}
	
	/*
	* претензия по заказу
	*
	*
	*/
	public function actionClaim($id){
		
		$claim = new ArbitrationCourt;
		$claim->scenario='user';
			
		if ( Yii::app()->getRequest()->getPost('ArbitrationCourt') !== null ) {
			$claim->setAttributes( Yii::app()->getRequest()->getPost( 'ArbitrationCourt' ) );
			$claim->order_id = $id;
			
			if ($claim->save()){
				$this->redirect(['list']);
			}
		}
		
		$this->render('claim', ['model' => $claim, 'order' => $id]);
		
	}
	
	//утверждение автором заказа
	public function actionAcceptOrder($id){
		OrdersPlatform::model()->acceptOrder(Yii::app()->request->getPost('id'));
		echo CJSON::encode(Yii::app()->request->getPost('id'));
		Yii::app()->end();
	}
	
	//отправить заказ на доработку
	public function actionCorrectionOrder($id){
		$model = $this->loadModel($id);
		
		$correction = new OrderCorrection;
		
		if ( Yii::app()->getRequest()->getPost('OrderCorrection') !== null ) {
			$correction->setAttributes( Yii::app()->getRequest()->getPost( 'OrderCorrection' ) );
			$correction->order_id = $model->id;
			
			if ($correction->save()){
				
				$model->status_id = StatusOrder::STATUS_CORRECTION;
				$model->save();
				
				$this->redirect(['list', 'type'=>'correction']);
			}
		}
		
		$this->render( 'correction', ['order' => $model, 'correction' => $correction]);
	}
	
	//статистика по заказам и текущая ставка
	public function actionStatistics(){
		
	}
	
	//вывод денег для автора
	public function actionReturnBalance(){
		
	}
	
	//тестовый адрес для просроченного заказа
	public function actionDelay(){
		OrdersPlatform::model()->OrdersToDelay();
	}
	
	/**
	 * Возвращает модель по указанному идентификатору
	 * Если модель не будет найдена - возникнет HTTP-исключение.
	 *
	 * @param integer идентификатор нужной модели
	 *
	 * @return void
	 */
	public function loadModel( $id ) {
		$model = OrdersPlatform::model()->findByPk( $id );
		if ( $model === null )
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.' ) );

		return $model;
	}
}