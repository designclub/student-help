<?php

/**
 * This is the model class for table "{{commission_clients}}".
 *
 * The followings are the available columns in table '{{commission_clients}}':
 * @property integer $id
 * @property integer $comission_percent
 * @property string $comission_sum
 * @property string $sum_paid_from
 * @property string $sum_paid_to
 * @property string $name_level
 */
class CommissionClients extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{commission_clients}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comission_percent', 'required'),
			array('comission_percent', 'numerical', 'integerOnly'=>true),
			array('comission_sum, sum_paid_from, sum_paid_to', 'length', 'max'=>10),
			array('name_level', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, comission_percent, comission_sum, sum_paid_from, sum_paid_to, name_level', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'comission_percent' => 'Комиссия (%)',
			'comission_sum' => 'Комиссия (в рублях)',
			'sum_paid_from' => 'Денежный оборот (от)',
			'sum_paid_to' => 'Денежный оборот (до)',
			'name_level' => 'Наименование уровня',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('comission_percent',$this->comission_percent);
		$criteria->compare('comission_sum',$this->comission_sum,true);
		$criteria->compare('sum_paid_from',$this->sum_paid_from,true);
		$criteria->compare('sum_paid_to',$this->sum_paid_to,true);
		$criteria->compare('name_level',$this->name_level,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getCommissionClient($id_client){
		
		$where = "client_id = " . $id_client;
		$where .= " AND status_id = " . StatusOrder::STATUS_FINISH;
		$where .= " AND update_date > NOW() - INTERVAL 6 MONTH";
		
		$sum_order_half_year = Yii::app()->db->createCommand("SELECT sum(cost) as sum_order_half_year FROM sh_orders_platform WHERE ". $where)->queryRow();
		
		$sum_order = !empty($sum_order_half_year['sum_order_half_year']) ? : 0 ; 

		$commission = Yii::app()->db->createCommand("SELECT comission_percent FROM sh_commission_clients WHERE " . $sum_order . " BETWEEN sum_paid_from AND sum_paid_to")->queryRow();
		
		return !empty($commission['comission_percent']) ? $commission['comission_percent'] : 0;
		
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommissionClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
