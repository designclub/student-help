<div class="panel panel-primary panel-profile">
	<div class="panel-heading">
		<?= Yii::t( 'OrderplatformModule.orderplatform', 'Filter types' ); ?>
	</div>
	<div class="panel-body">
		<div class="row">
			<?= CHtml::form('', 'post', ['id' => 'types-filter', 'class' => 'profile-filters']) ?>
			<?= TypesOrder::model()->getTree($filter) ?>
			<?= CHtml::endForm() ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$( document ).ready( function () {

		function isCheckboxCheked() {
			if ( $( "input[name^=TypesOrder]:not(:checked)" ).length === 0 ) {
				$( '#allTypes' ).prop( 'checked', true );
			}
		};

		$( '#allTypes' ).on( 'change', function () {
			$( "input[name^=TypesOrder]" ).prop( 'checked', this.checked );
		} );

		$( '#types-filter' ).on( 'change', function () {
			var elem = $( this );
			var data = elem.serialize() + '&TypesOrder[0]["type_id"]=0';

			isCheckboxCheked();

			$.ajax( {
				data: data,
				type: 'post',
			} )
			return false;
		} );

		isCheckboxCheked();
	} );
</script>