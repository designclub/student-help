<?php
/**
 * Файл настроек для модуля client
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2018 amyLabs && Yupe! team
 * @package yupe.modules.client.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.client.ClientModule',
    ],
    'import'    => [
        'application.modules.user.components.*',
        'application.modules.user.listeners.*',
        'application.modules.user.forms.*',
	],
    'component' => [],
    'rules'     => [
        '/client' => 'client/client/index',
    ],
];