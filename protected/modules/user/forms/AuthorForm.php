<?php
use yupe\widgets\YPurifier;

/**
 * Форма автора
 *
 * @category YupeComponents
 * @package  yupe.modules.user.forms
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     http://yupe.ru
 *
 **/
class AuthorForm extends CFormModel
{
    public $short_description_profile;
    public $options_types_work;
    public $options_subjects_work;
 

    public function rules()
    {
        $module = Yii::app()->getModule('user');

        return [
            ['short_description_profile, options_types_work, options_subjects_work', 'filter', 'filter' => 'trim'],
			
            [
                'short_description_profile, options_types_work, options_subjects_work',
                'filter',
                'filter' => [$obj = new YPurifier(), 'purify']
            ],
 
            ['about, short_description_profile', 'length', 'max' => 500],
            
        ];
    }

    public function attributeLabels()
    {
        return [
            'short_description_profile' => Yii::t('UserModule.user', 'Short description profile'),
            'options_types_work' => Yii::t('UserModule.user', 'Options types work'),
            'options_subjects_work' => Yii::t('UserModule.user', 'Options subjects work'),
        ];
    }


}
