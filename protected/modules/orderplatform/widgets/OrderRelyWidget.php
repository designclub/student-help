<?php

Yii::import('application.modules.orderplatform.models.*');

/**
 * Class OrderRelyWidget
 */
class OrderRelyWidget extends \yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'orderRely';

    /**
     * @throws CException
     */
    public function init()
    {

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $module = Yii::app()->getModule('orderplatform');
        $this->render($this->view);
    }
}