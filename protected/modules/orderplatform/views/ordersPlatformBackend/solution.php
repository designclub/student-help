<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OrderplatformModule.orderplatform', 'Мои заказы') => ['/orderplatform/ordersPlatformBackend/list'],
    Yii::t('OrderplatformModule.orderplatform', 'Выполнение заказа'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Выполнение заказа');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Форма выполнения заказа'); ?>
    </h1>
</div>

<?=  $this->renderPartial('_solution', ['model' => $solution]); ?>