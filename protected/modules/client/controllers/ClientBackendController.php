<?php
/**
 * ClientBackendController контроллер для client в панели управления
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2018 amyLabs && Yupe! team
 * @package yupe.modules.client.controllers
 * @since 0.1
 *
 */

class ClientBackendController extends\ yupe\ components\ controllers\ BackController {
 	
	public $user;
	
	public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Client.ClientBackend.Index']],
            ['deny'],
        ];
    }
	
	public function beforeAction( $action ) {
		$this->user = Yii::app()->getUser()->getProfile();

		if ( $this->user === null ) {

			Yii::app()->getUser()->setFlash(
				yupe\ widgets\ YFlashMessages::ERROR_MESSAGE,
				Yii::t( 'UserModule.user', 'User not found.' )
			);

			Yii::app()->getUser()->logout();

			$this->redirect(
				[ '/user/account/login' ]
			);
		}

		return true;
	}

	public function actionIndex() {
		
		$user = Yii::app()->getUser()->getProfile();
		$module = Yii::app()->getModule( 'user' );

		$form = new ProfileForm();

		$formAttributes = $form->getAttributes();

		unset( $formAttributes[ 'avatar' ], $formAttributes[ 'verifyCode' ] );

		$form->setAttributes( $user->getAttributes( array_keys( $formAttributes ) ) );

		// Если у нас есть данные из POST - получаем их:
		if ( ( $data = Yii::app()->getRequest()->getPost( 'ProfileForm' ) ) !== null ) {

			$transaction = Yii::app()->getDb()->beginTransaction();

			try {

				$form->setAttributes( $data );

				if ( $form->validate() ) {
					// Удаляем ненужные данные:
					unset( $data[ 'avatar' ] );

					// Заполняем модель данными:
					$user->setAttributes( $data );

					// Если есть ошибки в профиле - перекинем их в форму
					if ( $user->hasErrors() ) {
						$form->addErrors( $user->getErrors() );
					}

					// Если у нас есть дополнительные профили - проверим их
					foreach ( ( array )$module->profiles as $p ) {
						$p->validate() || $form->addErrors( $p->getErrors() );
					}

					// Если нет ошибок валидации:
					if ( $form->hasErrors() === false ) {

						Yii::log(
							Yii::t(
								'UserModule.user',
								'Profile for #{id}-{nick_name} was changed', [
									'{id}' => $user->id,
									'{nick_name}' => $user->nick_name,
								]
							),
							CLogger::LEVEL_INFO,
							UserModule::$logCategory
						);

						Yii::app()->getUser()->setFlash(
							yupe\ widgets\ YFlashMessages::SUCCESS_MESSAGE,
							Yii::t( 'UserModule.user', 'Your profile was changed successfully' )
						);

						if ( ( $uploadedFile = CUploadedFile::getInstance( $form, 'avatar' ) ) !== null ) {
							$user->changeAvatar( $uploadedFile );
						} elseif ( $form->use_gravatar ) {
							$user->removeOldAvatar();
						};

						$user->save();

						// И дополнительные профили, если они есть
						if ( is_array( $module->profiles ) ) {
							foreach ( $module->profiles as $k => $p ) {
								$p->save( false );
							}
						}

						Yii::app()->getUser()->setFlash(
							yupe\ widgets\ YFlashMessages::SUCCESS_MESSAGE,
							Yii::t( 'UserModule.user', 'Profile was updated' )
						);

						$transaction->commit();

						$this->getController()->redirect( [ '/user/profile/profile' ] );

					} else {

						Yii::log(
							Yii::t( 'UserModule.user', 'Error when save profile! #{id}', [ '{id}' => $user->id ] ),
							CLogger::LEVEL_ERROR,
							UserModule::$logCategory
						);
					}
				}

			} catch ( Exception $e ) {

				$transaction->rollback();

				Yii::app()->getUser()->setFlash(
					yupe\ widgets\ YFlashMessages::ERROR_MESSAGE,
					$e->getMessage()
				);
			}
		}

		$this->render(
			'//user/profile/profile', [
				'model' => $form,
				'module' => $module,
				'user' => $user
			]
		);
	}
}