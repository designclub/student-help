<?php
/**
* Отображение для clientBackend/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     http://yupe.ru
**/
$this->breadcrumbs = [
    Yii::t('ClientModule.client', 'client') => ['/client/clientBackend/index'],
    Yii::t('ClientModule.client', 'Index'),
];

$this->pageTitle = Yii::t('ClientModule.client', 'client - index');

$this->menu = $this->getModule()->getNavigation();
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('ClientModule.client', 'client'); ?>
        <small><?php echo Yii::t('ClientModule.client', 'Index'); ?></small>
    </h1>
</div>