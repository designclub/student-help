<?php

class PlatformRepository extends CApplicationComponent
{
  
    public function init()
    {
        return parent::init();
    }

    /**
     * Returns formatted subjects tree
     *
     * @param null|int $parentId
     * @param int $level
     * @param null|array|CDbCriteria $criteria
     * @param string $spacer
     * @return array
     */
    public function getFormattedListSubjects($parentId = null, $level = 0, $criteria = null, $spacer = '---')
    {
        if (empty($parentId)) {
            $parentId = null;
        }

        $subjects = SubjectOrder::model()->findAllByAttributes(['parent_id' => $parentId], $criteria);

        $list = [];

        foreach ($subjects as $subject) {

            $subject->name_subject = str_repeat($spacer, $level) . $subject->name_subject;

            $list[$subject->id] = $subject->name_subject;

            $list = CMap::mergeArray($list, $this->getFormattedListSubjects($subject->id, $level + 1, $criteria));
        }

        return $list;
    }

}