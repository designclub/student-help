<div>
	<div class="comment-item-carousel">
	
		<div class="row">
		
			<div class="col-sm-3 col-xs-12">
				<img src="<?= $data->getImageUrl(); ?>" alt="<?= $data->title; ?>" />
			</div>
			
			<div class="col-sm-9 col-xs-12 comment-description">
				<h5><?= $data->title; ?></h5>
 
				<div class="news-carousel-text">
					<?= $data->full_text;	?>
				</div>
				
			</div>
			
		</div>
		
	</div>
</div>