<?php

/**
 * This is the model class for table "{{types_order}}".
 *
 * The followings are the available columns in table '{{types_order}}':
 * @property integer $id
 * @property string $name_type
 *
 * The followings are the available model relations:
 * @property OrdersPlatform[] $ordersPlatforms
 */
class TypesOrder extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{types_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_type', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ordersPlatforms' => array(self::HAS_MANY, 'OrdersPlatform', 'type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_type' => 'Наименование типа',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_type',$this->name_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	public function getTree( $filter, $child = null ) {
		$data = '';
		$filterData = $filter->getData();

		if ( $child === null ) {
			$criteria = new CDbCriteria;
			$models = self::model()->findAll( $criteria );
		}
 
		$data .= CHtml::openTag( 'ul' );
		
		$data .= CHtml::openTag( 'li' , ['class' => 'col-sm-12 main-category']);
		$data .= CHtml::openTag( 'label' );
		$data .= CHtml::checkBox( "allTypes", '', ['id' => 'allTypes']);
		$data .= " Выбрать все типы работ ";
		$data .= CHtml::closeTag( 'label' );

		$data .= CHtml::closeTag( 'li' );
		
		foreach ( $models as $key => $model ) {
 
			$data .= CHtml::openTag( 'li' , ['class' => 'col-sm-3 sub-category']);
			$data .= CHtml::openTag( 'label' );
			$data .= CHtml::checkBox( "TypesOrder[{$model->id}][type_id]", $filter->getValue($model->id, 'type_id' ), ['
			value' => $model->id]);
			$data .= " {$model->name_type} ";
			$data .= CHtml::closeTag( 'label' );
			
			$data .= CHtml::closeTag( 'li' );
		}
		$data .= CHtml::closeTag( 'ul' );

		return $data;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TypesOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
