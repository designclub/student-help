<?php

/**
 * This is the model class for table "{{files_order}}".
 *
 * The followings are the available columns in table '{{files_order}}':
 * @property integer $id
 * @property string $date_upload
 * @property string $name_file
 * @property string $title_file
 * @property integer $author_id
 * @property integer $order_id
 * @property integer $solution_id
 *
 * The followings are the available model relations:
 * @property UserUser $author
 * @property OrdersPlatform $order
 * @property SolutionsOrder $solution
 */
class FilesOrder extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{files_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, solution_id', 'required'),
			array('author_id, order_id, solution_id', 'numerical', 'integerOnly'=>true),
			array('name_file, title_file', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_upload, name_file, title_file, author_id, order_id, solution_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
			'solution' => array(self::BELONGS_TO, 'SolutionsOrder', 'solution_id'),
		);
	}
	
	public function beforeSave(){
		
		$this->author_id   = Yii::app()->getUser()->getId();
		$this->date_upload = new CDbExpression('NOW()');

		return parent::beforeSave();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_upload' => 'Date Upload',
			'name_file' => 'Name File',
			'title_file' => 'Title File',
			'author_id' => 'Author',
			'order_id' => 'Order',
			'solution_id' => 'Solution',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_upload',$this->date_upload,true);
		$criteria->compare('name_file',$this->name_file,true);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('solution_id',$this->solution_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FilesOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
