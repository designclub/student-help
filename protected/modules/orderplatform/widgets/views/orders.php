<div class="row list-orders list-orders-panel">
	<h4>Мои заказы</h4>
	<?php $this->widget(
		'bootstrap.widgets.TbListView',
		array(
			'dataProvider'  => $orders,
			'template' => '{items}',
			'itemView'      => '_order-list',
			'viewData' => ['user' => Yii::app()->getUser()],
			'emptyText'		=> 'Пока заказов нет!'
		)
	); ?>
	<?php if(Yii::app()->user->getProfile()->type_account == 1): ?>
		<?= CHtml::link('Сделать заказ', Yii::app()->createAbsoluteUrl('/backend/orderplatform/ordersPlatform/create'), ['class' => 'create-order']); ?>
	<?php endif; ?>
</div>

<?php
	$this->widget('application.modules.orderplatform.widgets.DialogWidget', 
					['view'=>Yii::app()->user->getProfile()->type_account == 1 ? 'dialog' : 'dialogAuthor']); ?>