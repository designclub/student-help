<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Заказать работу') => ['/orderplatform/ordersPlatformBackend/create'],
    Yii::t('OrderplatformModule.orderplatform', 'Оформление заказа'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Заказать работу');

/*$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Мои заказы'), 'url' => ['/orderplatform/ordersPlatformBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Разместить заказ'), 'url' => ['/orderplatform/ordersPlatformBackend/create']],
];*/
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Форма добавления заказа'); ?>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>