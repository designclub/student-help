<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Баланс') => ['/orderplatform/userBalance/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Управление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Баланс - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Балансом'), 'url' => ['/orderplatform/userBalance/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Баланс'), 'url' => ['/orderplatform/userBalance/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Баланс'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Поиск Баланса');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('user-balance-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('OrderplatformModule.orderplatform', 'В данном разделе представлены средства управления Балансом'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'user-balance-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            'date_create',
            'date_update',
            'date',
            'number_bill',
            'money',
//            'balance',
//            'comment',
//            'user_id',
//            'order_id',
//            'operation',
//            'status',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
