var socket = io('http://138.201.254.223:8888');
jQuery(document).ready(function ($) {
	// Сериализация формы в объект:
	$.fn.serializeObject = function () {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function () {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	$('body').addClass('admin-panel');

	$('.popover-help').popover({
		trigger: 'hover',
		delay: 500,
		html: true
	});
	/**
	 * Ajax-управление статусами модулей:
	 **/
	$(document).on('click', '.changeStatus', function (e) {
		e.preventDefault();
		if ((link = this) === undefined || (method = $(this).attr('method')) === undefined || (message = actionToken.messages['confirm_' + method]) === undefined)
			bootbox.confirm(actionToken.messages['unknown']);
		else {
			bootbox.confirm(message, function (result) {
				if (result) {
					sendModuleStatus($(link).attr('module'), $(link).attr('status'));
				}
			});
		}
		return false;
	});
	/**
	 * Ajax-управление сбросом кеша и ресурсов (assets):
	 **/
	$(document).on('click', '.flushAction', function (e) {
		e.preventDefault();
		if ((link = this) === undefined || (method = $(this).attr('method')) === undefined || (message = actionToken.messages['confirm_' + method]) === undefined)
			bootbox.confirm(actionToken.messages['unknown']);
		else {
			bootbox.confirm(message, function (result) {
				if (result) {
					sendFlush(link);
				}
			});
		}
		return false;
	});

	/**
	 * Ajax-перехватчик для повторной отправки письма активации:
	 */
	$(document).on('click', '.user.sendactivation', function () {
		var link = $(this);
		$.ajax({
			url: link.attr('href'),
			data: actionToken.token,
			dataType: 'json',
			type: 'post',
			success: function (data) {
				if (typeof data.data != 'undefined' && typeof data.result != 'undefined')
					bootbox.alert('<i class="fa fa-fw fa-' + (data.result ? 'check' : 'times') + '-circle"></i> ' + data.data);
				else
					bootbox.alert('<i class="fa fa-fw fa-times-circle"></i> ' + actionToken.error);
			},
			error: function (data) {
				if (typeof data.data != 'undefined' && typeof data.result != 'undefined')
					bootbox.alert('<i class="fa fa-fw fa-' + (data.result ? 'check' : 'times') + '-circle"></i> ' + data.data);
				else
					bootbox.alert('<i class="fa fa-fw fa-times-circle"></i> ' + actionToken.error);
			}
		});
		return false;
	});
});

function ajaxSetStatus(elem, id) {
	$.ajax({
		url: $(elem).attr('href'),
		success: function () {
			$('#' + id).yiiGridView.update(id, {
				data: $('#' + id + '>.keys').attr('title')
			});
		}
	});
}

function ajaxSetSort(elem, id) {
	$.ajax({
		url: $(elem).attr('href'),
		success: function () {
			$('#' + id).yiiGridView.update(id);
		}
	});
}

function sendFlush(link) {
	var dataArray = [
		actionToken.token,
		'method=' + $(link).attr('method')
	];

	var myDialog = bootbox.alert(actionToken.message);

	$(myDialog).find('div.modal-footer .btn').hide();
	$(myDialog).find('div.modal-footer').append(actionToken.loadingimg);
	/**
	 * Запрет на закрытие бокса:
	 **/
	$('.modal-backdrop').unbind('click');
	/**
	 * Запрашиваем ответ сервера:
	 **/
	$.ajax({
		url: $(link).attr('href'),
		data: dataArray.join('&'),
		type: 'POST',
		dataType: 'json',
		error: function (data) {
			$(myDialog).find('.modal-body').html(
				data.data ? data.data : actionToken.error
			);
			$(myDialog).find('div.modal-footer .btn').show();
			$(myDialog).find('div.modal-footer img').hide();
		},
		success: function (data) {
			if (typeof data.result != 'undefined' && typeof data.data != 'undefined' && data.result === true) {
				$(myDialog).find('.modal-body').html(data.data);
			} else {
				$(myDialog).find('.modal-body').html(
					typeof data.data == 'undefined' ? actionToken.error : data.data
				);
			}
			$(myDialog).find('div.modal-footer .btn').show();
			$(myDialog).find('div.modal-footer img').hide();
			// При клике на кнопку - перегружаем страницу:
			$(myDialog).find('div.modal-footer .btn').click(function () {
				location.reload();
			});
		}
	});
}

function sendModuleStatus(name, status) {
	var dataArray = [
		actionToken.token,
		'module=' + name,
		'status=' + status
	];

	var myDialog = bootbox.alert(actionToken.message);
	$(myDialog).find('div.modal-footer .btn').hide();
	$(myDialog).find('div.modal-footer').append(actionToken.loadingimg);
	/**
	 * Запрет на закрытие бокса:
	 **/
	$('.modal-backdrop').unbind('click');
	/**
	 * Запрашиваем ответ сервера:
	 **/
	$.ajax({
		url: actionToken.url,
		data: dataArray.join('&'),
		type: 'POST',
		dataType: 'json',
		error: function (data) {
			if (typeof data.result != 'undefined' && typeof data.data != 'undefined' && data.result === true) {
				$(myDialog).find('.modal-body').html(data.data);
			} else {
				$(myDialog).find('.modal-body').html(
					typeof data.data == 'undefined' ? actionToken.error : data.data
				);
			}
			$(myDialog).find('div.modal-footer a.btn').show();
			$(myDialog).find('div.modal-footer img').hide();
			// При клике на кнопку - перегружаем страницу:
			$(myDialog).find('div.modal-footer a.btn').click(function () {
				location.reload();
			});
		},
		success: function (data) {
			if (typeof data.result != 'undefined' && typeof data.data != 'undefined' && data.result === true) {
				$(myDialog).find('.modal-body').html(data.data);
			} else {
				$(myDialog).find('.modal-body').html(
					typeof data.data == 'undefined' ? actionToken.error : data.data
				);
			}
			$(myDialog).find('div.modal-footer .btn').show();
			$(myDialog).find('div.modal-footer img').hide();
			// При клике на кнопку - перегружаем страницу:
			$(myDialog).find('div.modal-footer .btn').click(function () {
				location.reload();
			});
		}
	});
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('.preview-image-wrapper').removeClass('hidden');
			$('.preview-image').attr('src', e.target.result).show();
		};

		reader.readAsDataURL(input.files[0]);
	}
}

function loadDialog(idOrder, data){
	$("#dialog-body-"+idOrder).html(data);
}

jQuery(document).ready(function ($) {
	$('.link-more').on('click', function (e) {
		e.preventDefault();
		$(this).siblings('.more-text').toggleClass('hide');
	});

	$('.link-more-rely').on('click', function (e) {
		e.preventDefault();
		$('.more-text-rely').toggleClass('hide');
	});

	$('.link-rely').on('click', function (e) {
		e.preventDefault();
	});

	$('.link-dialog').on('click', function (e) {
		e.preventDefault();
		var idOrder = $(this).attr('data-order');
		var idUserTo = $(this).attr('data-user-to');
		var link = $(this).attr('link');

		$.ajax({
			type: 'POST',
			url: link,
			data: {
				order: idOrder,
				YUPE_TOKEN: yupeToken
			},
			success: function (data) {
				$('.dialog-body').attr('id', 'dialog-body-'+idOrder);

				loadDialog(idOrder, data);

				$('#DialogOrderUsers_order_id').val(idOrder);

				if (typeof idUserTo  !== "undefined"){
					$('#DialogOrderUsers_user_to').val(idUserTo);
				}

				$('.list-dialog').attr('data-user-to', idUserTo);
				$('.list-dialog').attr('data-order', idOrder);

				$('#navDialogs a').click(function(e) {
					$('#DialogOrderUsers_user_to').val($(this).attr('data-author-id'));
					e.preventDefault();
					$(this).tab('show');
				});
			}
		});
	});
	
	$('body').on('click', '.link-rely-accept', function (e) {
		e.preventDefault();
		
		var idRate = $(this).attr('data-rate');
		var link = $(this).attr('href');

		$.confirm({
			title: 'Подтверждаете ставку автора?',
			icon: 'fa fa-question-circle-o',
			scrollToPreviousElement: false,
			closeIcon: true,
			animation: 'zoom',
			theme: 'modern',
			content: '',
			buttons: {
				confirm: {
					text: 'Принять',
					btnClass: 'btn-orange',
					action: function () {
						$.ajax({
							type: 'POST',
							url: link,
							data: {
								rate: idRate,
								YUPE_TOKEN: yupeToken
							},
							success: function (data){
								location.reload();
							}
						});
					}
				},
				cancel: {
					text: 'Отмена',
					btnClass: 'btn-default',
					action: function () {

					}
				}
			}
		});
		
		return false;
	});
	

	// Отправить идентификатор пользователя
	socket.emit('connect message', { userId: userId });

	// Получить новые сообщения
	socket.on('new message', function (data){

		console.log(data);
		// Обновить счетчик у заказа
		var showDialog = $('.link-dialog[data-order=' + data.order_id + ']');

		showDialog
			.removeClass('hidde')
			.addClass('show');

		var count = parseInt(showDialog.find('span').text());

		showDialog
			.find('span')
			.text(count+1);

		// Показать уведомление
		$('#notifications').notify({
			message: { html: '<strong>'+data.userName+'</strong><br />'+data.message }
		}).show();

		$('span[data-user='+data.user_from+'][data-order='+data.order_id+']')
			.removeClass('hide')
			.find('strong')
			.text(data.count);

		//добавляем сообщение в чат диалога тому, кому оно адресовано
		var html = $('<div class="col-sm-10 pull-right"><div class="row"><div class="col-sm-10"><div class="dialog-message dialog-message-user"></div></div><div class="col-sm-2"><label></label></div></div></div>');

		var create_date = new Date(data.create_date);

		if (userId === data.user_to){
			html.find('label').text(data.userName);
		}
		else{
			html.find('label').text(userName);
		}

		html.find('div.dialog-message').text(data.message);
		html.find('div.dialog-message').append($('<span></span>').text(create_date.getDate() + '.' + ((create_date.getMonth() + 1) < 10 ? '0' + (create_date.getMonth() + 1) : create_date.getMonth() + 1) + '.' + create_date.getFullYear() + 'г.'));

		if (userType == 1) {
			$('div#'+data.userName).append(html);
		} else {
			$('.list-dialog').append(html);
		}
		setTimeout(function() {
			$('.list-dialog').scrollTop(100000000000);
		}, 200);
		/*$.each(data, function(i, e) {
			var id          = e.id;
			var user_to   = e.user_to;
			var user_from   = e.user_from;
			var create_date = new Date(e.create_date);
			var message     = e.message;
			var nick_from = e.nick_from;
			var nick_to = e.nick_to;
			var order_id    = e.order_id;
			var listDialog  = $('.list-dialog');
			var html        = $('<div class="col-sm-10"><div class="row"><div class="col-sm-2"><label></label></div><div class="col-sm-10"><div class="dialog-message"></div></div></div></div>');

			// Показать кнопки диалога
			$('.link-dialog[data-order=' + order_id + ']')
				.removeClass('hidde')
				.addClass('show');

			html.find('label').text(nick_author);
			html.find('div.dialog-message').text(message);
			html.find('div.dialog-message').append($('<span></span>').text(create_date.getDate() + '.' + ((create_date.getMonth() + 1) < 10 ? '0' + (create_date.getMonth() + 1) : create_date.getMonth() + 1) + '.' + create_date.getFullYear() + 'г.'));

			if ($('.active.list-dialog').length > 0){
				$('.active.list-dialog').append(html);
			} else {
				$('.list-dialog').append(html);
			}
			setTimeout(function() {
				$('.list-dialog').scrollTop(100000000000);
			}, 200);
		})*/


	});

	$('.link-rely').on('click',function(){
        var dataURL = $(this).attr('href');
        $('#modal-rely-content').load(dataURL);
    });

});

// Скролинг при переключении в табах диалога
$(document).delegate('#navDialogs li', 'click', function() {
	var elem = $(this);
	var idDialog = elem.data('user');
	$('#'+idDialog).scrollTop(100000000000);
	$.ajax({
		url: elem.data('url'),
		success: function(data) {
			elem
				.find('span')
				.addClass('hide');
		}
	})
});


function SendMessage(form, data, hasError)
{
	if (hasError) return false;

	var url = form.attr('action');
	var idOrder = form.find("#DialogOrderUsers_order_id").val();


	//добавляем сообщение в чат диалога самому себе

	// Отправить сообщение
	// Получить идентификатор автора если есть
	var active = $('#navDialogs > li.active');

	var dataForm = form.serializeArray();

	dataForm.push({
		'name': 'userId', 'value': userId,
	});

	dataForm.push({
		'name': 'userName', 'value': userName,
	});

	socket.emit('send message', { form: dataForm });

	var message = form.find('textarea').val();

	var html = $('<div class="col-sm-10"><div class="row"><div class="col-sm-2"><label></label></div><div class="col-sm-10"><div class="dialog-message"></div></div></div></div>');

	var create_date = new Date();

	html.find('label').text(userName);
	html.find('div.dialog-message').text(message);

	html.find('div.dialog-message').append($('<span></span>').text(create_date.getDate() + '.' + ((create_date.getMonth() + 1) < 10 ? '0' + (create_date.getMonth() + 1) : create_date.getMonth() + 1) + '.' + create_date.getFullYear() + 'г.'));

	$('.list-dialog').append(html);

	document.getElementById("dialog-form").reset();

	setTimeout(function() {
		$('.list-dialog').scrollTop(100000000000);
	}, 200);

	 /*$.ajax({
	 	url: url,
	 	type: 'POST',
	 	data: form.serialize(),
	 	success: function (data){
	 		document.getElementById("dialog-form").reset();
	 		loadDialog(idOrder, data);

	 		$('#navDialogs a').on('click', function (e) {
				$('#navDialogs li').removeClass('active');
				$(this).parent().addClass('active');

				$('.tab-pane').removeClass('active');
				$('#'+$(this).attr('href').slice(1)).addClass('active');

	 			e.preventDefault();
	 			//$(this).tab('show');
	 		});
	 	},
	 	error: function () {
	 		document.getElementById("dialog-form").reset();
	 	}
	 });*/
	return false;
}

function rateSendForm(form, data, hasError){
    if (hasError) return false;

	var url = form.attr('action');
    $.ajax({
        url: url,
        type: 'POST',
		dataType: 'json',
        data: form.serialize(),
        success: function (response){
			console.log(response);
            if (response){
				$("#order-id-"+response.order_id).html(response.rate);
				$('.close').click();
				location.reload();
            }
        },
        error: function(){
        }
    });
    return false;
}

$(document).on('click', '.order-to-work', function (e) {
	e.preventDefault();

	var url = $(this).attr('href');
	var id = $(this).attr('data-order');
	
	$.confirm({
		title: 'Взять заказ в работу!',
		icon: 'fa fa-question-circle-o',
		scrollToPreviousElement: false,
		closeIcon: true,
		animation: 'zoom',
		theme: 'modern',
		content: '',
		buttons: {
			confirm: {
				text: 'Продолжить',
				btnClass: 'btn-orange',
				action: function () {
					$.ajax({
						url: url,
						type: 'post',
						dataType: 'json',
						data: {
							id: id,
							YUPE_TOKEN: yupeToken
						},
						complete: function (response) {
 
							$.alert({
								title: 'Принять заказ',
								content: 'Заказ перешел в работу!',
							});
							
							$("#order-"+response.responseJSON).remove();
						}
					});
				}
			},
			cancel: {
				text: 'Отмена',
				btnClass: 'btn-default',
				action: function () {

				}
			}
		}
	});
	return false;
});

$(document).on('click', '.order-no-work', function (e) {
	e.preventDefault();

	var url = $(this).attr('href');
	var id = $(this).attr('data-order');
	$.confirm({
		title: 'Подтвердите отказ от заказа!',
		icon: 'fa fa-question-circle-o',
		scrollToPreviousElement: false,
		closeIcon: true,
		animation: 'zoom',
		theme: 'modern',
		content: '',
		buttons: {
			confirm: {
				text: 'Продолжить',
				btnClass: 'btn-orange',
				action: function () {
					$.ajax({
						url: url,
						type: 'post',
						dataType: 'json',
						data: {
							id: id,
							YUPE_TOKEN: yupeToken
						},
						complete: function (url) {
							$.alert({
								title: 'Отказ от заказа',
								content: 'Вы отказались от выполнения заказа!',
							});
							
							$("#order-"+id).remove();
							
/*							setTimeout(
								 function() {
									 window.location.href = url;
								}, 2000
							);*/
						}
					});
				}
			},
			cancel: {
				text: 'Отмена',
				btnClass: 'btn-default',
				action: function () {

				}
			}
		}
	});
	return false;
});

$(document).on('click', '.btn-delete', function (e) {
	e.preventDefault();

	var url = $(this).attr('href');
	var id = $(this).attr('data-order');
	$.confirm({
		title: 'Подтвердите удаление заказа!',
		icon: 'fa fa-question-circle-o',
		scrollToPreviousElement: false,
		closeIcon: true,
		animation: 'zoom',
		theme: 'modern',
		content: '',
		buttons: {
			confirm: {
				text: 'Удалить',
				btnClass: 'btn-orange',
				action: function(){
					$.ajax({
						url: url,
						type: 'post',
						dataType: 'json',
						data: {
							id: id,
							YUPE_TOKEN: yupeToken
						},
						complete: function(){
							
							$.alert({
								title: 'Удаление заказа',
								content: 'Заказ удален!',
							});
							
							$("#order-"+id).remove();
							
						}
					});
				}
			},
			cancel: {
				text: 'Отмена',
				btnClass: 'btn-default',
				action: function () {

				}
			}
		}
	});
	return false;
});


$(document).on('click', '.order-to-correction', function (e) {
	e.preventDefault();

	var url = $(this).attr('href');
	var id = $(this).attr('data-order');
	$.confirm({
		title: 'Отправить на доработку!',
		icon: 'fa fa-question-circle-o',
		scrollToPreviousElement: false,
		closeIcon: true,
		animation: 'zoom',
		theme: 'modern',
		content: '',
		buttons: {
			confirm: {
				text: 'Отправить',
				btnClass: 'btn-orange',
				action: function(){
					$.ajax({
						url: url,
						type: 'post',
						dataType: 'json',
						data: {
							id: id,
							YUPE_TOKEN: yupeToken
						},
						complete: function(){
							
							$.alert({
								title: 'Заказа на доработку',
								content: 'Успешно отправлен!',
							});
							
							$("#order-"+id).remove();
							
						}
					});
				}
			},
			cancel: {
				text: 'Отмена',
				btnClass: 'btn-default',
				action: function () {

				}
			}
		}
	});
	return false;
});


$(document).on('click', '.order-accept', function (e) {
	e.preventDefault();

	var url = $(this).attr('href');
	var id = $(this).attr('data-order');
	$.confirm({
		title: 'Подтвердите исполнение заказа!',
		icon: 'fa fa-question-circle-o',
		scrollToPreviousElement: false,
		closeIcon: true,
		animation: 'zoom',
		theme: 'modern',
		content: '',
		buttons: {
			confirm: {
				text: 'Подтверждаю',
				btnClass: 'btn-orange',
				action: function(){
					$.ajax({
						url: url,
						type: 'post',
						dataType: 'json',
						data: {
							id: id,
							YUPE_TOKEN: yupeToken
						},
						complete: function(){
							
							$.alert({
								title: 'Подтверждение исполнения заказа',
								content: 'Заказ принят!',
							});
							
							$("#order-"+response.responseJSON).remove();
							
						}
					});
				}
			},
			cancel: {
				text: 'Отмена',
				btnClass: 'btn-default',
				action: function () {

				}
			}
		}
	});
	return false;
});

function decisionForm(form, data, hasError) {
	
    if (hasError) return false;

	var url = form.attr('action');
	
    $.ajax({
        url: url,
        type: 'POST',
        data: form.serialize(),
        success: function (response) {
 
        },
        error: function () {
 
        }
    });
    return false;
}