<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Претензии по заказам') => ['/orderplatform/admin/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Управление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Претензии по заказам - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Комиссиями для авторов'), 'url' => ['/orderplatform/commissionAuthor/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Комиссию для авторов'), 'url' => ['/orderplatform/commissionAuthor/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Претензии по заказам'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'управление'); ?></small>
    </h1>
</div>
 
<?php
 $this->widget(
    'yupe\widgets\CustomEmptyGrid',
    [
        'id'           => 'claims-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
	 	'actionsButtons' => [

		],
        'columns'      => [
			[
				'header' => '№ претензии',
				'value' => '$data->id',
			],
	 		[
				'header' => 'Логин пользователя',
				'value' => '$data->user->nick_name'
			],
	 	 	[
				'header' => 'Жалобу направил',
				'value' => function($data){
	 				return ($data->user->type_account == 1 ) ? 'клиент' : 'автор';
 				},
			],
	 
	 		[
				'header' => 'Претензия',
				'value' => '$data->comment',
			],
	 
	 		[
				'header' => 'Статус',
				'value' => '$data->getStatus()',
			],
	 
	 		[
                'class'       => 'yupe\widgets\CustomButtonColumn',
                'template'    => '{viewOrder}',
                'buttons'     => [
                    'viewOrder'       => [
                        'icon'  => 'fa fa-eye',
                        'label' => 'История заказа',
                        'url'   => 'array("/orderplatform/adminBackend/viewOrder", "id" => $data->id)',
                        'options' => ['class' => 'btn btn-sm btn-default']
                    ],
                   
                ],
            ],
        ],
    ]
); ?>
