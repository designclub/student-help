<?php

/**
 * This is the model class for table "{{arbitration_court}}".
 *
 * The followings are the available columns in table '{{arbitration_court}}':
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $date_result
 * @property string $comment
 * @property string $final_decision
 * @property integer $status
 * @property integer $user_id
 * @property integer $order_id
 *
 * The followings are the available model relations:
 * @property OrdersPlatform $order
 * @property UserUser $user
 */
	 
class ArbitrationCourt extends yupe\models\YModel
{
	
	const STATUS_CREATE = 1; //направлена
	const STATUS_VIEW = 2;	//на рассмотрении
	const STATUS_FINISH = 3; //завершено
	
	public $idUser = null;
 
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{arbitration_court}}';
	}

	/**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('comment', 'required', 'on' => 'user'),
            array('final_decision', 'required', 'on' => 'admin'),
            array('status, user_id, order_id', 'numerical', 'integerOnly'=>true),
            array('date_update, date_result, comment, final_decision', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, date_create, date_update, date_result, comment, final_decision, status, user_id, order_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date_create' => 'Дата направления заявки',
            'date_update' => 'Дата обновления заявки',
            'date_result' => 'Дата закрытия заявки',
            'comment' => 'Претензия',
            'final_decision' => 'Решение',
            'status' => 'Статус',
            'user_id' => 'Пользователь',
            'order_id' => 'Заказ',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('date_create',$this->date_create,true);
        $criteria->compare('date_update',$this->date_update,true);
        $criteria->compare('date_result',$this->date_result,true);
        $criteria->compare('comment',$this->comment,true);
        $criteria->compare('final_decision',$this->final_decision,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('order_id',$this->order_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

	public function beforeSave()
	{
		
		if (is_null( $this->idUser)){
			$this->idUser = Yii::app()->getUser()->getId();
		}
		
		$user = User::model()->loadClient($this->idUser);
 	
        Yii::app()->db->createCommand('SET time_zone = "'.$user->timeZone->name_zone.'"')->execute();
		
		if ($this->isNewRecord){
			$this->user_id   = Yii::app()->getUser()->getId();
			$this->date_create = new CDbExpression('NOW()');
			$this->status   = self::STATUS_CREATE;
		}
		else{
			$this->date_update = new CDbExpression('NOW()');
		}

		return parent::beforeSave();
	}
	
	public function getStatusList()
    {
        return [
            self::STATUS_CREATE => 'Новая претензия',
            self::STATUS_VIEW =>  'Заявка на рассмотрении',
            self::STATUS_FINISH => 'Заявка завершена',
        ];
    }
	
	public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status])
            ? $data[$this->status]
            : Yii::t('UserModule.user', 'status is not set');
    }
	
	public function loadOrder( $id ) {
		$model = OrdersPlatform::model()->findByPk( $id );
		if ( $model === null )
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.' ) );

		return $model;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArbitrationCourt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
