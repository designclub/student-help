<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Комиссии для авторов') => ['/orderplatform/commissionAuthor/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Добавление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Комиссии для авторов - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Комиссиями для авторов'), 'url' => ['/orderplatform/commissionAuthor/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Комиссию для авторов'), 'url' => ['/orderplatform/commissionAuthor/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Комиссии для авторов'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>