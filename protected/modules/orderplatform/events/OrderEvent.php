<?php
use yupe\ components\ Event;

/**
 * Class ScheduleEvent
 */
class OrderEvent extends Event{
	
	/**
	 * @var Order
	**/
	
	protected $order;

	protected $user;

	public function __construct( OrdersPlatform $order, User $user) {
		$this->order = $order;
		$this->user = $user;
	}

	/**
	 * @param \OrdersPlatform $order
	 */
	public function setOrder( $order ) {
		$this->order = $order;
	}
	
	/**
	* @return \OrdersPlatform
	*/
	public function getOrder() {
		return $this->order;
	}

	/**
	 * @param \User $user
	 */
	public function setUser( $user ) {
		$this->user = $user;
	}

	/**
	 * @return \User
	 */
	public function getUser() {
		return $this->user;
	}
}