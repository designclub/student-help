<?php
/**
* Класс AdminBackendController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     http://yupe.ru
**/
class AdminBackendController extends \yupe\components\controllers\BackController
{
    /**
    * Отображает Комиссию для авторов по указанному идентификатору
    *
    * @param integer $id Идинтификатор Комиссию для авторов для отображения
    *
    * @return void
    */
    public function actionViewOrder($id)
    {
		$claim = $this->loadModel($id);
		$arbitration = new Arbitration;
		$arbitration->claim = $claim->id;
		$arbitration->order = $claim->order_id;
		
        $this->render('viewOrder', ['order' => OrdersPlatform::model()->loadOrder($claim->order_id), 'claim' => $claim, 'arbitration' => $arbitration]);
    }
        
    public function actionIndex()
    {
        $model = new ArbitrationCourt('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('ArbitrationCourt') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('ArbitrationCourt'));
        $this->render('index', ['model' => $model]);
    }
	
	public function actionDecision(){
		$request = Yii::app()->getRequest();

        if (!$request->getIsPostRequest() && !$request->getPost('Arbitration')) {
            throw new CHttpException(404);
        }

        if(Yii::app()->callbackManager->add($request->getPost('Arbitration'), $request->getUrlReferrer()) === true) {
        } else {
        }
	}
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = ArbitrationCourt::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
