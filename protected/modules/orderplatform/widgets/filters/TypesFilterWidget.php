<?php

Yii::import('application.modules.orderplatform.models.*');
Yii::import('application.modules.orderplatform.components.*');

/**
 * Class TypesFilterWidget
 */
class TypesFilterWidget extends \yupe\widgets\YWidget{
	
	public $view = 'types-filter-widget';

    public function run(){
        $filter = new TypesFilter;
        
        if (isset($_POST['TypesOrder'])){
            $filter->save($_POST['TypesOrder']);
        }
 
        $this->render($this->view, [
            'filter' => $filter,
        ]);
    }
}