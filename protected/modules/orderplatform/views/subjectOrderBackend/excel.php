<?php
$form = $this->beginWidget(
	'\yupe\widgets\ActiveForm', [
		'id' => 'excel-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'type' => 'inline',
		'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
		'clientOptions' => [
			'validateOnSubmit' => true,
		],
	]
);
?>
 
<p><div class="row">
	<div class="col-sm-12"></div>
</div></p>
<div class="row">
	<div class="col-sm-8">
		<?= $form->fileFieldGroup(
			$model,
			'name',
			[
				'widgetOptions' => [
					'htmlOptions' => [
					],
				],
			]
		); ?>
	</div>
</div>
<p><div class="row">
	<div class="col-sm-12"></div>
</div></p>
<div class="row">
	<div class="col-sm-8">
		<?php $this->widget(
			'bootstrap.widgets.TbButton',
			[
				'buttonType' => 'submit',
				'context' => 'primary',
				'label' => 'Обновить данные',
			]
		); ?>
	</div>
</div>
<?php $this->endWidget(); ?>