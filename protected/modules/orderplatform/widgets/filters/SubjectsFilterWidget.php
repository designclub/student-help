<?php

Yii::import('application.modules.orderplatform.models.*');
Yii::import('application.modules.orderplatform.components.*');

/**
 * Class SubjectsFilterWidget
 */
class SubjectsFilterWidget extends \yupe\widgets\YWidget{
	
	public $view = 'subjects-filter-widget';

    public function run(){
        $filter = new SubjectsFilter;
        
        if (isset($_POST['SubjectOrder'])){
            $filter->save($_POST['SubjectOrder']);
        }
 
        $this->render($this->view, [
            'filter' => $filter,
        ]);
    }
}