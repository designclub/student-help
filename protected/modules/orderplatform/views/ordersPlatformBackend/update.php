<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    $model->title => ['/orderplatform/ordersPlatformBackend/update', 'id' => $model->id],
    Yii::t('OrderplatformModule.orderplatform', 'Редактирование'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', $model->title);

?>
<div class="page-header">
    <h1>
        &laquo;<?=  $model->title; ?>&raquo;
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>