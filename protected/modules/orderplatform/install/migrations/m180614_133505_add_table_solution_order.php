<?php

class m180614_133505_add_table_solution_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
		//таблица решений по заказам
        $this->createTable('{{solutions_order}}', [
            "id" => "pk",
            "date_finish" => "timestamp NOT NULL",
            "order_id" => "INTEGER(11)  NOT NULL",
            "author_id" => "INTEGER(11)  NOT NULL",
            "solution" => "text NULL",
            "comment" => "text NULL",
        ], $this->getOptions());
	}

	public function safeDown()
	{
		$this->dropTable('{{solutions_order}}');
	}
}