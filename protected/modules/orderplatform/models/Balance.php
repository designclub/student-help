<?php

class Balance extends yupe\models\YFormModel
{

    /**
     * @var
     */
    public $sumBalance;
  

    /**
     * @return array
     */
    public function rules()
    {

        return [
            ['sumBalance', 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'sumBalance' => 'Сумма пополнения',
        ];
    }

    /**
     * @return array
     */
    public function attributeDescriptions()
    {
        return [
            'sumBalance' => ' Введите сумму которую хотите внести',
        ];
    }
	
}
