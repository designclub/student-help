<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OrderplatformModule.orderplatform', 'Пополнение баланса'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Баланс - пополнение');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Баланс'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'пополнение'); ?></small>
    </h1>
</div>

<?php

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'id'                     => 'user-balance-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => ['class' => 'well'],
    ]
);
?>
    <div class="row">
        <div class="col-sm-7">
            <?=  $form->textFieldGroup($model, 'sumBalance', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('sumBalance'),
                        'data-content' => $model->getAttributeDescription('sumBalance')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
            'label'      => Yii::t('OrderplatformModule.orderplatform', 'Пополнить баланс'),
        ]
    ); ?>

<?php $this->endWidget(); ?>