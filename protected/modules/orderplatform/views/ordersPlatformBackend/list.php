<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Заказы') => ['/orderplatform/ordersPlatformBackend/list'],
    Yii::t('OrderplatformModule.orderplatform', 'Аукцион заказов'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Список заказов');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Список заказов'); ?>
    </h1>
</div>

<div class="row list-orders">
		<?php $this->widget(
			'bootstrap.widgets.TbListView',
			array(
				'dataProvider'  => $data,
				'template' => '{items}',
				'itemView'      => '_order-list',
				'viewData' => ['user' => $user],
				'emptyText'		=> 'В настоящий момент заказов нет'
			)
		); ?>
</div>

<?php
	$this->widget('application.modules.orderplatform.widgets.DialogWidget', 
					['view'=>$user->type_account == 1 ? 'dialog' : 'dialogAuthor']); ?>