<div class="block-order" id="order-<?= $data->id; ?>">
	<div class="row">

		<div class="col-sm-9 col-xs-12">
			<h6 class="label label-info">Статус заказа: <?= $data->status->name_status; ?></h6>
			<h5><?= $data->title; ?></h5>
			<div class="row data-order">
				<div class="col-lg-5 col-xs-12">
					<?= $data->subject->name_subject; ?>
				</div>
				<div class="col-lg-5 col-xs-12">
					<?= $data->type->name_type; ?>
				</div>
			</div>
			<div class="row data-order">
				<div class="col-lg-5 col-xs-12">
					<i class="fa fa-clock-o"></i> Срок:
					<?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->target_date); ?>
				</div>
				<div class="col-lg-6 col-xs-12">
					<i class="fa fa-info" aria-hidden="true"></i> Уникальность:
					<?= $data->uniqueness; ?>% (
					<?= $data->uniqCheck->name_check_unique; ?>)
				</div>
			</div>
			<div class="row data-order">
				<?php if(isset($data->author->nick_name)): ?>
				<div class="col-lg-5 col-xs-12">
					<i class="fa fa-user" aria-hidden="true"></i> Автор:
					<?= $data->author->nick_name; ?>
				</div>
				<?php endif; ?>
				<div class="col-lg-5 col-xs-12">
					<?php $class = $data->getCountDialog() > 0 ? 'show' : 'hide' ?>
					<?= CHtml::link("<i class='fa fa-comments-o' aria-hidden='true'></i> <span class='counter-$data->id'>{$data->getCountDialog()}</span>", '#', ['class'=>'link-dialog '.$class, 'data-order' => $data->id, 'data-toggle'=>'modal', 'data-target'=>'#dialogModal', 'link'=>Yii::app()->createAbsoluteUrl('/backend/orderplatform/dialog/dialogOrder')]); ?>
				</div>
			</div>

			<div class="row text-order">
				<div class="col-lg-12">
					<?= CHtml::link('Описание заказа', '#', ['class'=>'link-more']);?>
					<div class="more-text hide">
						<?= $data->text; ?>
						<div class="files-order">
							<?php if (count($data->filesForOrders()) > 0): ?>
							<?php foreach($data->filesForOrders() as $file): ?>
							<?= CHtml::link($file->name, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->fileName, ['target' => '_blank', 'class'=>'btn']); ?>
							<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>

			</div>


			<?php $solutions = $data->solutions($data->id, $data->author_id); ?>
			<?php if (count($solutions)>0): ?>
				<?php foreach($solutions as $solution): ?>
 
					<div class="row rates-block-order">
						<div class="col-lg-12">
							<h4></h4>
						</div>
						<div class="col-lg-12">
							Комментарий: <?= $solution->comment; ?>
						</div>						
						<div class="col-lg-12">
							<div class="files-order">
								<p>Файлы:</p>	
								<?php if (count($solution->filesSolutionsOrder()) > 0): ?>
									<?php foreach($solution->filesSolutionsOrder() as $file): ?>
										<?= CHtml::link($file->title_file, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->name_file, ['target' => '_blank', 'class'=>'files-solutions']); ?>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
						
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>


			<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id < StatusOrder::STATUS_AUTHOR): ?>
			<?php if (count($data->rates()) > 0 && $data->author_id == null): ?>
			<div class="rates-block-order">
				<h4>По заказу сделано ставок: <label><?= count($data->rates); ?></label></h4>
				<?php foreach($data->rates() as $rate): ?>
				<div class="row">
					<div class="col-xs-12">
						Автор <label><?= $rate->author->nick_name;?></label> сделал ставку:
						<?= $cost = round(($rate->rate*(UserBalance::model()->getCommission()/100))+$rate->rate, 1); ?>руб.
					</div>
					<div class="col-xs-12">
						<?php if ($cost > UserBalance::model()->getBalance(Yii::app()->getUser()->getId())): ?>
							<div class="label label-warning label-margin">недостаточно баланса для принятия ставки</div>
							
							<div><?= CHtml::link('Пополнить баланс', Yii::app()->createUrl('/orderplatform/userBalanceBackend/addBalance'), ['class' => 'btn btn-info']); ?></div>
						<?php else: ?>
							<?= CHtml::link('Принять ставку', [Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/relyAccept')], ['class'=>'btn btn-success link-rely-accept', 'data-rate' => $rate->id]); ?>
						<?php endif; ?>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
			<!-- end rely-block -->
			<?php endif; ?>
			<?php endif; ?>

		</div>


		<div class="col-sm-3 col-xs-12">
			<div class="buttons">

				<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id < StatusOrder::STATUS_AUTHOR): ?>
				<?= CHtml::link('<span class="icon-pencil2"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/update', ['id'=>$data->id]), ['class'=>'btn', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Редактировать"]); ?>

				<?= CHtml::link('<span class="icon-garbage"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/delete', ['id'=>$data->id]), ['data-order' => $data->id, 'class'=>'btn btn-delete', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Удалить"]); ?>
				<?php endif; ?>


				<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id == StatusOrder::STATUS_AUTHOR && $data->author_id == Yii::app()->getUser()->getId()): ?>

				<?= CHtml::link('<span class="icon-cross"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/cancelOrder', ['id'=>$data->id]), ['class'=>'btn order-no-work', 'data-order' => $data->id, 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Отказаться от заказа"]); ?>

				<?= CHtml::link('<span class="icon-checkmark"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/confirmOrder', ['id'=>$data->id]), ['class'=>'btn order-to-work', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Взять в работу"]); ?>
				<?php endif; ?>

				<!--направление жалобы-->
				<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id >= StatusOrder::STATUS_WORK && $data->status_id <= StatusOrder::STATUS_CHECK ): ?>

				<?= CHtml::link('<i class="fa fa-undo" aria-hidden="true"></i>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/claim', ['id'=>$data->id]), ['data-order' => $data->id, 'class'=>'btn btn-claim', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Направить претензию по заказу"]); ?>
				<?php endif; ?>
				
				<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id == StatusOrder::STATUS_CHECK && $data->client_id == Yii::app()->getUser()->getId()): ?>

				<?= CHtml::link('<span class="icon-reply"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/correctionOrder', ['id'=>$data->id]), ['class'=>'btn order-to-correctionSSS', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Отправить на доработку"]); ?>

				<?= CHtml::link('<span class="icon-checkmark2"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/acceptOrder', ['id'=>$data->id]), ['class'=>'btn order-accept', 'data-order' => $data->id, 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Принять заказ"]); ?>

				<?php endif; ?>
			</div>
		</div>

	</div>

	<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id == StatusOrder::STATUS_WORK && $data->author_id == Yii::app()->getUser()->getId()): ?>
	<div class="row buttons-order rates-block rates-block-order">
		<div class="col-sm-12 col-xs-12">

			<?= CHtml::link('Выполнить заказ', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/makeOrder', ['id'=>$data->id]), ['class'=>'btn btn-success']); ?>

		</div>
	</div>
	<?php endif; ?>
	
	<?php if ($data->status_id != StatusOrder::STATUS_DELETE && $data->status_id == StatusOrder::STATUS_CORRECTION && $data->author_id == Yii::app()->getUser()->getId()): ?>
	<div class="row buttons-order rates-block rates-block-order">
		<div class="col-sm-12 col-xs-12">
 			<?php foreach($data->correctionsOrder() as $correction): ?>
 				<p><label>Комментарий от заказчика:</label> <?= $correction->comment; ?></p>
 			<?php endforeach; ?>
			<?= CHtml::link('Исправить заказ', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/makeOrder', ['id'=>$data->id, 'correction' => $correction->id]), ['class'=>'btn btn-success']); ?>
			
		</div>
	</div>
	<?php endif; ?>

</div>