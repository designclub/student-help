<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OrderplatformModule.orderplatform', 'Баланс'),
    Yii::t('OrderplatformModule.orderplatform', 'Ошибка пополнения'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Баланс - пополнение');

?>
<div class="page-header">
    <h2>
        Ошибка пополнения баланса!
    </h2>
</div>
