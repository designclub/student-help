﻿<div class="rates-block-order">
	<h4>По заказу сделано ставок: <label><?= count($order->rates); ?></label></h4>
	<?php foreach($order->rates() as $rate): ?>
	<div class="row">
		<div class="col-xs-12">
			Автор <label><?= $rate->author->nick_name;?></label> сделал ставку:
			<?= $cost = round(($rate->rate*(UserBalance::model()->getCommission()/100))+$rate->rate, 1); ?>руб.
		</div>
	</div>
	<?php endforeach; ?>
</div>
<!-- end rely-block -->