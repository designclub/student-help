<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model OrdersPlatform
 *   @var $form TbActiveForm
 *   @var $this OrdersPlatformBackendController
 **/
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm', [
		'id' => 'orders-platform-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'htmlOptions' => [ 'class' => 'well', 'enctype' => 'multipart/form-data' ],
	]
);
?>

<?= $form->errorSummary($model) ?>

<div class="alert alert-info">
	<?=  Yii::t('OrderplatformModule.orderplatform', 'Поля, отмеченные'); ?>
	<span class="required">*</span>
	<?=  Yii::t('OrderplatformModule.orderplatform', 'обязательны.'); ?>
</div>

<div class="row">
	<div class="col-sm-6 col-xs-12">
		<?=  $form->dropDownListGroup($model, 'type_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(TypesOrder::model()->findAll(), 'id', 'name_type'),
						'htmlOptions' => [
							'empty'=> '-- Выбрать тип работы --'
						]
                    ],             
                ]); ?>
	</div>
	<div class="col-sm-6 col-xs-12">
		<?= $form->dropDownListGroup($model, 'subject_id', [
                    'widgetOptions' => [
                        'data' => Yii::app()->getComponent('platformRepository')->getFormattedListSubjects(),
						'htmlOptions' => [
							'empty'=> '-- Выбрать предмет работы --'
						]
                    ]
                ]); ?>
	</div>
</div>
 
<div class="row">
	<div class="col-sm-12">
		<?= $form->datePickerGroup(
                $model,
                'target_date',
                [
                    'widgetOptions' => [
                        'options' => [
                            'format' => 'dd-mm-yyyy',
                            'weekStart' => 1,
                            'autoclose' => true,
                            'minDate' => date('d-m-Y'),
                            'startDate'=>date("d-m-Y"),
                        ],
                        'htmlOptions'=>['placeholder'=>'']
                    ],
                    'prepend' => '<span class="calendar-picker"><i class="fa fa-calendar"></i></span>',
                ]
                ); ?>
	</div>
</div>

<!--<div class="row">
	<div class="col-sm-12">
		<?=  $form->textFieldGroup($model, 'theme', [
					'widgetOptions' => [
						'htmlOptions' => [
							'class' => 'popover-help',
							'data-original-title' => $model->getAttributeLabel('theme'),
							'data-content' => $model->getAttributeDescription('theme')
						]
					]
				]); ?>
	</div>
</div>-->

<div class="row">
	<div class="col-sm-12">
		<?=  $form->textFieldGroup($model, 'title', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('title'),
                        'data-content' => $model->getAttributeDescription('title')
                    ]
                ]
            ]); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<?=  $form->textAreaGroup($model, 'text', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
					'placeholder'=>'Дополнительная информация для автора',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text'),
                    'data-content' => $model->getAttributeDescription('text')
                ]
            ]]); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-4 col-xs-12">
		<?=  $form->textFieldGroup($model, 'budget', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'placeholder' => 'Укажите желаемую сумму за работу',
                    ]
                ]
            ]); ?>
	</div>
	<div class="col-sm-4 col-xs-12">
		<?=  $form->textFieldGroup($model, 'count_page_from', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('count_page_from'),
                        'data-content' => $model->getAttributeDescription('count_page_from')
                    ]
                ]
            ]); ?>
	</div>
	<div class="col-sm-4 col-xs-12">
		<?=  $form->textFieldGroup($model, 'count_page_to', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('count_page_to'),
                        'data-content' => $model->getAttributeDescription('count_page_to')
                    ]
                ]
            ]); ?>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<?=  $form->dropDownListGroup($model, 'uniq_check_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(UniqueCheck::model()->findAll(), 'id', 'name_check_unique'),
						
                    ]
                ]); ?>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<?=  $form->textFieldGroup($model, 'uniqueness', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'placeholder' => 'Укажите процент уникальности / оригинальности работы',
                    ]
                ]
            ]); ?>
	</div>
</div>

<div class='row'>
	<?php if (count($model->filesForOrders()) > 0): ?>
		<?php foreach($model->filesForOrders() as $file): ?>
		<div class="col-sm-6">
			<?= CHtml::link($file->name, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->fileName, ['target' => '_blank']); ?>
		</div>
		<div class="col-sm-2">
			<?= CHtml::link('<span class="icon-cross"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/deleteFile', ['id'=>$file->id]), ['class'=>'btn delete-file', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Удалить файл"]); ?>
		</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>

<div class='row'>
	<div class="col-sm-12">
		<?= $form->fileFieldGroup(
                $model,
                'files[]',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'multiple' => true
                            // 'onchange' => 'readURL(this);',
                            // 'style' => 'background-color: inherit;',
                        ],
                    ],
                ]
            ); ?>
		<p>Загрузить можно не более 5-ти файлов</p>

	</div>
</div>

<?php
$this->widget(
	'bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index' ],
		'label' => Yii::t( 'OrderplatformModule.orderplatform', 'Разместить заказ в аукционе' ),
	]
);
?>

<?php $this->endWidget(); ?>


<script type='text/javascript'>
/*	$( document ).ready( function () {
		$('.calendar-picker').click(function(){
			$('#OrdersPlatform_target_date').datepicker({'format':'dd\x2Dmm\x2Dyyyy','weekStart':1,'autoclose':true,'minDate':'21\x2D03\x2D2019','startDate':'21\x2D03\x2D2019','language':'ru'});
		});
		
		$('.calendar-picker').datepicker({'format':'dd\x2Dmm\x2Dyyyy','weekStart':1,'autoclose':true,'minDate':'21\x2D03\x2D2019','startDate':'21\x2D03\x2D2019','language':'ru'});
		});
	});*/
</script>



<?php Yii::app()->clientScript->registerScript('report', "

		$('#OrdersPlatform_type_id').chosen({
			placeholder_text_multiple: 'Выбрать тип работы',
			placeholder_text_single: 'Выбрать тип работы',
			max_selected_options: 1,
			allow_single_deselect: true,
		});
		
		$('#OrdersPlatform_subject_id').chosen({
			placeholder_text_multiple: 'Выбрать предмет работы',
			placeholder_text_single: 'Выбрать предмет работы',
			max_selected_options: 1,
			allow_single_deselect: true,
		});
		
    ", CClientScript::POS_READY);
?>