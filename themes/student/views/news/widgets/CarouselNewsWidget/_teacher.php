<div>
	<div class="teacher-item-carousel">
		<div class="row">
			<div class="col-sm-3 col-xs-12">
				<img src="<?= $data->getImageUrl(); ?>" alt="<?= $data->title; ?>" />
			</div>
			<div class="col-sm-9 col-xs-12 teacher-description">
				<h4><?= $data->title; ?></h4>
				<label><?= $data->short_text; ?></label>
				<div class="news-carousel-text">
					<?php 
						//$str = iconv("UTF-8","windows-1251", $data->full_text);
						$str = $data->full_text;
						$str = substr($str, 0, 450);
						$str = rtrim($str, "!,.-");
						$str = substr($str, 0, strrpos($str, ' '));
						//$str = iconv("windows-1251", "UTF-8", $str);
						$str .= "...";
						echo $str;
					?>
					<div class="window-teacher">
						<?= CHtml::link('<i class="fa fa-play" aria-hidden="true"></i>Прослушать приветствие', '/audio/'.$data->slug.'.mp3', ['class'=>'track trckmp window-teacher-link']); ?>
					</div>
					<div class="window-teacher window-teacher-left">
						<a href="#" class="window-teacher-link" data-toggle="modal" data-target="#teachers">Подробнее</a>
						<div class="text-teacher hidden"><?= $data->full_text; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>