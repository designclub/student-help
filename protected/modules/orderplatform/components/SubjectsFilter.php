<?php

/**
 * Class SubjectsFilter
 *
 */
class SubjectsFilter extends CApplicationComponent
{

	public $data = [];

    public function init(){
        $this->getData();
    }

    public function save($field){
        $user = Yii::app()->user->getProfile();
        $user->options_subjects_work = serialize($field);
        return $user->save();
    }
	
    public function getData($user = null){
		if (is_null($user)){
			$user = Yii::app()->user->getProfile();
		}
		if ($user->options_subjects_work){
			return $this->data = unserialize($user->options_subjects_work);
		}
		
        return [];
    }

    public function getValue($id, $name){
        $data = $this->data;
        return ArrayHelper::getValue($data, "{$id}.".$name);
    }
	
    public function filter($criteria){
        $data = $this->getFilterData();
		$filterCondition = new CDbCriteria();
		
		foreach ($data as $filter){

			$subjectId = $filter['subject_id'];
 
			$newCriteria = new CDbCriteria();
			$newCriteria->addCondition('subject_id = ' . $filter['subject_id']);
			
			$filterCondition->mergeWith($newCriteria, 'OR');

		}
		
		$criteria->mergeWith($filterCondition, 'AND');
 
        return $criteria;
    }
	
	public function getUsers($category, $count){
		
		$users = User::model()->findAllByAttributes(['type_account' => USER::TYPE_AUTHOR]);
		$usersStore = [];

		foreach($users as $user){
			
			$is_store = false;
			$userFilter = new self;
			$data = $userFilter->getData($user);
 
			$isArray = [];
			foreach (new RecursiveIteratorIterator(new RecursiveArrayIterator($data), RecursiveIteratorIterator::LEAVES_ONLY) as $key => $value) {
				if ('subject_id' === $key) {
					$isArray[] = $value;
				}
			}
			
			if (count($isArray) > 0){
				foreach($data as $item){
					if(isset($item['subject_id']) && $category == $item['subject_id']){
						$is_store = true;
					}
				}
			}
			else{
				$is_store = true;
			}
			
			if($is_store){
				$usersStore[$user->id] = $user;
			}
		}
		
		return $usersStore;
	}

    public function getFilterData(){
        $data = [];

        foreach ($this->data as $key => $d) {

            $subjectId = ArrayHelper::getValue($d, 'subject_id');
			
            if ($subjectId) {
                $data[$key] = $d;
            }
        }

        return $data;
    }

}