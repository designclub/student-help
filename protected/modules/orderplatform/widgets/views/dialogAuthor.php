
   <div id="dialogModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content"  id="modal-body-content-text">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
                        aria-label="Закрыть окно">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body dialog-body">
                 
            </div>
            
            <div class="modal-footer">
				<div class="row">
					<?php $form = $this->beginWidget(
						'bootstrap.widgets.TbActiveForm',
						[
							'id' => 'dialog-form',
							'type' => 'inline',
							'action' => Yii::app()->createUrl('/backend/orderplatform/dialog/send'),
							'enableClientValidation' => true,
							'clientOptions' => [
								'validateOnSubmit' => true,
								'afterValidate' => 'js:SendMessage'
							],
						]
						); ?>

						<?= $form->hiddenField($dialog, 'order_id'); ?>
						<?= $form->hiddenField($dialog, 'user_to'); ?>
						
						<div class="col-sm-10">
							<?= $form->textAreaGroup($dialog, 'message'); ?>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-success" id="message-send">
								<i class="fa fa-share" aria-hidden="true"></i>
							</button>
						</div>
					<?php $this->endWidget(); ?>
				</div>
<!--				<div class="row">
					<div class="col-sm-12">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					</div>
				</div>-->
                
            </div>

        </div>
    </div>
</div>