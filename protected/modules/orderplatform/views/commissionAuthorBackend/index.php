<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Комиссии для авторов') => ['/orderplatform/commissionAuthor/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Управление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Комиссии для авторов - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Комиссиями для авторов'), 'url' => ['/orderplatform/commissionAuthor/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Комиссию для авторов'), 'url' => ['/orderplatform/commissionAuthor/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Комиссии для авторов'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'управление'); ?></small>
    </h1>
</div>

<p> <?=  Yii::t('OrderplatformModule.orderplatform', 'В данном разделе представлены средства управления Комиссиями для авторов'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'commission-author-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'name_level',
            'comission_percent',
            'comission_sum',
            'count_order',
            'sum_orders',
//            'count_comments',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
