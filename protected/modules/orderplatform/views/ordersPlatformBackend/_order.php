<div class="block-order">
	<div class="row">
		<div class="col-lg-12">
			<h5><?= $data->title; ?></h5>
		</div>
		<div class="row data-order">
			<div class="col-lg-3">
				<?= $data->subject->name_subject; ?>
			</div>
			<div class="col-lg-3">
				<?= $data->type->name_type; ?>
			</div>
			<div class="col-lg-3">
				Статус: <?= $data->status->name_status; ?>
			</div>
		</div>
	</div>

	<div class="row options-order">
		<div class="col-lg-3">
			<i class="fa fa-clock-o"></i>
			Срок: <?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->target_date); ?>
		</div>

		<div class="col-lg-4">
			<i class="fa fa-info" aria-hidden="true"></i>
			Уникальность: <?= $data->uniqueness; ?>% (<?= $data->uniqCheck->name_check_unique; ?>)
		</div>
	</div>

	<div class="row text-order">
		<div class="col-lg-12">
			<?= CHtml::link('Описание заказа', '#', ['class'=>'link-more']);?>
			<div class="more-text hide">
				<?= $data->text; ?>
			</div>
		</div>
	</div>

	<div class="row buttons-order">
		<div class="col-lg-4">
			<?= CHtml::link('Редактировать', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/update', ['id'=>$data->id]), ['class'=>'btn btn-warning']); ?>
		</div>
		<div class="col-lg-4">
			<?= CHtml::link('Удалить', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/delete', ['id'=>$data->id]), ['class'=>'btn btn-danger']); ?>
		</div>
		<?php if ($data->status_id == 1): ?>
		<div class="col-lg-4">
			<?= CHtml::link('Разместить на аукционе', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/toAuction', ['id'=>$data->id]), ['class'=>'btn btn-primary']); ?>
		</div>
		<?php endif; ?>
	</div>
</div>