<?php

/**
 * This is the model class for table "{{order_correction}}".
 *
 * The followings are the available columns in table '{{order_correction}}':
 * @property integer $id
 * @property string $date_create
 * @property string $comment
 * @property integer $client_id
 * @property integer $order_id
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property UserUser $client
 * @property OrdersPlatform $order
 */
class OrderCorrection extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order_correction}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comment', 'required'),
			array('client_id, order_id, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_create, comment, client_id, order_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'client' => array(self::BELONGS_TO, 'User', 'client_id'),
			'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_create' => 'Date Create',
			'comment' => 'Комментарий',
			'client_id' => 'Client',
			'order_id' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('order_id',$this->order_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave()
	{
		
		$user = $this->loadClient(Yii::app()->getUser()->getId());
 	
        Yii::app()->db->createCommand('SET time_zone = "'.$user->timeZone->name_zone.'"')->execute();

		$this->client_id   = Yii::app()->getUser()->getId();
		$this->date_create = new CDbExpression('NOW()');

		return parent::beforeSave();
	}
	
	private function loadClient( $id ) {
		$model = User::model()->findByPk( $id );
		if ( $model === null )
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.' ) );

		return $model;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderCorrection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
