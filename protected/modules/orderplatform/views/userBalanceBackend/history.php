<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    Yii::t('OrderplatformModule.orderplatform', 'История операций'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'История операций');

?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'История операций'); ?>
    </h1>
</div>

<div class="row">
	<div class="col-xs-12">

			<?php $this->widget(
				'yupe\widgets\CustomEmptyGrid',
				[
					'id' => 'history-grid',
					'dataProvider' => $history->historyBalances(),
					'type'         => 'striped condensed',
					'template'=>'{items}{pager}',
					 'actionsButtons' => [

					],
					'columns' => [
						[
							'header' => 'Дата операции',
							'value' => 'Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->date)',
						],
						[
							'header' => 'Операция',
							'value' => '$data->getOperation()',
						],						
						[
							'header' => 'Комментарий',
							'value' => '$data->comment',
						],
						[
							'header' => 'Сумма',
							'value' => function($data){
								return round($data->money, 0);
							} 
						],
						[
							'header' => 'Текущий баланс',
							'value' => function($data){
								return round($data->balance, 0);
							} 
						]
					],
				]
			); ?>

	</div>
</div>