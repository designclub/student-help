<?php
/**
* Отображение для client/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     http://yupe.ru
**/
$this->pageTitle = Yii::t('ClientModule.client', 'client');
$this->description = Yii::t('ClientModule.client', 'client');
$this->keywords = Yii::t('ClientModule.client', 'client');

$this->breadcrumbs = [Yii::t('ClientModule.client', 'client')];
?>

<h1>
    <small>
        <?php echo Yii::t('ClientModule.client', 'client'); ?>
    </small>
</h1>