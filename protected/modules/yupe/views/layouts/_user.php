<div class="col-lg-3 col-md-3 сol-sm-4 hidden-xs panel-menu">
	<div class="menu-customer">
		<ul>
			<?php if(Yii::app()->user->getProfile()->type_account == 1): ?>

			<?php endif; ?>

			<?php if(Yii::app()->user->getProfile()->type_account == 2): ?>
			<li>
				<?= CHtml::link('<i class="fa fa-list-alt" aria-hidden="true"></i> Аукцион', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/auction')); ?>
			</li>
			<?php endif; ?>
			<li>
				<?= CHtml::link('<i class="fa fa-th-list" aria-hidden="true"></i> Мои заказы', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list')); ?>
				<ul>
					
					<?php if(Yii::app()->user->getProfile()->type_account == 1): ?>
					<li>
						<?= CHtml::link('В аукционе', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'auction'])); ?>
					</li>
					<?php endif; ?>

					<?php if(Yii::app()->user->getProfile()->type_account == 1): ?>
					<li>
						<?= CHtml::link('Отправлены автору', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'author'])); ?>
					</li>
					<?php endif; ?>

					<?php if(Yii::app()->user->getProfile()->type_account == 2): ?>
					<li>
						<?= CHtml::link('Присланы заказчиком', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'author'])); ?>
					</li>
					<?php endif; ?>


					<li>
						<?= CHtml::link('В работе', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'work'])); ?>
					</li>
					<li>
						<?= CHtml::link('На проверку', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'check'])); ?>
					</li>
					<li>
						<?= CHtml::link('На доработке', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'correction'])); ?>
					</li>
					<li>
						<?= CHtml::link('Архив', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/list', ['type'=>'archive'])); ?>
					</li>
				</ul>
			</li>
			<li>
			<?= CHtml::link('<i class="fa fa-rub" aria-hidden="true"></i> История операций', Yii::app()->createUrl('/orderplatform/userBalanceBackend/history')); ?>
			</li>
			<li><i class="fa fa-comments-o" aria-hidden="true"></i><a href="#">Контакты</a>
			</li>
			<li><i class="fa fa-question" aria-hidden="true"></i><a href="#">Помощь</a>
			</li>
			<li>
				<?= CHtml::link('<i class="fa fa-signal" aria-hidden="true"></i> Статистика', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/statistics')); ?>
			</li>
			
		</ul>
	</div>
</div> <!-- end panel-container -->

<div class="col-lg-9 col-md-9 col-sm-8 panel-container">
	<div id="content">
		<div class="row">
			<?= $content; ?>
		</div>
	</div>
</div> <!-- end panel-container -->