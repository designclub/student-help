<?php

Yii::import('application.modules.notify.models.NotifySettings');

/**
 * Class UserManagerListener
 */
class OrderListener
{
    /**
     * @param OrderEvent $event
     */
    public static function onOrderRely(OrderEvent $event)
    {
		Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'rely order {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/rely',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser()
			]
        );
    }

    /**
     * @param OrderEvent $event
     */
    public static function onOrderAuthorApply(OrderEvent $event)
    {
        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'client apply author {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/authorApply',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser()
            ]
        );
    }   
	
	/**
     * @param OrderEvent $event
     */
    public static function onOrderAuthorConfirm(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'author confirm order {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/authorConfirm',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }	
	
	/**
     * @param OrderEvent $event
     */
    public static function onOrderAuthorCancel(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'author cancel order {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/authorCancel',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }		
	/**
     * @param OrderEvent $event
     */
    public static function onOrderCheck(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'author send order check {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/check',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }	
	
	/**
     * @param OrderEvent $event
     */
    public static function onOrderCorrection(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'client return order to correction {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/correction',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }
	
	/**
     * @param OrderEvent $event
     */
    public static function onOrderApply(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'client apply order {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/apply',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }
	
	/**
     * @param OrderEvent $event
     */
    public static function onOrderDispute(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'client open dispute {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/dispute',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }
	
	/**
     * @param OrderEvent $event
     */
    public static function onOrderDelay(OrderEvent $event)
    {

        Yii::app()->notify->send(
            $event->getUser(),
            Yii::t(
                'OrderplatformModule.orderplatform',
                'author delay order {site}',
                ['{site}' => Yii::app()->getModule('yupe')->siteName]
            ),
            '/email/delay',
            [
                'order' =>  $event->getOrder(),
				'user' => $event->getUser(),
            ]
        );
    }
}
