<div class="row list-orders list-orders-panel">
	<h4>Мои заказы</h4>
	<?php $this->widget(
		'bootstrap.widgets.TbListView',
		array(
			'dataProvider'  => $orders,
			'template' => '{items}',
			'itemView'      => '_order-list-author',
			'viewData' => ['user' => Yii::app()->getUser()],
			'emptyText'		=> 'В настоящий момент заказов нет'
		)
	); ?>
</div>

<?php
	$this->widget('application.modules.orderplatform.widgets.DialogWidget', 
					['view'=>Yii::app()->user->getProfile()->type_account == 1 ? 'dialog' : 'dialogAuthor']); ?>