<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Баланс') => ['/lessonschedule/userBalance/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Добавление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Баланс - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Балансом'), 'url' => ['/lessonschedule/userBalance/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Баланс'), 'url' => ['/lessonschedule/userBalance/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Баланс'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>