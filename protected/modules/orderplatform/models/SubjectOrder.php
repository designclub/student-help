<?php

/**
 * This is the model class for table "{{subject_order}}".
 *
 * The followings are the available columns in table '{{subject_order}}':
 * @property integer $id
 * @property string $name_subject
 *
 * The followings are the available model relations:
 * @property OrdersPlatform[] $ordersPlatforms
 */
class SubjectOrder extends yupe\models\YModel{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subject_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_subject', 'length', 'max'=>255),
			array('parent_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_subject, parent_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ordersPlatforms' => array(self::HAS_MANY, 'OrdersPlatform', 'subject_id'),
			'parent' => [self::BELONGS_TO, 'SubjectOrder', 'parent_id'],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Подкатегория предмета',
			'name_subject' => 'Название предмета',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_subject',$this->name_subject,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getChildTree($filter, $child){
		
		$data = "";
		
		if (!empty($child)){
			$criteria = new CDbCriteria;
			$criteria->condition = 'parent_id = :parent';
			$criteria->params = [':parent' => $child];
			$models = self::model()->findAll($criteria);
		}
		
		$data .= CHtml::openTag('div' , ['class' => 'col-sm-12 main-category']);
		$data .= CHtml::openTag('label');
		$data .= CHtml::checkBox( "SubjectOrder[{$child}][subject_id]", $filter->getValue($child, 'subject_id' ), ['
			value' => $child, 'class' => 'subject-parent']);
		$data .= " Выбрать все предметы";
		$data .= CHtml::closeTag( 'label' );
		$data .= CHtml::closeTag('div');
		
		foreach ( $models as $key => $model ) {
 
			$data .= CHtml::openTag( 'div' , ['class' => 'col-sm-6 sub-category']);
			$data .= CHtml::openTag( 'label' );
			$data .= CHtml::checkBox( "SubjectOrder[{$model->id}][subject_id]", $filter->getValue($model->id, 'subject_id' ), ['
			value' => $model->id, 'data-subject-parent' => $child]);
			$data .= " {$model->name_subject} ";
			$data .= CHtml::closeTag( 'label' );
			
			$data .= CHtml::closeTag( 'div' );
		}

		return $data;
		
	}
	
	public function getTree( $filter, $child = null ) {
		
		$container = "";
		$data = ""; $i = 0;
		
		$tabContent = "";
		
		$filter->getData();
		
		$criteria = new CDbCriteria;
		$criteria->condition = 'parent_id IS NULL';
		$models = self::model()->findAll( $criteria );
		
		$data .= CHtml::openTag('ul', ['class' => 'nav-vertical']);
		$tabContent .= CHtml::openTag('div', ['class' => 'tab-content']);
		
		foreach($models as $key => $model){
		
			$data .= CHtml::openTag('li', ['role' => 'subject', 'class' => ($i==0) ? 'active' : '']);
			
			$data .= CHtml::link($model->name_subject, '#subject-'.$model->id, ["aria-controls"=>"subject", "role"=>"subject", "data-toggle"=>"tab"]);

			$data .= CHtml::closeTag('li');
			
			$childs = self::model()->findAllByAttributes(['parent_id' => $model->id]);
			
			if(count($childs)>0){
				$tabContent .= CHtml::openTag('div', ['role' => 'subject', 'id' => 'subject-'.$model->id, 'class' => 'tab-pane '.(($i==0) ? 'active' : '')]);
					$tabContent .= $this->getChildTree($filter, $model->id);
				$tabContent .= CHtml::closeTag('div');
			}
			++$i;
		}
		
		$data .= CHtml::closeTag('ul');
		
		$container .= CHtml::openTag('div', ['class' => 'row']);
		$container .= CHtml::openTag('div', ['class' => 'col-sm-4 col-xs-12']);
		$container .= $data;
		$container .= CHtml::closeTag('div');
		$container .= CHtml::openTag('div', ['class' => 'col-sm-8 col-xs-12']);
		$container .= $tabContent;
		$container .= CHtml::closeTag('div');
		$container .= CHtml::closeTag('div');
		
		return $container;
	}
	
    public function getParentName($empty = '---')
    {
        return isset($this->parent) ? $this->parent->name_subject : $empty;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubjectOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
