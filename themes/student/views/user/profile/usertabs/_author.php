﻿<div class="row">
	<div class="col-xs-12">
		<?php if (Yii::app()->user->getProfile()->type_account == User::TYPE_AUTHOR): ?>
			<?php $this->widget('application.modules.orderplatform.widgets.filters.TypesFilterWidget') ?>
		<?php endif ?>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<?php if (Yii::app()->user->getProfile()->type_account == User::TYPE_AUTHOR): ?>
			<?php $this->widget('application.modules.orderplatform.widgets.filters.SubjectsFilterWidget') ?>
		<?php endif ?>
	</div>
</div>