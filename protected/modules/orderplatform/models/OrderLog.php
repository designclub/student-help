<?php

/**
 * This is the model class for table "{{order_log}}".
 *
 * The followings are the available columns in table '{{order_log}}':
 * @property integer $id
 * @property string $date_event
 * @property string $text_order
 * @property string $comment
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property OrdersPlatform $order
 */
class OrderLog extends yupe\models\YModel
{
	const ORDER_TO_AUTHOR = 'Передано автору в работу';
	
	const ORDER_TO_EDIT = 'Отправлено на доработку';
	
	const ORDER_DISPUTE = 'Открыт спор';
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{order_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id', 'required'),
			array('order_id, user_id, status', 'numerical', 'integerOnly'=>true),
			array('text_order, comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_event, text_order, comment, order_id, user_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
		);
	}

	public function beforeSave() {


		if ($this->isNewRecord){
			$this->user_id   = Yii::app()->getUser()->getId();
			$this->date_event = new CDbExpression('NOW()');
		}

		return parent::beforeSave();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_event' => 'Date Event',
			'text_order' => 'Text Order',
			'comment' => 'Comment',
			'order_id' => 'Order',
			'user_id' => 'User',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_event',$this->date_event,true);
		$criteria->compare('text_order',$this->text_order,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function addEvent($event, OrdersPlatform $order){
		$log = new self;
		$log->text_order = CJSON::encode($order);
		$log->order_id = $order->id;
		$log->comment = $event;
		$log->validate();
		$log->save();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrderLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
