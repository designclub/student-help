<?php
/**
* Класс SubjectOrderController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     http://yupe.ru
**/
class SubjectOrderBackendController extends \yupe\components\controllers\BackController
{
    /**
    * Отображает Предмет работы по указанному идентификатору
    *
    * @param integer $id Идинтификатор Предмет работы для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель Предмета работы.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new SubjectOrder;

        if (Yii::app()->getRequest()->getPost('SubjectOrder') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('SubjectOrder'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OrderplatformModule.orderplatform', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование Предмета работы.
    *
    * @param integer $id Идинтификатор Предмет работы для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('SubjectOrder') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('SubjectOrder'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('OrderplatformModule.orderplatform', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель Предмета работы из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Предмета работы, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('OrderplatformModule.orderplatform', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('OrderplatformModule.orderplatform', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление Предметами работ.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new SubjectOrder('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('SubjectOrder') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('SubjectOrder'));
        $this->render('index', ['model' => $model]);
    }
	
	public function actionExcel() {
		$model = new StoreExchangeExcel;
		if ( Yii::app()->getRequest()->getIsPostRequest() && $data = Yii::app()->getRequest()->getPost( 'StoreExchangeExcel' ) ) {
			
				$model->save(false);
			
				$i = 0;
				$j = 0;
				$e = 0;
 
				$sheet_array = Yii::app()->yexcel->readActiveSheet( Yii::getPathOfAlias('webroot.uploads.image') .'/'. $model->name );
 
				foreach ( $sheet_array as $row )

					++$i;
				for ( $k = 2; $k <= $i; $k++ ) {
 
					if ( !empty($sheet_array[$k]['A']) || !empty($sheet_array[$k]['B'] ) ) {
						
						if (!empty($sheet_array[$k]['A']) && $sheet_array[$k]['A'] !=' '){
							$isParentSubj = SubjectOrder::model()->findByAttributes(['name_subject'=>$sheet_array[$k]['A']]);
						
							if (count( $isParentSubj ) > 0 ){
								$parent_id = $isParentSubj->id;
							}
							else{
								$subjectParent = new SubjectOrder;
								$subjectParent->name_subject = $sheet_array[ $k ][ 'A' ]; //наименование
								$subjectParent->save();
								$parent_id = $subjectParent->id;
							}
						}
						
						if (!empty($sheet_array[$k]['B']) && $sheet_array[$k]['B']!=' '){
							$isSubj= SubjectOrder::model()->findByAttributes(['name_subject'=>$sheet_array[$k]['B']]);
							if(empty($isSubj)){
								$subject = new SubjectOrder;
								$subject->parent_id = $parent_id; //родительская категория
								$subject->name_subject = $sheet_array[ $k ][ 'B' ]; //наименование
								
								//сохранение данных 
								if ($subject->save()){
									++$j;
									Yii::app()->user->setFlash('success', 'Товаров добавлено, шт: '.$j.'');
								}
								else{
									++$e;
									Yii::app()->user->setFlash('error', 'Товаров НЕ добавлено, шт: '.$e.'');	
								}
							}
						}
					}
				}
		}

		$this->render( 'excel', [ 'model' => $model ] );
	}
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = SubjectOrder::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
