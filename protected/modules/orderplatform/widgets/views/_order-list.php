<div class="block-order">
	<div class="row">
		<div class="col-lg-12">
			<h5><?= $data->title; ?></h5>
		</div>
		<div class="row data-order">
			<div class="col-lg-3">
				<?= $data->subject->name_subject; ?>
			</div>
			<div class="col-lg-3">
				<?= $data->type->name_type; ?>
			</div>
			<div class="col-lg-3">
				<label class="label label-info">Статус заказа: <?= $data->status->name_status; ?></label>
			</div>
		</div>
	</div>

	<div class="row options-order">
		<div class="col-lg-4">
			<i class="fa fa-clock-o"></i>
			Срок: <?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $data->target_date); ?>
		</div>
		<!--<div class="col-lg-3">
			<i class="fa fa-info" aria-hidden="true"></i>
			Уникальность: <?= $data->uniqueness; ?>% (<?= $data->uniqCheck->name_check_unique; ?>)
		</div>-->
		<div class="col-lg-5">
			<i class="fa fa-user" aria-hidden="true"></i>
			Автор: <?= empty($data->author->nick_name) ? 'не назначен' : $data->author->nick_name; ?>
		</div>
		<div class="col-lg-2">
			<?php $dialogOrder = DialogOrderUsers::model()->listAuthorsOrder($data->id); ?>
			<?php $class = count($dialogOrder) > 0 ? 'show' : 'hide' ?>
			<?= CHtml::link('<i class="fa fa-comments-o" aria-hidden="true"></i>', '#', ['class'=>'link-dialog '.$class, 'data-order' => $data->id, 'data-toggle'=>'modal', 'data-target'=>'#dialogModal', 'link'=>Yii::app()->createAbsoluteUrl('/backend/orderplatform/dialog/dialogOrder')]); ?>
		</div>
	</div>

	<div class="row text-order">
		<div class="col-lg-12">
			<?= CHtml::link('Описание заказа', '#', ['class'=>'link-more']);?>
			<div class="more-text hide">
				<?= $data->text; ?>
			</div>
		</div>
	</div>

</div>