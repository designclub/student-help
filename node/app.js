const app        = require('express')();
const server     = require('http').Server(app);
const io         = require('socket.io')(server);
const fs         = require("fs");
const mysql      = require("mysql");
let users        = {};

let con = mysql.createConnection({
    host: '127.0.0.1',
    port:  '3306',
    user: 'help',
    password: 'helpstd56DeSiGnClub',
    database: 'help',
});

server.listen(8888);

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};

// Конвертировать данные
function convertData(field)
{
	let create_date = new Date();

    form = {};

    form['order_id'] = field['DialogOrderUsers[order_id]'];
    form['message'] = field['DialogOrderUsers[message]'];
    form['user_to'] = field['DialogOrderUsers[user_to]'];
    form['user_from'] = field['userId'];
    form['status_id'] = 1;
    form['create_date'] = create_date.toMysqlFormat();

    return form;
}

function convertArray(data){
	form = data.form;
    let field = {};
    for (let i = 0; i < form.length; i++) {
        field[form[i].name] = form[i].value;
    }

	return field;
}

io.on('connection', function (socket) {

    socket.on('connect message', function (data) {
        users[socket.id.toString()] = {userId: parseInt(data.userId)};
    });

    socket.on('send message', function (data) {

        let obj = convertArray(data);
        let field = convertData(obj);

		con.query('INSERT INTO sh_dialog_order_users SET ?', field, function (error, results, fields){
			if (error) throw error;

			field.userName = obj.userName

            con.query('SELECT count(*) as count FROM `sh_dialog_order_users` WHERE user_from = ? and user_to = ? and is_read = ?', [field.user_from, field.user_to, 0], function (error, results){
                if (error) throw error;

                field.count = results[0].count;

                let key = getKey(field.user_to);

                if (key){
                    io.sockets.sockets[key].emit('new message', field);
                }
            });
        });

		/*let sql = `
            SELECT d.*, uc.nick_name as nick_to, ua.nick_name as nick_from
            FROM sh_dialog_order_users as d
            INNER JOIN sh_user_user as uc ON uc.id = d.user_to
            INNER JOIN sh_user_user as ua ON ua.id = d.user_from
            WHERE order_id = ?
            ORDER BY id DESC
            LIMIT 1
        `;*/

        /*setTimeout(function(){
            con.query(sql, [field.order_id], function(error, result, fields){
                if (result) {
                    let userId = field.userTo;
                    if (field.userTo === null) {
                        userId = result[0].client_id;
                    }
                    let key = getKey(userId);
                    if (key) {
                        io.sockets.sockets[key].emit('new message', result);
                    }
                }
            });
        }, 1000);*/
    });

    // Вернуть ключ сокета по идентификатору пользователя
    function getKey(userId) {
        let userToKey = null;
        for (var key in users) {
            if (users[key].userId == userId) {
                userToKey = key;
            }
        }
        return userToKey;
    }
});