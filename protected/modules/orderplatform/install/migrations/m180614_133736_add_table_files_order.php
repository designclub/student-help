<?php

class m180614_133736_add_table_files_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
		//таблица файлов по заказам
        $this->createTable('{{files_order}}', [
            "id" => "pk",
            "date_upload" => "timestamp NOT NULL",
            "name_file" => "varchar(500) NOT NULL",
            "author_id" => "INTEGER(11)  NOT NULL",
            "order_id" => "INTEGER(11)  NOT NULL"
        ], $this->getOptions());
	}

	public function safeDown()
	{
		$this->dropTable('{{files_order}}');
	}
}