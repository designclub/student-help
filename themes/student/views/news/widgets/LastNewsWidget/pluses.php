<?php Yii::import('application.modules.news.NewsModule'); ?>
 
<?php if (isset($models) && $models != []): ?>
	<?php foreach ($models as $model): ?>
		<div class="item-block">
			<div class="img-block">
				<?= CHtml::image($model->getImageUrl()); ?>
			</div>
			<div class="title-block">
				<?= $model->title; ?>
			</div>			
			<div class="txt-block">
				<?= $model->full_text; ?>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
 