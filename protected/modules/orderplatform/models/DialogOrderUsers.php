<?php

/**
 * This is the model class for table "{{dialog_order_users}}".
 *
 * The followings are the available columns in table '{{dialog_order_users}}':
 * @property integer $id
 * @property string $create_date
 * @property string $update_date
 * @property string $message
 * @property integer $order_id
 * @property integer $status_id
 * @property integer $is_read
 *
 * The followings are the available model relations:
 * @property OrdersPlatform[] $ordersPlatforms
 */
class DialogOrderUsers extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dialog_order_users}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['order_id, message', 'required'],
			['order_id, user_from, user_to, status_id, is_read', 'numerical', 'integerOnly'=>true],
			['create_date, update_date, message', 'safe'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, create_date, update_date, message, order_id, client_id, author_id, status_id, is_read', 'safe', 'on'=>'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'user' => [self::BELONGS_TO, 'User', 'user_from'],
			'ordersPlatforms' => [self::HAS_MANY, 'OrdersPlatform', 'dialog_id'],
		];
	}

	public function beforeSave()
	{
		if ($this->isNewRecord){
			$this->user_from = Yii::app()->getUser()->getId();
			$this->create_date = date('Y-m-d H:m:s');
			$this->update_date = date('Y-m-d H:m:s');
			$this->status_id =1;
		}
		else{
			$this->update_date = date('Y-m-d H:m:s', time());
		}

		return parent::beforeSave();
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'          => 'ID',
			'parent_id'   => 'Parent_id',
			'create_date' => 'Create Date',
			'update_date' => 'Update Date',
			'message'     => 'Текст сообщения',
			'order_id'    => 'Order',
			'status_id'   => 'Status',
			'is_read'     => 'Прочитан',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('is_read',$this->is_read);

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	//диалог
	public function listDialog($order_id, $user_id){
		$criteria=new CDbCriteria;
		$criteria->condition = 'order_id = :order_id AND (user_from = :user OR user_from = :self) AND (user_to = :user OR user_to = :self)';
		$criteria->params = [':order_id'=>$order_id, ':user' =>$user_id, ':self' =>Yii::app()->getUser()->getId()];
		//$criteria->addInCondition('user_froms', [$author_id, Yii::app()->getUser()->getId()]);

		return self::model()->findAll($criteria);
	}

	//получаем список авторов, написавших по заказу
	public function listAuthorsOrder($order_id){
		$criteria=new CDbCriteria;
		$criteria->condition = 'order_id = :order_id AND user_from <> :client';
		$criteria->params = [':order_id'=>$order_id, ':client' => Yii::app()->getUser()->getId()];
		$criteria->group = 'user_from';
		return self::model()->findAll($criteria);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DialogOrderUsers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
