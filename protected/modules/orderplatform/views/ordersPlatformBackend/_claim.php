<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model OrdersPlatform
 *   @var $form TbActiveForm
 *   @var $this OrdersPlatformBackendController
 **/
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm', [
		'id' => 'orders-claim-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'htmlOptions' => [ 'class' => 'well', 'enctype' => 'multipart/form-data' ],
	]
);
?>

<?= $form->errorSummary($model) ?>

<div class="row">
	<div class="col-sm-12">
		<?=  $form->textAreaGroup($model, 'comment', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('comment'),
                    'data-content' => $model->getAttributeDescription('comment')
                ]
            ]]); ?>
	</div>
</div>

 

<?php
$this->widget(
	'bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index', 'class' => 'btn btn-success' ],
		'label' => Yii::t( 'OrderplatformModule.orderplatform', 'Отправить претензию' ),
	]
);
?>

<?php $this->endWidget(); ?>