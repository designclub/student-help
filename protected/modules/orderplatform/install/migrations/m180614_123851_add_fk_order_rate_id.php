<?php

class m180614_123851_add_fk_order_rate_id extends yupe\components\DbMigration
{
	public function safeUp()
	{
		$this->addForeignKey('fk_rates', '{{rates_order}}', 'order_id', '{{orders_platform}}', 'id', 'NO ACTION', 'NO ACTION');
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_rates', '{{rates_order}}');
	}
}