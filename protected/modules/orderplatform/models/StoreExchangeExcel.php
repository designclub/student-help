<?php

/**
 * This is the model class for table "{{store_exchange_excel}}".
 *
 * The followings are the available columns in table '{{store_exchange_excel}}':
 * @property integer $id
 * @property string $date
 * @property string $name
 */
class StoreExchangeExcel extends yupe\models\YModel
{

	public $name;
 
	public function behaviors()
    {
        $module = Yii::app()->getModule('image');

        return [
            'excel' => [
                'class' => 'yupe\components\behaviors\FileUploadBehavior',
                'attributeName' => 'name',
				'types' => 'xlsx, xls',
                'uploadPath' => 'image',
            ],
        ];
    }
	
	 /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{subject_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_subject', 'length', 'max'=>255),
			array('parent_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_subject, parent_id', 'safe', 'on'=>'search'),
		);
	}

	  
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubjectOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
