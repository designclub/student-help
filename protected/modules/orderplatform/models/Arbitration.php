<?php

class Arbitration extends yupe\models\YFormModel
{
	const MONEY_AUTHOR = 1;
	const MONEY_CLIENT = 2;

    public $order;
    public $claim;
    public $decision;
  

    /**
     * @return array
     */
    public function rules()
    {

        return [
            ['order, claim, decision', 'required'],
            ['order, claim, decision', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'decision' => 'Решение по претензии',
        ];
    }
	
	public function getDecisionList()
    {
        return [
            self::MONEY_AUTHOR => 'Передача денег автору',
            self::MONEY_CLIENT => 'Возврат денег заказчику'
        ];
    }
    /**
     * @return array
     */
    public function attributeDescriptions()
    {
        return [
            'decision' => 'Выберите решение по претензии',
        ];
    }
	
}
