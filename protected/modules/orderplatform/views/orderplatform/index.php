<?php
/**
* Отображение для orderplatform/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     http://yupe.ru
**/
$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'orderplatform');
$this->description = Yii::t('OrderplatformModule.orderplatform', 'orderplatform');
$this->keywords = Yii::t('OrderplatformModule.orderplatform', 'orderplatform');

$this->breadcrumbs = [Yii::t('OrderplatformModule.orderplatform', 'orderplatform')];
?>

<h1>
    <small>
        <?php echo Yii::t('OrderplatformModule.orderplatform', 'orderplatform'); ?>
    </small>
</h1>