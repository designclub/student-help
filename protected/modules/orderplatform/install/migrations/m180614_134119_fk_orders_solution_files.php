<?php

class m180614_134119_fk_orders_solution_files extends yupe\components\DbMigration
{
	public function safeUp()
	{
		$this->addForeignKey('fk_file', '{{files_order}}', 'order_id', '{{orders_platform}}', 'id', 'NO ACTION', 'NO ACTION');
		$this->addForeignKey('fk_author_file', '{{files_order}}', 'author_id', '{{user_user}}', 'id', 'NO ACTION', 'NO ACTION');
		
		$this->addForeignKey('fk_solution', '{{solutions_order}}', 'order_id', '{{orders_platform}}', 'id', 'NO ACTION', 'NO ACTION');
		$this->addForeignKey('fk_author_solution', '{{solutions_order}}', 'author_id', '{{user_user}}', 'id', 'NO ACTION', 'NO ACTION');		
		
		$this->addForeignKey('fk_author_rates', '{{rates_order}}', 'author_id', '{{user_user}}', 'id', 'NO ACTION', 'NO ACTION');
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_file', '{{files_order}}');
		$this->dropForeignKey('fk_author_file', '{{files_order}}');
		
		$this->dropForeignKey('fk_solution', '{{solutions_order}}');
		$this->dropForeignKey('fk_author_solution', '{{solutions_order}}');
		
		$this->dropForeignKey('fk_author_rates', '{{rates_order}}');
	}
}