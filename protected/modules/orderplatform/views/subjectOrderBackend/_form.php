<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model SubjectOrder
 *   @var $form TbActiveForm
 *   @var $this SubjectOrderController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'id'                     => 'subject-order-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => ['class' => 'well'],
    ]
);
?>

<div class="alert alert-info">
    <?=  Yii::t('OrderplatformModule.orderplatform', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?=  Yii::t('OrderplatformModule.orderplatform', 'обязательны.'); ?>
</div>
	
	<?=  $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?=  $form->textFieldGroup($model, 'name_subject', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('name_subject'),
                        'data-content' => $model->getAttributeDescription('name_subject')
                    ]
                ]
            ]); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-7">
			<?= $form->dropDownListGroup(
				$model,
				'parent_id',
				[
					'widgetOptions' => [
						'data' => Yii::app()->getComponent('platformRepository')->getFormattedListSubjects(),
						'htmlOptions' => [
							'empty' => Yii::t('OrderplatformModule.orderplatform', '--no--'),
							'encode' => false,
						],
					],
				]
			); ?>
        </div>
    </div>

    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('OrderplatformModule.orderplatform', 'Сохранить Предмет работы и продолжить'),
        ]
    ); ?>
    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
            'label'      => Yii::t('OrderplatformModule.orderplatform', 'Сохранить Предмет работы и закрыть'),
        ]
    ); ?>

<?php $this->endWidget(); ?>