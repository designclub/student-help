<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Закрыть окно">
                    <span aria-hidden="true">&times;</span>
                </button>


	<h4 class="modal-title">Сделать ставку</h4>
</div>
<div class="modal-body order-rely-body">
	<?php
	/**
	 * Отображение для _form:
	 *
	 *   @category YupeView
	 *   @package  yupe
	 *   @author   Yupe Team <team@yupe.ru>
	 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
	 *   @link     http://yupe.ru
	 *
	 *   @var $model OrdersPlatform
	 *   @var $form TbActiveForm
	 *   @var $this OrdersPlatformBackendController
	 **/
	$form = $this->beginWidget(
		'bootstrap.widgets.TbActiveForm', [
			'id' => 'order-rely-form',
			'type' => 'inline',
			'action' => Yii::app()->createUrl( '/backend/orderplatform/ordersPlatform/rely' ),
			'enableClientValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
				'afterValidate' => 'js:rateSendForm'
			],
		]
	);
	?>

	<div class="row">
		<div class="col-sm-12">

			<div class="row">
				<div class="col-lg-12">
					<h5><?= $order->title; ?></h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<?= $order->subject->name_subject; ?>
				</div>
				<div class="col-lg-6">
					<?= $order->type->name_type; ?>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-6">
					<i class="fa fa-clock-o"></i> Срок:
					<?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $order->target_date); ?>
				</div>
				<div class="col-lg-6">
					<i class="fa fa-info" aria-hidden="true"></i> Уникальность:
					<?= $order->uniqueness; ?>% (<?= $order->uniqCheck->name_check_unique; ?>)
				</div>
			</div>
			
			
			<div class="rate-form">

				<?=  $form->textFieldGroup($model, 'rate', 
					['widgetOptions' => [
							'htmlOptions' => ['data-commission' => UserBalance::model()->getCommission()]]
					]); ?>
					
				<div class="row">
					<div class="col-xs-12">
						<div class="label-margin rely-client hide">Ваш заработок со ставки будет равен <span id="rely-client"></span> руб.</div>
					</div>
				</div>

				<?=  $form->hiddenField($model, 'order_id'); ?>
				<?=  $form->hiddenField($model, 'author_id'); ?>

				<?php
				$this->widget(
					'bootstrap.widgets.TbButton', [
						'buttonType' => 'submit',
						'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index', 'class' => 'btn btn-info' ],
						'label' => empty($model->rate) ? Yii::t( 'OrderplatformModule.orderplatform', 'Поставить' ) : Yii::t( 'OrderplatformModule.orderplatform', 'Изменить' ),
					]
				);
				?>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	 $('body').on( 'keyup', '#RatesOrder_rate', function(){
		
		 var rate = $(this).val();
		 var commission = $(this).attr('data-commission');
		 if (commission > 0)
			rely = (parseFloat(rate) - parseFloat(rate*(commission/100))).toFixed(2);
			if (rely > 0){
				 $('#rely-client').html(parseFloat(rely).toFixed(2));
				 $('.rely-client').removeClass('hide');
			}
			else
				 $('.rely-client').addClass('hide');

	 });
});
</script>

