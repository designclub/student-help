<div class="row">
	<?php if(count($authors)> 0): ?>

	<div class="col-sm-4">
		<ul class="nav nav-pills nav-stacked" id="navDialogs">
			<?php $i=0; ?>
			<?php foreach ($authors as $author): ?>
			<?php $unreadMessages = $order->getUnreadMessages($author->user->id); ?>
			<li
				role="author"
				class="<?= ($i==0) ? 'active' : ''; ?>"
				data-user="<?= $author->user->nick_name; ?>"
				data-url="<?= Yii::app()->createUrl('/orderplatform/dialogBackend/read', ['order' => $order->id, 'user' => $author->user->id]) ?>"
				>
				<a href="#<?= $author->user->nick_name; ?>" data-author-id="<?= $author->user->id; ?>">
					<?= $author->user->nick_name; ?>
					<span
						class="badge pull-right <?= $unreadMessages > 0 ? '' : 'hide' ?>"
						data-user="<?= $author->user->id; ?>"
						data-order="<?= $order->id; ?>">
						<i class="fa fa-bell"></i>
						<strong><?= $unreadMessages ?></strong>
					</span>
				</a>
			</li>
			<?php ++$i; ?>
			<?php endforeach; ?>
		</ul>
	</div>

	<div class="col-sm-8">
		<div class="tab-content">
			<?php $i=0; ?>

			<?php foreach ($authors as $author): ?>

			<?php $dialog = DialogOrderUsers::model()->listDialog($order->id, $author->user_from);  ?>

			<div role="dialog" class="row tab-pane <?= ($i==0) ? 'active' : ''; ?> list-dialog" id="<?= $author->user->nick_name; ?>">
				<?php foreach ($dialog as $d):	?>
				<?php if ($d->user_to == Yii::app()->getUser()->getId()): ?>
				<div class="col-sm-10 pull-right">
					<div class="row">
						<div class="col-sm-10">
							<div class="dialog-message dialog-message-user">
								<?= $d->message; ?>
								<span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $d->create_date); ?></span>
							</div>
							</div>
							<div class="col-sm-2">
								<label><?= $author->user->nick_name; ?></label>
							</div>
					</div>
				</div>
				<?php else: ?>
				<div class="col-sm-10">
					<div class="row">
						<div class="col-sm-2">
							<label>
								<?= $d->user->nick_name; ?>
							</label>
						</div>
						<div class="col-sm-10">
							<div class="dialog-message">
								<?= $d->message; ?>
								<span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $d->create_date); ?></span>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endforeach; ?>
			</div>

			<?php ++$i; ?>
			<?php endforeach; ?>

		</div>

		<div class="row">
			<div class="dialog-widget">
				<?php
				$form = $this->beginWidget(
					'bootstrap.widgets.TbActiveForm',
					[
						'id' => 'dialog-form',
						'type' => 'inline',
						'action' => Yii::app()->createUrl('/backend/orderplatform/dialog/send'),
						'enableClientValidation' => true,
						'clientOptions' => [
							'validateOnSubmit' => true,
							'afterValidate' => 'js:SendMessage'
						],
					]
					); ?>

				<?= $form->hiddenField($model, 'order_id'); ?>
				<?= $form->hiddenField($model, 'user_to'); ?>

				<div class="col-sm-10">
					<?= $form->textAreaGroup($model, 'message'); ?>
				</div>
				<div class="col-sm-2">
					<button type="submit" class="btn btn-success" id="message-send">
							<i class="fa fa-share" aria-hidden="true"></i>
						</button>

				</div>
				<?php $this->endWidget(); ?>
			</div>
		</div>

	</div>

	<?php else: ?>

	<div class="list-dialog">
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-2"><label></label>
				</div>
				<div class="col-sm-10">
					<div class="dialog-message">
						Активных диалогов нет
						<span></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php endif; ?>
</div>