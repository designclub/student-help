<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Типы работ') => ['/orderplatform/typesOrderBackend/index'],
    $model->name_type => ['/orderplatform/typesOrderBackend/view', 'id' => $model->id],
    Yii::t('OrderplatformModule.orderplatform', 'Редактирование'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Типы работ - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Типами работ'), 'url' => ['/orderplatform/typesOrderBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Тип работы'), 'url' => ['/orderplatform/typesOrderBackend/create']],
    ['label' => Yii::t('OrderplatformModule.orderplatform', 'Тип работы') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Редактирование Типа работы'), 'url' => [
        '/orderplatform/typesOrderBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Просмотреть Тип работы'), 'url' => [
        '/orderplatform/typesOrderBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Удалить Тип работы'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/orderplatform/typesOrderBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('OrderplatformModule.orderplatform', 'Вы уверены, что хотите удалить Тип работы?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Редактирование') . ' ' . Yii::t('OrderplatformModule.orderplatform', 'Типа работы'); ?>        <br/>
        <small>&laquo;<?=  $model->name_type; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>