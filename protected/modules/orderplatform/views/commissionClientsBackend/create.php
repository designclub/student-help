<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Комиссии для клиентов') => ['/orderplatform/commissionClients/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Добавление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Комиссии для клиентов - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Комиссиями для клиентов'), 'url' => ['/orderplatform/commissionClients/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Комиссию для клиентов'), 'url' => ['/orderplatform/commissionClients/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Комиссии для клиентов'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>