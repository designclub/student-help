﻿ <div>
	<div class="price-item">
		<div class="row">
			<div class="col-sm-12 col-xs-12 img-block">
				<img src="<?= $data->getImageUrl(200, 200, false); ?>" alt="<?= $data->title; ?>" />
			</div>
			<div class="col-sm-12 col-xs-12 title"><?= $data->title; ?></div>
			<div class="col-sm-12 col-xs-12 price"><?= $data->full_text; ?></div>
		</div>
	</div>
</div>