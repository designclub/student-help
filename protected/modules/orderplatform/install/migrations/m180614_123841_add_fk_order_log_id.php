<?php

class m180614_123841_add_fk_order_log_id extends yupe\components\DbMigration
{
	public function safeUp()
	{
		 $this->addForeignKey('fk_log', '{{order_log}}', 'order_id', '{{orders_platform}}', 'id', 'NO ACTION', 'NO ACTION');
		 
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_log', '{{order_log}}');
	}
}