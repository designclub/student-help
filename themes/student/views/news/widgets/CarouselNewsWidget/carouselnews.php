<?php
/**
 * Отображение для GalleryWidget/gallerywidget:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$url = Yii::app()->getTheme()->getAssetsUrl();

Yii::app()->getClientScript()->registerScriptFile($url . '/js/owl.carousel.min.js', CClientScript::POS_END);
 
?>

<?php
	$this->widget('application.modules.gallery.widgets.OwlCarouselNewsWidget', array(
				'id'=>'carousel-price', 
				'options' => [
					'items'					=> 3,
					'loop'					=> true,	
					'autoplay'				=> false,
					'autoplaySpeed'			=> 1300,
					'autoplayHoverPause'	=> true,
					'smartSpeed'			=> 350,
					'fluidSpeed'			=> 150,
					'dragEndSpeed'			=> 150,
					'animateOut'			=> 'fadeOut',
					'animateIn'				=> 'fadeIn',
					'nav'					=> true,
					'navText'				=> ["<i class='fa fa-angle-left' aria-hidden='true'></i>
	", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
					'dots'					=> false,
					'responsiveClass'		=> true,
					'responsive' => [
						0 =>[
							'items' => 1,
							'dots' => false
						],					
						600 =>[
							'items' => 1,
							'dots' => false
						],	
						1000 =>[
							'items' => 3,
							'dots' => false
						]
					]
				]
)); ?>

<div class="row category-description">
	<h4><?= $category->name; ?></h4>
	<?php if ($category->description):?>
		<div class="description">
			<?= $category->description; ?>
		</div>
	<?php endif; ?>
</div>

<div class="row carousel-prices">
		<?php $this->widget(
			'bootstrap.widgets.TbListView',
			array(
				'dataProvider'  => $dataProvider,
				'itemView'      => '_news',
				'template'      => "{items}",
				'itemsCssClass' => 'owl-carousel owl-carousel-carousel-price',
				'itemsTagName'  => 'div',
				'emptyText'		=> 'Пока ничего не добавлено'
			)
		); ?>
</div>
