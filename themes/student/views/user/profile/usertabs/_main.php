﻿<?php
	$form = $this->beginWidget(
		'bootstrap.widgets.TbActiveForm', [
			'id' => 'profile-form',
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'type' => 'vertical',
			'htmlOptions' => [
				'class' => '',
				'enctype' => 'multipart/form-data',
			]
		]
	);
?>

<div class="profile-form">
<?= $form->errorSummary($model); ?>

<div class="panel panel-primary">
	<div class="panel-heading">Аватар</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12">
				<?php $this->widget('AvatarWidget', ['user' => $user, 'noCache' => true, 'imageHtmlOptions' => ['width' => 100, 'height' => 100]]); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div id="userpic" class="userpic">
					<div class="js-preview userpic__preview"></div>
					<div class="js-fileapi-wrapper">
						<div class="js-browse">
							<span class="btn-txt">Выбрать аватар</span>
							<?= $form->fileFieldGroup(
								$model,
								'avatarCrop',
								['widgetOptions' => ['htmlOptions' => ['style' => 'background: inherit;']]]
							); ?>
							<?= $form->hiddenField($model, 'avatarName'); ?>

						</div>
						<div class="js-upload" style="display: none;">
							<div class="progress progress-success"><div class="js-progress bar"></div></div>
							<span class="btn-txt">Загрузка</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="panel panel-primary">
	<div class="panel-heading">Контакты</div>
	<div class="panel-body">

		<div class="row">
			<div class="col-sm-6">
				<?= $form->textFieldGroup(
            $user,
            'email',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'disabled' => true,
                        'class' => Yii::app()->getUser()->profile->getIsVerifyEmail() ? 'text-success' : ''
                    ],
                ],
                //'append' => CHtml::link(Yii::t('UserModule.user', 'Change email'), ['/user/profile/email']),
            ]
        ); ?>
				<?php /*if (Yii::app()->getUser()->profile->getIsVerifyEmail()): { ?>
				<p class="email-status-confirmed text-success">
					<?= Yii::t('UserModule.user', 'E-mail was verified'); ?>
				</p>
				<?php } else: { ?>
				<p class="email-status-not-confirmed text-error">
					<?= Yii::t('UserModule.user', 'e-mail was not confirmed, please check you mail!'); ?>
				</p>
				<?php } endif*/ ?>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo $form->labelEx($model, 'phone', ['class' => 'control-label']); ?>
					<?php $this->widget(
						'CMaskedTextField',
						[
							'model' => $model,
							'attribute' => 'phone',
							'mask' => $module->phoneMask,
							'placeholder' => '*',
							'htmlOptions' => [
								'class' => 'form-control'
							]
						]
					); ?>
					<?php echo $form->error($model, 'phone'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-primary">
	<div class="panel-heading">Личная информация</div>
	<div class="panel-body">

		<div class="row">
			<div class="col-xs-4">
				<?= $form->textFieldGroup($model, 'last_name') ?>
			</div>

			<div class="col-xs-4">
				<?= $form->textFieldGroup($model, 'first_name') ?>
			</div>

			<div class="col-xs-4">
				<?= $form->textFieldGroup($model, 'middle_name') ?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<?= $form->dropDownListGroup(
					$model,
					'gender',
					[
						'widgetOptions' => [
							'data' => User::model()->getGendersList(),
							'htmlOptions' => [
								'data-original-title' => $model->getAttributeLabel('gender'),
								'data-content' => User::model()->getAttributeDescription('gender')
							],
						],
					]
				); ?>
			</div>
			<div class="col-xs-6">
				<?= $form->datePickerGroup(
            $model,
            'birth_date',
            [
                'widgetOptions' => [
                    'options' => [
                        'format' => 'yyyy-mm-dd',
                    ],
                ],
                'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
            ]
        ); ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
				<?= $form->textAreaGroup(
					$model,
					'about',
					['widgetOptions' => ['htmlOptions' => ['rows' => 7]]]
				); ?>
			</div>
		</div>
		
		<?php if (Yii::app()->user->getProfile()->type_account == 2): ?>
		<div class="row">
			<div class="col-xs-12">
				<?= $form->textAreaGroup(
					$model,
					'payment_details',
					['widgetOptions' => ['htmlOptions' => ['rows' => 7]]]
				); ?>
			</div>
		</div>
		<?php endif; ?>

	</div>
</div>




<div class="row">
	<div class="col-xs-12">
		<?php if (is_array($module->profiles) && count($module->profiles)): { ?>
		<?php foreach ($module->profiles as $k => $p): { ?>
		<?php $this->renderPartial("//" . $k . "/" . $k . "_profile", ["model" => $p, "form" => $form]); ?>
		<?php } endforeach; ?>
		<?php } endif; ?>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<?php $this->widget(
            'bootstrap.widgets.TbButton',
            [
                'buttonType' => 'submit',
                'context' => 'primary',
                'label' => Yii::t('UserModule.user', 'Save profile'),
            ]
        ); ?>
	</div>
</div>
</div>
<?php $this->endWidget(); ?>

<div class="modal" id="mphoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
               	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div id="image-preview" class="js-image"></div>
            </div>
            <div class="modal-footer">
                <div>
                    <input type="button" class="btn btn-primary" id="upload" value="Сохранить">
                </div>
                <div class="js-upload" style="display: none;">
                    <div class="progress progress-striped active">
                        <div class="progress-bar js-progress bar"  role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width: 1%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$( document ).ready(function(){
	
	$('#userpic').fileapi({
            url: '/profile/photo',
			data: {'YUPE_TOKEN': yupeToken },
			dataType: 'json',
            accept: 'image/*',
            imageSize: { minWidth: 320, minHeight: 320 },
            imageTransform: {
                width: 320,
                height: 320
            },
            elements: {
                active: { show: '.js-upload', hide: '.js-browses' },
                preview: {
                    el: '.js-preview',
                    width: 200,
                    height: 200
                },
                progress: '.js-progress'
            },
            onSelect: function (evt, ui){
                var file = ui.files[0];

                $('#mphoto').modal('show');
                if(file) {
                    $('#upload')
                        .bind('click.fileapi', function () {
                            $('#userpic').fileapi('upload');
                            $('#mphoto').modal('hide');
                        });
                    $('.js-image').cropper({
                        file: file,
                        bgColor: '#fff',
                        maxSize: [528, $(window).height()-400],
                        minSize: [200, 200],
                        selection: '90%',
                        aspectRatio: 1,
                        onSelect: function (coords) {
                            $('#userpic').fileapi('crop', file, coords);
                        }
                    });
                }
            },
            onComplete: function(evt, ui) {
				if(ui.result.error == false){
					var res = ui.xhr.responseText;
					res = res.substr(4, res.length - 2);
					res = res.replace('"', '');
					res = res.replace('@2x', '');
					$('#photo_preview').val(res);
					$('#ProfileForm_avatarName').val(ui.result.filename);
				}
            }
        });
	
});
</script>