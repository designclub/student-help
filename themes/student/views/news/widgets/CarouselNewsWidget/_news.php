﻿ <div>
	<div class="price-item">
		<div class="row">
			<div class="col-sm-6 col-xs-12 img-block">
				<img src="<?= $data->getImageUrl(90, 165, false); ?>" alt="<?= $data->title; ?>" />
			</div>
			<div class="col-sm-6 col-xs-12 options"><?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl().'/images/bottle.png'); ?> / <?= $data->short_text; ?></div>
		</div>		
		<div class="row">
			<div class="col-sm-6 col-xs-12 price"><?= $data->full_text; ?></div>
			<div class="col-sm-6 col-xs-12 descr"><p><?= $data->title; ?></p></div>
		</div>
	</div>
</div>