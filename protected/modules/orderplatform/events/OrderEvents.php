<?php

/**
 * Class ScheduleEvents
 */
class OrderEvents
{
    /**
     *
     */
    const ORDER_RELY = 'platform.order.rely';
	
	/**
     *
     */
    const ORDER_APPLY_AUTHOR = 'platform.order.apply.author';
	
	/**
     *
     */
    const ORDER_CONFIRM_AUTHOR = 'platform.order.confirm.author';
	
	 /**
     *
     */
    const ORDER_CANCEL_AUTHOR = 'platform.order.cancel.author';

    /**
     *
     */
    const ORDER_CHECK = 'platform.order.check';

    /**
     *
     */
    const ORDER_CORRECTION = 'platform.order.correction';
	
    /**
     *
     */
    const ORDER_APPLY = 'platform.order.apply';
	
    /**
     *
     */
    const ORDER_DISPUTE = 'platform.order.dispute';
	
    /**
     *
     */
    const ORDER_DELAY = 'platform.order.delay';

}
