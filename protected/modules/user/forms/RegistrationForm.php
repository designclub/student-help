<?php
use yupe\widgets\YPurifier;

/**
 * Форма регистрации
 *
 * @category YupeComponents
 * @package  yupe.modules.user.forms
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     http://yupe.ru
 *
 **/
class RegistrationForm extends CFormModel
{

    public $nick_name;
    public $email;
    public $phone;
    public $password;
    public $type_account = 1;
    public $cPassword;
    public $verifyCode;
	public $time_zone;
	public $agreement;

    public $disableCaptcha = false;

    public function isCaptchaEnabled()
    {
        $module = Yii::app()->getModule('user');

        if (!$module->showCaptcha || !CCaptcha::checkRequirements() || $this->disableCaptcha) {
            return false;
        }

        return true;
    }

    public function rules()
    {
        $module = Yii::app()->getModule('user');

        return [
            ['nick_name, email', 'filter', 'filter' => 'trim'],
            ['nick_name, email', 'filter', 'filter' => [new YPurifier(), 'purify']],
            ['nick_name, email, password, cPassword, time_zone', 'required'],
            ['nick_name, email', 'length', 'max' => 50],
            ['password, cPassword', 'length', 'min' => $module->minPasswordLength],
            [
                'nick_name',
                'match',
                'pattern' => '/^[A-Za-z0-9_-]{2,50}$/',
                'message' => 'Вы можете использовать только латинские буквы и цифры'
            ],
            ['nick_name', 'checkNickName'],
			['type_account, agreement', 'safe'],
            [
                'cPassword',
                'compare',
                'compareAttribute' => 'password',
                'message' => Yii::t('UserModule.user', 'Password is not coincide')
            ],
            ['email', 'email'],
            ['email', 'checkEmail'],
			[
                'phone',
                'match',
                'pattern' => $module->phonePattern,
                'message' => 'Некорректный формат телефона (пример: +79123456789)'
            ],
            [
                'verifyCode',
                'yupe\components\validators\YRequiredValidator',
                'allowEmpty' => !$this->isCaptchaEnabled(),
                'message' => Yii::t('UserModule.user', 'Check code incorrect')
            ],
            ['verifyCode', 'captcha', 'allowEmpty' => !$this->isCaptchaEnabled()],
			//['agreement', 'required', 'on' =>'author', 'message' => 'Необходимо принять условия соглашения'],
			['agreement', 'compare', 'compareValue'=>1, 'on' =>'author', 'message' => 'Необходимо принять условия соглашения'],
            ['verifyCode', 'emptyOnInvalid']
        ];
    }

    /**
     * Метод выполняется перед валидацией
     *
     * @return bool
     */
    public function beforeValidate()
    {
        $module = Yii::app()->getModule('user');

        if ($module->generateNickName) {
            $this->nick_name = 'user' . time();
        }

        return parent::beforeValidate();
    }

    public function attributeLabels()
    {
        return [
            'nick_name' => Yii::t('UserModule.user', 'User name'),
            'email' => Yii::t('UserModule.user', 'Email'),
            'password' => Yii::t('UserModule.user', 'Password'),
            'cPassword' => Yii::t('UserModule.user', 'Password confirmation'),
            'verifyCode' => Yii::t('UserModule.user', 'Check code'),
            'time_zone' => Yii::t('UserModule.user', 'Time zone'),
            'agreement' => Yii::t('UserModule.user', 'Agreement', ['{agreement}' => CHtml::link('пользовательского соглашения', '#')]),
        ];
    }

    public function checkNickName($attribute, $params)
    {
        $model = User::model()->find('nick_name = :nick_name', [':nick_name' => $this->$attribute]);

        if ($model) {
            $this->addError('nick_name', Yii::t('UserModule.user', 'User name already exists'));
        }
    }

    public function checkEmail($attribute, $params)
    {
        $model = User::model()->find('email = :email', [':email' => $this->$attribute]);

        if ($model) {
            $this->addError('email', Yii::t('UserModule.user', 'Email already busy'));
        }
    }

    public function emptyOnInvalid($attribute, $params)
    {
        if ($this->hasErrors()) {
            $this->verifyCode = null;
        }
    }
}
