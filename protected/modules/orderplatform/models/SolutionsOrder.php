<?php

/**
 * This is the model class for table "{{solutions_order}}".
 *
 * The followings are the available columns in table '{{solutions_order}}':
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $date_finish
 * @property integer $order_id
 * @property integer $author_id
 * @property string $solution
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property UserUser $author
 * @property OrdersPlatform $order
 */
class SolutionsOrder extends yupe\models\YModel{
	
	public $files = [];
		
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{solutions_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, author_id', 'numerical', 'integerOnly'=>true),
			array('date_finish, solution, comment, date_create, date_update', 'safe'),
			['files', 'fileValidator'],
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_finish, order_id, author_id, solution, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
			'filesOrders'     => [self::HAS_MANY, 'FilesOrder', 'order_id'],
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_finish' => 'Дата и время выполнения',
			'date_create' => 'Дата и время добавления',
			'date_update' => 'Дата и время изменения',
			'order_id' => 'Заказ',
			'author_id' => 'Автор',
			'solution' => 'Решение',
			'comment' => 'Описание к заказу',
			'files[]'          => 'Загрузка файлов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_finish',$this->date_finish,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('solution',$this->solution,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function fileValidator($attribute, $value){
		
		$module = Yii::app()->getModule('orderplatform');
		$files = CUploadedFile::getInstances($this, $attribute);

		$count = 0;

		if (!$this->isNewRecord) {
			$count = FilesOrder::model()->count('order_id = :order AND solution_id = :solution', [':order' => $this->order_id, ':solution' => $this->id]);
		}

		if ($count + count($files) > 5) {
			$this->addError('files', 'Максимальное кол-во файлов для загрузки 5');
			return false;
		}

		$this->files = $files;

		return true;
	}

	public function beforeSave(){
		
		if ($this->isNewRecord){
			$this->date_create = new CDbExpression('NOW()');
		}
			
		$this->date_update = new CDbExpression('NOW()');
		
		$this->date_finish = new CDbExpression('NOW()');
		
		$this->author_id   = Yii::app()->getUser()->getId();

		return parent::beforeSave();
	}
	
	public function afterSave(){

		$module = Yii::app()->getModule('orderplatform');
		foreach ($this->files as $key => $file){
			$fileName = uniqid().'.'.$file->getExtensionName();
			$fileModel = new FilesOrder;
			$path = $module->getUploadPath().'/'.$fileName;
			if ($file->saveAs($path)){
				$fileModel->order_id = $this->order_id;
				$fileModel->solution_id = $this->id;
				$fileModel->name_file = $fileName;
				$fileModel->title_file = $file->getName();
				$fileModel->save();
			}
		}
		return parent::afterSave();
	}
	
	public function filesSolutionsOrder(){
		
		$criteria=new CDbCriteria;
		$criteria->condition = 'order_id = :order';
		$criteria->params = [':order' => $this->order_id];
 
		$orders = FilesOrder::model()->findAll($criteria);

		return($orders);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SolutionsOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
