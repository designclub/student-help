<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', '1') => ['/orderplatform/ordersPlatformBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', '1 - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление 1'), 'url' => ['/orderplatform/ordersPlatformBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить 1'), 'url' => ['/orderplatform/ordersPlatformBackend/create']],
    ['label' => Yii::t('OrderplatformModule.orderplatform', '1') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Редактирование 1'), 'url' => [
        '/orderplatform/ordersPlatformBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Просмотреть 1'), 'url' => [
        '/orderplatform/ordersPlatformBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Удалить 1'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/orderplatform/ordersPlatformBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('OrderplatformModule.orderplatform', 'Вы уверены, что хотите удалить 1?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Просмотр') . ' ' . Yii::t('OrderplatformModule.orderplatform', '1'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_date',
        'update_date',
        'target_date',
        'title',
        'theme',
        'text',
        'budget',
        'cost',
        'file',
        'type_id',
        'subject_id',
        'status_id',
        'client_id',
        'author_id',
        'dialog_id',
    ],
]); ?>
