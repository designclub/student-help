<?php

/**
 * This is the model class for table "{{rates_order}}".
 *
 * The followings are the available columns in table '{{rates_order}}':
 * @property integer $id
 * @property string $date_rate
 * @property integer $order_id
 * @property integer $author_id
 * @property string $rate
 *
 * The followings are the available model relations:
 * @property OrdersPlatform $order
 */
class RatesOrder extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{rates_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_id, author_id', 'numerical', 'integerOnly'=>true),
			array('rate', 'required'),
			array('rate', 'length', 'max'=>10),
			array('date_rate, update_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, date_rate, update_date, order_id, author_id, rate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			'order' => array(self::BELONGS_TO, 'OrdersPlatform', 'order_id'),
		);
	}

	public function beforeSave() {

		if ($this->isNewRecord){
			$this->date_rate = new CDbExpression('NOW()');
		}
		else{
			$this->update_date = new CDbExpression('NOW()');
		}

		return parent::beforeSave();
	}
	
	public function afterFind(){
		$this->rate = round($this->rate, 0);
		parent::afterFind();
	}
	
	public function getRateAuthor($id_order){
		$rate = self::model()->findByAttributes(['order_id' => $id_order, 'author_id' => Yii::app()->getUser()->getId()]);
		if (count($rate) > 0){
			return($rate);
		}
		else{
			return(false);
		}
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date_rate' => 'Date Rate',
			'update_date' => 'Update Date',
			'order_id' => 'Order',
			'author_id' => 'Author',
			'rate' => 'Ставка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date_rate',$this->date_rate,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('rate',$this->rate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RatesOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
