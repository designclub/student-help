﻿<?php $solutions = $order->solutions($order->id, $order->author_id); ?>
<?php if (count($solutions)>0): ?>
	<?php foreach($solutions as $solution): ?>

		<div class="row rates-block-order">
			<div class="col-lg-12">
				<span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $solution->date_create); ?></span>
			</div>
			<div class="col-lg-12">
				Комментарий: <?= $solution->comment; ?>
			</div>						
			<div class="col-lg-12">
				<div class="files-order">
					<p>Файлы:</p>	
					<?php if (count($solution->filesSolutionsOrder()) > 0): ?>
						<?php foreach($solution->filesSolutionsOrder() as $file): ?>
							<?= CHtml::link($file->title_file, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->name_file, ['target' => '_blank', 'class'=>'files-solutions']); ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>

			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>