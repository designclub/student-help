﻿<?php if (count($order->filesForOrders()) > 0): ?>
	<?php foreach($order->filesForOrders() as $file): ?>
	<p><?= CHtml::link($file->name, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->fileName, ['target' => '_blank', 'class'=>'btn']); ?></p>
	<?php endforeach; ?>
<?php endif; ?>