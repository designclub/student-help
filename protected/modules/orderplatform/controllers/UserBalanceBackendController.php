<?php
/**
* Класс UserBalanceController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     http://yupe.ru
**/
class UserBalanceBackendController extends \yupe\components\controllers\BackController
{
	
	public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['addBalance'], 'roles' => ['Orderplatform.OrderplatformBackend.AddBalance']],
            ['allow', 'actions' => ['getMoney'], 'roles' => ['Orderplatform.OrderplatformBackend.GetMoney']],
            ['allow', 'actions' => ['transfer'], 'roles' => ['Orderplatform.OrderplatformBackend.Transfer']],
            ['allow', 'actions' => ['history'], 'roles' => ['Orderplatform.OrderplatformBackend.History']],
            ['allow', 'actions' => ['successPayment'], 'roles' => ['Orderplatform.OrderplatformBackend.SuccessPayment']],
            ['allow', 'actions' => ['failPayment'], 'roles' => ['Orderplatform.OrderplatformBackend.FailPayment']],
            ['deny'],
        ];
    }
	
	//пополнение баланса
	public function actionAddBalance(){
		
        $model = new Balance;
		$user = User::model()->findByPk(Yii::app()->getUser()->getId());
        if (Yii::app()->getRequest()->getPost('Balance') !== null) {
			$model->setAttributes(Yii::app()->getRequest()->getPost('Balance'));

            if ($model->validate()) {

				$pay = Yii::app()->sberbankPayment;
				$userBalance = UserBalance::model()->addBalance($model->sumBalance);
				$result = (array)$pay->registerDo($userBalance->id, (int)(round($model->sumBalance) * 100), Yii::app()->getUser()->getId(), null, null, null);

				
				if (isset($result['formUrl'])){

					$userBalance->number_bill = $result['orderId'];
					
					$userBalance->save();
					return $this->redirect($result['formUrl']);

				}else{
					return $this->redirect('failPay');
				}
				
            }
        }
        $this->render('addBalance', ['model' => $model]);
		
    }   
    
	
	//страница успешного проведения платежа
	public function actionSuccessPayment(){
 		if (Yii::app()->request->getParam('orderId')){

			$id_order = Yii::app()->request->getParam('orderId');
 
			$payment = UserBalance::model()->findByAttributes(['number_bill' => $id_order]);

			if (!empty($payment->number_bill)){

				$pay = Yii::app()->sberbankPayment;
				$result = (array)$pay->getOrderStatus($payment->number_bill);
 
				if(($result['ErrorCode']== 0) && ($result['ErrorMessage'] == 'Успешно') && ($payment->status == UserBalance::STATUS_DURING)){

					$payment->status = UserBalance::STATUS_OK;
					//$userBalance->number_card = $result['Pan'];
					$payment->save();
					$this->render('success');
				}
				else{
					return $this->render('fail', ['result' => $result]);
				}
			}
		}
	}
	
	public function actionFailPayment(){
		$this->render('fail');
	}
	
	//история баланса
	public function actionHistory(){
		$history = new UserBalance;
		
        $this->render('history', ['history' => $history]);
    }
       
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = UserBalance::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
