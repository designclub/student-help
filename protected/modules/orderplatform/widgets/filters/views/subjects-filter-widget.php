<div class="panel panel-primary panel-profile">
	<div class="panel-heading">
		<?= Yii::t( 'OrderplatformModule.orderplatform', 'Filter subjects' ); ?>
	</div>
	<div class="panel-body">
		<div class="row">
			<?= CHtml::form('', 'post', ['id' => 'subjects-filter', 'class' => 'subjects-filter']) ?>
				<?= SubjectOrder::model()->getTree($filter) ?>
			<?= CHtml::endForm() ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	
$(document ).ready(function() {
	
	function isSubjectParentsCheked(){
		 $('.subject-parent').each(function(i, e){
			 if($("input[data-subject-parent="+$(e).val()+"]:not(:checked)").length === 0){
				$("#SubjectOrder_"+$(e).val()+"_subject_id").prop('checked', true);
			}
		 });
	};
	
	isSubjectParentsCheked();
	
	$('.subject-parent').on('change', function(){
		var elem = $(this);
		var value = elem.val();
		$("input[data-subject-parent="+value+"]").prop('checked', this.checked);
	});
	
	$('#subjects-filter').on('change', function(){
		var elem = $(this);
		var data = elem.serialize()+'&SubjectOrder[0][subject_id]=0';
		
		isSubjectParentsCheked();
		
		$.ajax({
			data: data,
			type: 'post',
		})
		return false;
	});
	
	isSubjectParentsCheked();
});
	
</script>