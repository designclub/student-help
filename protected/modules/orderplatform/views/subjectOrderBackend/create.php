<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Предметы работ') => ['/orderplatform/subjectOrderBackend/index'],
    Yii::t('OrderplatformModule.orderplatform', 'Добавление'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Предметы работ - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Предметами работ'), 'url' => ['/orderplatform/subjectOrderBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Предмет работы'), 'url' => ['/orderplatform/subjectOrderBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Предметы работ'); ?>
        <small><?=  Yii::t('OrderplatformModule.orderplatform', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>