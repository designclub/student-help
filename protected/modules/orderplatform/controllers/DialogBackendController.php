<?php
/**
 * Класс OrdersPlatformBackendController:
 *
 *   @category Yupe\yupe\components\controllers\BackController
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
class DialogBackendController extends\ yupe\ components\ controllers\ BackController {

	public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['dialogOrder'], 'roles' => ['Orderplatform.DialogBackendController.DialogOrder']],
            ['allow', 'actions' => ['send', 'read'], 'roles' => ['Orderplatform.DialogBackendController.Send']],
            ['deny'],
        ];
    }

	public function actionDialogOrder(){

		$order = OrdersPlatform::model()->findByPk((int)Yii::app()->request->getPost('order'));
		$user = User::model()->findByPk(Yii::app()->getUser()->getId());

		if (count($order) > 0){

			//автор
			if ($user->type_account == 2){
				$dialog = DialogOrderUsers::model()->listDialog($order->id, $order->client_id);
				$this->renderPartial('_dialogAuthor', ['order'=>$order, 'dialog' => $dialog], false, true);
			}
			//заказчик
			elseif($user->type_account == 1){
				$model = new DialogOrderUsers;
				$model->order_id = $order->id;

				$listAuthor =  DialogOrderUsers::model()->listAuthorsOrder($order->id);

				//по-умолчанию user_to первый автор в списке сообщений
				$model->user_to = $listAuthor[0]['user_from'];
				$this->renderPartial('_dialog', ['order'=>$order, 'authors' => $listAuthor, 'model' => $model], false, true);
			}
		}
		else{
			echo 'Диалогов нет';
			Yii::app()->end();
		}
	}

	public function actionSend(){
        $request = Yii::app()->getRequest();

        if (!$request->getIsPostRequest() && !$request->getPost('DialogOrderUsers')) {
            throw new CHttpException(404);
        }

		$user = User::model()->findByPk(Yii::app()->getUser()->getId());

		$dialog = new DialogOrderUsers;
		$dialog->setAttributes($request->getPost('DialogOrderUsers'));

		if ($dialog->save()){

			$order = OrdersPlatform::model()->findByPk($dialog->order_id);

			//автор
			if ($user->type_account == 2){
				$dialog->user_to = $order->client_id;
				$dialog->save();

				$listDialog = DialogOrderUsers::model()->listDialog($order->id, $order->client_id);
				$this->renderPartial('_dialogAuthor', ['order'=>$order, 'dialog' => $listDialog], false, true);
			}
			//заказчик
			elseif($user->type_account == 1){

				$model = new DialogOrderUsers;
				$model->order_id = $order->id;
				$model->user_to = $dialog->user_to;
				$listAuthor =  DialogOrderUsers::model()->listAuthorsOrder($order->id);

				$this->renderPartial('_dialog', ['order'=>$order, 'authors' => $listAuthor, 'model' => $model], false, true);
			}
		}
		else{
			Yii::app()->ajax->failure('Сообщение не доставлено');
			Yii::app()->end();
		}
	}

	public function actionRead($order, $user)
	{
		$models = DialogOrderUsers::model()->updateAll([
			'is_read' => 1
		],
		'order_id=:order_id and user_from=:user_from',
		[
			'order_id' => $order,
			'user_from' => $user
		]);
	}

}