<?php

/**
 * This is the model class for table "{{commission_author}}".
 *
 * The followings are the available columns in table '{{commission_author}}':
 * @property integer $id
 * @property string $name_level
 * @property integer $comission_percent
 * @property string $comission_sum
 * @property integer $count_order
 * @property string $sum_orders
 * @property integer $count_comments
 */
class CommissionAuthor extends yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{commission_author}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comission_percent', 'required'),
			array('comission_percent, count_order, count_comments', 'numerical', 'integerOnly'=>true),
			array('name_level', 'length', 'max'=>255),
			array('comission_sum, sum_orders', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_level, comission_percent, comission_sum, count_order, sum_orders, count_comments', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_level' => 'Наименование уровня',
			'comission_percent' => 'Комиссия (%)',
			'comission_sum' => 'Комиссия (в рублях)',
			'count_order' => 'Количество заказов',
			'sum_orders' => 'Сумма заказов',
			'count_comments' => 'Количество отзывов',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_level',$this->name_level,true);
		$criteria->compare('comission_percent',$this->comission_percent);
		$criteria->compare('comission_sum',$this->comission_sum,true);
		$criteria->compare('count_order',$this->count_order);
		$criteria->compare('sum_orders',$this->sum_orders,true);
		$criteria->compare('count_comments',$this->count_comments);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getCommissionAuthor($id_author){
		
		$where = "author_id = " . $id_author;
		$where .= " AND status_id = " . StatusOrder::STATUS_FINISH;
		$where .= " AND update_date > NOW() - INTERVAL 6 MONTH";
		
		$sum_order_half_year = Yii::app()->db->createCommand("SELECT sum(cost) as sum_order_half_year FROM sh_orders_platform WHERE ". $where)->queryRow();		
		
		$count_order_half_year = Yii::app()->db->createCommand("SELECT count(id) as count_order_half_year FROM sh_orders_platform WHERE ". $where)->queryRow();
		
		$sum_order = !empty($sum_order_half_year['sum_order_half_year']) ? : 0;
		
		$count_order = !empty($count_order_half_year['count_order_half_year']) ? : 0;

		$commission = Yii::app()->db->createCommand("SELECT comission_percent FROM sh_commission_author WHERE " . $sum_order . " >= sum_orders  AND " . $count_order. " >= count_order ORDER BY id DESC LIMIT 1")->queryRow();

		return !empty($commission['comission_percent']) ? $commission['comission_percent'] : 0;
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommissionAuthor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
