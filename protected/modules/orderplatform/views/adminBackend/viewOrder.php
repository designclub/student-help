<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = ['Просмотр истории заказа'];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Просмотр истории заказа');
 
?>
<div class="page-header page-header-bottom">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Просмотр истории заказа'); ?> № &laquo;<?=  $order->id; ?>&raquo;
    </h1>
</div>

<div class="row">
	<div class="col-xs-8">
		<?php $this->widget('bootstrap.widgets.TbDetailView', [
			'data'       => $order,
			'attributes' => [
				'title',

				[
					'label' => 'Статус заказа',
					'value' => $order->status->name_status
				],

				[
					'label' => 'Срок',
					'value' => Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $order->target_date)
				],

				[
					'label' => 'Тип работы',
					'value' => $order->type->name_type
				],

				[
					'label' => 'Предмет работы',
					'value' => $order->subject->name_subject
				],

				[
					'label' => 'Уникальность',
					'value' => $order->uniqueness . '% (' . ($order->uniqCheck->name_check_unique) . ')'
				],
	
				[
					'label' => 'Клиент',
					'value' => $order->client->nick_name
				],
	
				[
					'label' => 'Автор',
					'value' => $order->author->nick_name
				],
	
				[
					'label' => 'Описание заказа',
					'value' => $order->text
				]

			],
		]); ?>
	  
		
	</div>
	
	<div class="col-xs-4">
		<div class="panel panel-default">
		  <div class="panel-heading"><label class="label label-danger">Претензия</label></div>
		  <div class="panel-body">
			<?= $claim->comment; ?> 
		  </div>
		</div>
		
		<div class="row">
			<div class="col-xs-12">
			<?php
					$form = $this->beginWidget(
						'bootstrap.widgets.TbActiveForm', [
							'id' => 'decision-form',
							'action' => Yii::app()->createUrl('/orderplatform/adminBackend/decision'),
							'enableClientValidation' => true,
							'clientOptions' => [
								'validateOnSubmit' => true,
								'afterValidate' => 'js:decisionForm'
							],
						]
					);
					?>
					
					<?= $form->hiddenField($arbitration, 'order'); ?>
					<?= $form->hiddenField($arbitration, 'claim'); ?>

					<?= $form->radioButtonList($arbitration, 'decision', $arbitration->getDecisionList()); ?>
					
					<?php $this->widget(
						'bootstrap.widgets.TbButton', [
							'buttonType' => 'submit',
							'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index', 'class' => 'btn-primary'],
							'label'      => Yii::t('OrderplatformModule.orderplatform', 'Вынести решение по претензии'),
						]
					); ?>

			<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>


<div class="row">
  <div class="col-xs-12">
	  <?php $this->widget("bootstrap.widgets.TbTabs", array(
			"id" => 'tabs',
			"type" => 'tabs',
			'htmlOptions' => ['class'=>'tabs-order'],
			"tabs" => [
				["label" => "Приложение к заказу", "content" => $this->renderPartial('order/_files', ['order'=>$order],true,false), "active" => true],
				["label" => "История правок", "content" => $this->renderPartial('order/_history', ['order'=>$order],true,false)],
				["label" => "Ставки по заказу", "content" => $this->renderPartial('order/_rates', ['order'=>$order],true,false)],
				//["label" => "Диалог по заказу", "content" => $this->renderPartial('order/_dialog', ['order'=>$order],true,false)],
			],
		)); ?>
	</div>
</div>