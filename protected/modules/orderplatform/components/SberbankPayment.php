<?php

/*use Yii;
use yii\base\Component;
use yii\helpers\Url;
use yii\helpers\Json;*/

/**
*  Компонент экваринга Сбербанка
*/
class SberbankPayment extends CApplicationComponent
{
    /**
     * Логин магазина, полученный при подключении
     * @var string
     */
    protected $_userName;

    /**
     * Пароль магазина, полученный при подключении
     * @var string
     */
    protected $_password;

    /**
     * Массив обязательных параметров запроса.
     * @var array
     */
    protected $_params;

    /**
     * Адрес, на который надо перенаправить пользователя в случае успешной оплаты
     * @var array
     */
    public $returnUrl;

    /**
     * Адрес, на который надо перенаправить пользователя в случае неуспешной оплаты
     * @var array
     */
    public $failUrl;

    /**
     * Код валюты платежа ISO 4217. Если не указан, считается равным 810 (российские рубли).
     * @var mixed
     */
    public $currency;

    /**
     * Ссылка платежного шлюза
     * @var string
     */
    public $apiUrl;

    /**
     * Язык в кодировке ISO 639-1. Если не указан, будет использован язык, указанный в настройках
     * магазина как язык по умолчанию (default language)
     * @var mixed
     */
    public $language;

    public function init()
    {
        $this->_params = [
            'userName'=>$this->_userName,
            'password'=>$this->_password,
        ];

        parent::init();
    }

    /**
     * Метод отправляет запросы
     * @param  string $url   url на который нужно отправить запрос
     * @return string        ответ на запрос
     */
    protected function connect($url)
    {
        // $result = file_get_contents($url.'?'.$this->convertParams());
        $ch = curl_init($url);
        curl_setopt ($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $this->convertParams());
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec ($ch);
        $result = curl_multi_getcontent ($ch);
        curl_close ($ch);
        return $result;
    }

    /**
     * Конвертирует передаваемые параметры в get url
     * @return string url параметров запроса
     */
    protected function convertParams()
    {
        return http_build_query($this->_params);
    }

    public function setUserName($userName)
    {
        $this->_userName = $userName;
    }

    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * Устанавливает повторяющиеся, необязательные опции
     */
    protected function _setOptions()
    {
        $this->_params['returnUrl'] = Yii::app()->createAbsoluteUrl($this->returnUrl);
        $this->_params['failUrl'] = Yii::app()->createAbsoluteUrl($this->failUrl);
        $this->_params['currency'] = $this->currency;
        $this->_params['language'] = $this->language;
    }

    /**
     * Запросы, используемые при одностадийной оплате
     * Запрос регистрации заказа
     * @param  intager  $orderNumber Номер (идентификатор) заказа в системе магазина, уникален для каждого магазина в пределах системы
     * @param  intager  $amount      Сумма платежа в копейках (или центах)
     * @param  intager  $clientId    Номер (идентификатор) клиента в системе магазина. Используется для реализации функционала связок.
     * @param  string  $bindingId   Идентификатор связки, созданной ранее.
     * @return string  Массив с параметров ответа сервера
     */
    public function registerDo($orderNumber, $amount, $clientId=null, $bindingId=null, $customerDetails=null, $cartItems=null)
    {
        $this->_setOptions();
        $this->_params['orderNumber']                    = $orderNumber;
        $this->_params['amount']                         = $amount;
        $this->_params['clientId']                       = $clientId;
        $this->_params['bindingId']                      = $bindingId;
        $this->_params['taxSystem']                      = 2;
        // $this->_params['orderBundle']['customerDetails'] = $customerDetails;
        $this->_params['orderBundle']['cartItems']       = $cartItems;

        $result = $this->connect($this->apiUrl.'/rest/register.do');

        try{
            $res=json_decode($result);
            return $res;
        }
        catch(\Exception $e){
            print_r($result);
        }
    }

    /**
     * Запросы, используемые при двухстадийной оплате
     * Запрос регистрации заказа c предавторизацией
     * @param  intager $orderNumber Номер (идентификатор) заказа в системе магазина, уникален для каждого магазина в пределах системы
     * @param  intager $amount      Сумма платежа в копейках (или центах)
     * @return string  Массив с параметров ответа сервера
     */
    public function registerPreAuthDo($orderNumber, $amount, $clientId=null)
    {
        $this->_setOptions();
        $this->_params['orderNumber'] = $orderNumber;
        $this->_params['amount'] = $amount;
        $this->_params['clientId'] = $clientId;

        $result = $this->connect($this->apiUrl.'/rest/registerPreAuth.do');

        return json_decode($result);
    }

    /**
     * Запрoс завершения oплаты заказа
     * @param  intager $orderId Номер заказа в платежной системе. Уникален в пределах системы.
     * @param  intager $amount  Сумма платежа в копейках (или центах)
     * @return string  Массив с параметров ответа сервера
     */
    public function depositDo($orderId, $amount,$loginAuto=false)
    {
        if ($loginAuto)
            $this->_loginAuto();

        $this->_setOptions();
        $this->_params['orderNumber'] = $orderId;
        $this->_params['amount'] = $amount;

        $result = $this->connect($this->apiUrl.'/rest/deposit.do');

        return json_decode($result);
    }

    /**
     * Запрос отмены оплаты заказа
     * @param  string $orderId Номер заказа в платежной системе. Уникален в пределах системы.
     * @return string  Массив с параметров ответа сервера
     */
    public function reverseDo($orderId)
    {
        $this->_params['orderId'] = $orderId;
        $result = $this->connect($this->apiUrl.'/rest/reverse.do');

        return json_decode($result);
    }

    /**
     * Запрос возврата средств оплаты заказа
     * @param  string $orderId Номер заказа в платежной системе. Уникален в пределах системы.
     * @param  intager $amount  Сумма платежа в копейках (или центах)
     * @return string  Массив с параметров ответа сервера
     */
    public function refundDo($orderId, $amount)
    {
        $this->_params['orderId'] = $orderId;
        $this->_params['amount'] = $amount;
        $result = $this->connect($this->apiUrl.'/rest/refund.do');

        return json_decode($result);
    }

    /**
     * Запрос состояния заказа
     * @param intager $orderId Номер заказа в платежной системе. Уникален в пределах системы.
     * @return string  Массив с параметров ответа сервера
     */
    public function getOrderStatus($orderId)
    {
        $this->_params['orderId'] = $orderId;
        $result = $this->connect($this->apiUrl.'/rest/getOrderStatus.do');

        return json_decode($result);
    }

    /**
     * Расширенный запрос состояния заказа
     * @param  intager $orderId     Номер заказа в платежной системе. Уникален в пределах системы.
     * @param  intager $orderNumber Номер (идентификатор) заказа в системе магазина.
     * @return string  Массив с параметров ответа сервера
     */
    public function getOrderStatusExtended($orderId, $orderNumber)
    {
        $this->_params['orderId'] = $orderId;
        $this->_params['orderNumber'] = $orderNumber;
        $result = $this->connect($this->apiUrl.'/rest/getOrderStatusExtended.do');

        return json_decode($result);
    }

    /**
     * Запрос проверки вовлечённости карты в 3DS
     * @param  integer $pan Номер карты
     * @return string  Массив с параметров ответа сервера
     */
    public function verifyEnrollment($pan)
    {
        $this->_params['pan'] = $pan;
        $result = $this->connect($this->apiUrl.'/rest/verifyEnrollment.do');

        return json_decode($result);
    }

    /**
     * Запрос статистики по платежам за период
     * @param  [type] $size              Количество элементов на странице (максимальное значение = 200).
     * @param  [type] $from              Дата и время начала периода для выборки заказов в формате YYYYMMDDTHHmmss.
     * @param  [type] $to                Дата и время окончания периода для выборки заказов в формате YYYYMMDDTHHmmss.
     * @param  [type] $transactionStates В этом блоке необходимо перечислить требуемые состояния заказов. Только заказы, находящиеся в
     * одном из указанных состояний, попадут в отчёт. Несколько значений указываются через запятую. Возможные значения: CREATED, APPROVED,
     * DEPOSITED, DECLINED, REVERSED, REFUNDED.
     * @param  [type] $merchants         Список Логинов мерчантов, чьи транзакции должны попасть в отчёт. Несколько значений
     * указываются через запятую. Чтобы получить список отчётов по всем доступным мерчантам, необходимо оставить это поле пустым.
     * @return string  Массив с параметров ответа сервера
     */
    public function getLastOrdersForMerchantsDo($size, $from, $to, $transactionStates, $merchants)
    {
        $this->_params['size'] = $size;
        $this->_params['from'] = $from;
        $this->_params['to'] = $to;
        $this->_params['transactionStates'] = $transactionStates;
        $this->_params['merchants'] = $merchants;
        $result = $this->connect($this->apiUrl.'/rest/getLastOrdersForMerchants.do');

        return json_decode($result);
    }

    /**
     * Запрос списка возможных связок для мерчанта
     * @param  integer $user идентификатор клиента
     * @return string       json разультат запроса
     */
    public function getBindingsDo($user)
    {
        $this->_params['clientId'] = $user;
        $result = $this->connect($this->apiUrl.'/rest/getBindings.do');

        return json_decode($result);
    }

    /**
     * Запрос проведения платежа по связкам
     * @param  [type] $mdOrder   Номер заказа в платежной системе. Уникален в пределах системы
     * @param  [type] $bindingId Идентификатор связки созданной при оплате заказа или использованной для оплаты. Присутствует только если магазину разрешено создание связок.
     * @param  [type] $cvc       CVC код. Этот параметр обязателен, если для мерчанта не выбрано разрешение "Может проводить оплату без подтверждения CVC".
     * @return string  Массив с параметров ответа сервера
     */
    public function paymentOrderBindingDo($mdOrder, $bindingId, $cvc=null)
    {
        $this->_loginAuto();
        $this->_params['mdOrder'] = $mdOrder;
        $this->_params['bindingId'] = $bindingId;
        $this->_params['cvc'] = $cvc;
        $result = $this->connect($this->apiUrl.'/rest/paymentOrderBinding.do');

        return json_decode($result);
    }

    /**
     * Запрос деактивации связки
     * @param  [type] $bindingId Идентификатор связки созданной при оплате заказа или использованной для оплаты. Присутствует только если магазину разрешено создание связок.
     * @return string  Массив с параметров ответа сервера
     */
    public function unBindCardDo($bindingId)
    {
        $this->_params['bindingId'] = $bindingId;
        $result = $this->connect($this->apiUrl.'/rest/unBindCard.do');

        return json_decode($result);
    }

    /**
     * Запрос активации связки
     * @param  [type] $bindingId Идентификатор связки созданной при оплате заказа или использованной для оплаты. Присутствует только если магазину разрешено создание связок.
     * @return string  Массив с параметров ответа сервера
     */
    public function bindCardDo($bindingId)
    {
        $this->_params['bindingId'] = $bindingId;
        $result = $this->connect($this->apiUrl.'/rest/bindCard.do');

        return json_decode($result);
    }

    /**
     * Запрос изменения срока действия карты
     * @param  [type] $bindingId Идентификатор связки созданной при оплате заказа или использованной для оплаты. Присутствует только если магазину разрешено создание связок.
     * @param  [type] $newExpiry Новая дата (год и месяц) окончания срока действия в формате YYYYMM
     * @return string  Массив с параметров ответа сервера
     */
    public function extendBindingDo($bindingId, $newExpiry)
    {
        $this->_params['bindingId'] = $bindingId;
        $this->_params['newExpiry'] = $newExpiry;
        $result = $this->connect($this->apiUrl.'/rest/extendBinding.do');

        return json_decode($result);
    }
}
?>