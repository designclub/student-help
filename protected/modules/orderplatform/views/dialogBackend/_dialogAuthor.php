<div class="row">
	<?php if(count($dialog)> 0): ?>

	<div class="list-dialog">

		<?php foreach ($dialog as $d):	?>

		<?php if ($d->user_to == Yii::app()->getUser()->getId()): ?>

		<div class="col-sm-10 pull-right">
			<div class="row">
				<div class="col-sm-10">
					<div class="dialog-message dialog-message-user">
						<?= $d->message; ?>
						<span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $d->create_date); ?></span>
					</div>
				</div>
				<div class="col-sm-2">
					<label><?= $d->user->nick_name; ?></label>
				</div>
			</div>
		</div>

		<?php else: ?>

		<div class="col-sm-10" >
			<div class="row">
				<div class="col-sm-2">
					<label><?= $d->user->nick_name; ?></label>
				</div>
				<div class="col-sm-10">
					<div class="dialog-message">
						<?= $d->message; ?>
						<span><?= Yii::app()->dateFormatter->format("dd.MM.yyyyг.", $d->create_date); ?></span>
					</div>
				</div>
			</div>
		</div>

		<?php endif; ?>

		<?php endforeach; ?>

	</div>

	<?php else: ?>

	<div class="list-dialog">
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-2">
					<label></label>
				</div>
				<div class="col-sm-10">
					<div class="dialog-message dialog-none">
						Активных диалогов нет
						<span></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php endif; ?>
</div>