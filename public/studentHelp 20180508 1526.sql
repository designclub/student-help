﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.4.201.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 08.05.2018 15:26:23
-- Версия сервера: 5.7.16
-- Версия клиента: 4.1
--

--
-- Отключение внешних ключей
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Установить режим SQL (SQL mode)
--
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE help;

--
-- Удалить таблицу `sh_callback`
--
DROP TABLE IF EXISTS sh_callback;

--
-- Удалить таблицу `sh_contentblock_content_block`
--
DROP TABLE IF EXISTS sh_contentblock_content_block;

--
-- Удалить таблицу `sh_migrations`
--
DROP TABLE IF EXISTS sh_migrations;

--
-- Удалить таблицу `sh_options_order`
--
DROP TABLE IF EXISTS sh_options_order;

--
-- Удалить таблицу `sh_order_log`
--
DROP TABLE IF EXISTS sh_order_log;

--
-- Удалить таблицу `sh_status_pay`
--
DROP TABLE IF EXISTS sh_status_pay;

--
-- Удалить таблицу `sh_type_pay`
--
DROP TABLE IF EXISTS sh_type_pay;

--
-- Удалить таблицу `sh_blog_post_to_tag`
--
DROP TABLE IF EXISTS sh_blog_post_to_tag;

--
-- Удалить таблицу `sh_blog_tag`
--
DROP TABLE IF EXISTS sh_blog_tag;

--
-- Удалить таблицу `sh_mail_mail_template`
--
DROP TABLE IF EXISTS sh_mail_mail_template;

--
-- Удалить таблицу `sh_mail_mail_event`
--
DROP TABLE IF EXISTS sh_mail_mail_event;

--
-- Удалить таблицу `sh_menu_menu_item`
--
DROP TABLE IF EXISTS sh_menu_menu_item;

--
-- Удалить таблицу `sh_menu_menu`
--
DROP TABLE IF EXISTS sh_menu_menu;

--
-- Удалить таблицу `sh_user_user_auth_assignment`
--
DROP TABLE IF EXISTS sh_user_user_auth_assignment;

--
-- Удалить таблицу `sh_user_user_auth_item_child`
--
DROP TABLE IF EXISTS sh_user_user_auth_item_child;

--
-- Удалить таблицу `sh_user_user_auth_item`
--
DROP TABLE IF EXISTS sh_user_user_auth_item;

--
-- Удалить таблицу `sh_balance_user`
--
DROP TABLE IF EXISTS sh_balance_user;

--
-- Удалить таблицу `sh_orders_platform`
--
DROP TABLE IF EXISTS sh_orders_platform;

--
-- Удалить таблицу `sh_dialog_order_users`
--
DROP TABLE IF EXISTS sh_dialog_order_users;

--
-- Удалить таблицу `sh_status_order`
--
DROP TABLE IF EXISTS sh_status_order;

--
-- Удалить таблицу `sh_subject_order`
--
DROP TABLE IF EXISTS sh_subject_order;

--
-- Удалить таблицу `sh_types_order`
--
DROP TABLE IF EXISTS sh_types_order;

--
-- Удалить таблицу `sh_blog_post`
--
DROP TABLE IF EXISTS sh_blog_post;

--
-- Удалить таблицу `sh_blog_user_to_blog`
--
DROP TABLE IF EXISTS sh_blog_user_to_blog;

--
-- Удалить таблицу `sh_blog_blog`
--
DROP TABLE IF EXISTS sh_blog_blog;

--
-- Удалить таблицу `sh_comment_comment`
--
DROP TABLE IF EXISTS sh_comment_comment;

--
-- Удалить таблицу `sh_feedback_feedback`
--
DROP TABLE IF EXISTS sh_feedback_feedback;

--
-- Удалить таблицу `sh_gallery_image_to_gallery`
--
DROP TABLE IF EXISTS sh_gallery_image_to_gallery;

--
-- Удалить таблицу `sh_gallery_gallery`
--
DROP TABLE IF EXISTS sh_gallery_gallery;

--
-- Удалить таблицу `sh_image_image`
--
DROP TABLE IF EXISTS sh_image_image;

--
-- Удалить таблицу `sh_news_news`
--
DROP TABLE IF EXISTS sh_news_news;

--
-- Удалить таблицу `sh_notify_settings`
--
DROP TABLE IF EXISTS sh_notify_settings;

--
-- Удалить таблицу `sh_page_page`
--
DROP TABLE IF EXISTS sh_page_page;

--
-- Удалить таблицу `sh_user_tokens`
--
DROP TABLE IF EXISTS sh_user_tokens;

--
-- Удалить таблицу `sh_yupe_settings`
--
DROP TABLE IF EXISTS sh_yupe_settings;

--
-- Удалить таблицу `sh_user_user`
--
DROP TABLE IF EXISTS sh_user_user;

--
-- Удалить таблицу `sh_category_category`
--
DROP TABLE IF EXISTS sh_category_category;

--
-- Установка базы данных по умолчанию
--
USE help;

--
-- Создать таблицу `sh_category_category`
--
CREATE TABLE sh_category_category (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) DEFAULT NULL,
  slug varchar(150) NOT NULL,
  lang char(2) DEFAULT NULL,
  name varchar(250) NOT NULL,
  image varchar(250) DEFAULT NULL,
  short_description text DEFAULT NULL,
  description text NOT NULL,
  status tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_category_category_status` для объекта типа таблица `sh_category_category`
--
ALTER TABLE sh_category_category
ADD INDEX ix_sh_category_category_status (status);

--
-- Создать индекс `ux_sh_category_category_alias_lang` для объекта типа таблица `sh_category_category`
--
ALTER TABLE sh_category_category
ADD UNIQUE INDEX ux_sh_category_category_alias_lang (slug, lang);

--
-- Создать внешний ключ
--
ALTER TABLE sh_category_category
ADD CONSTRAINT fk_sh_category_category_parent_id FOREIGN KEY (parent_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_user_user`
--
CREATE TABLE sh_user_user (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  update_time datetime NOT NULL,
  first_name varchar(250) DEFAULT NULL,
  middle_name varchar(250) DEFAULT NULL,
  last_name varchar(250) DEFAULT NULL,
  nick_name varchar(250) NOT NULL,
  email varchar(250) NOT NULL,
  gender tinyint(1) NOT NULL DEFAULT 0,
  birth_date date DEFAULT NULL,
  site varchar(250) NOT NULL DEFAULT '',
  about varchar(250) NOT NULL DEFAULT '',
  location varchar(250) NOT NULL DEFAULT '',
  status int(11) NOT NULL DEFAULT 2,
  access_level int(11) NOT NULL DEFAULT 1 COMMENT 'по-умолчанию все администраторы',
  visit_time datetime DEFAULT NULL,
  create_time datetime NOT NULL,
  avatar varchar(150) DEFAULT NULL,
  hash varchar(255) NOT NULL DEFAULT '424975c21b3065c69a40ea150a3dc03d0.95943900 1519971334',
  email_confirm tinyint(1) NOT NULL DEFAULT 0,
  phone char(30) DEFAULT NULL,
  type_account tinyint(1) NOT NULL COMMENT '0-admin 1-автор 2-заказчик',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 3276,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_user_user_status` для объекта типа таблица `sh_user_user`
--
ALTER TABLE sh_user_user
ADD INDEX ix_sh_user_user_status (status);

--
-- Создать индекс `ux_sh_user_user_email` для объекта типа таблица `sh_user_user`
--
ALTER TABLE sh_user_user
ADD UNIQUE INDEX ux_sh_user_user_email (email);

--
-- Создать индекс `ux_sh_user_user_nick_name` для объекта типа таблица `sh_user_user`
--
ALTER TABLE sh_user_user
ADD UNIQUE INDEX ux_sh_user_user_nick_name (nick_name);

--
-- Создать таблицу `sh_yupe_settings`
--
CREATE TABLE sh_yupe_settings (
  id int(11) NOT NULL AUTO_INCREMENT,
  module_id varchar(100) NOT NULL,
  param_name varchar(100) NOT NULL,
  param_value varchar(500) NOT NULL,
  create_time datetime NOT NULL,
  update_time datetime NOT NULL,
  user_id int(11) DEFAULT NULL,
  type int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 37,
AVG_ROW_LENGTH = 455,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_yupe_settings_module_id` для объекта типа таблица `sh_yupe_settings`
--
ALTER TABLE sh_yupe_settings
ADD INDEX ix_sh_yupe_settings_module_id (module_id);

--
-- Создать индекс `ix_sh_yupe_settings_param_name` для объекта типа таблица `sh_yupe_settings`
--
ALTER TABLE sh_yupe_settings
ADD INDEX ix_sh_yupe_settings_param_name (param_name);

--
-- Создать индекс `ux_sh_yupe_settings_module_id_param_name_user_id` для объекта типа таблица `sh_yupe_settings`
--
ALTER TABLE sh_yupe_settings
ADD UNIQUE INDEX ux_sh_yupe_settings_module_id_param_name_user_id (module_id, param_name, user_id);

--
-- Создать внешний ключ
--
ALTER TABLE sh_yupe_settings
ADD CONSTRAINT fk_sh_yupe_settings_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_user_tokens`
--
CREATE TABLE sh_user_tokens (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  token varchar(255) DEFAULT NULL,
  type int(11) NOT NULL,
  status int(11) DEFAULT NULL,
  create_time datetime NOT NULL,
  update_time datetime DEFAULT NULL,
  ip varchar(255) DEFAULT NULL,
  expire_time datetime NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 23,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sh_user_tokens
ADD CONSTRAINT fk_sh_user_tokens_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Создать таблицу `sh_page_page`
--
CREATE TABLE sh_page_page (
  id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) DEFAULT NULL,
  lang char(2) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  create_time datetime NOT NULL,
  update_time datetime NOT NULL,
  user_id int(11) DEFAULT NULL,
  change_user_id int(11) DEFAULT NULL,
  title_short varchar(150) NOT NULL,
  title varchar(250) NOT NULL,
  slug varchar(150) NOT NULL,
  body mediumtext NOT NULL,
  keywords varchar(250) NOT NULL,
  description varchar(250) NOT NULL,
  status int(11) NOT NULL,
  is_protected tinyint(1) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  layout varchar(250) DEFAULT NULL,
  view varchar(250) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_page_page_category_id` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD INDEX ix_sh_page_page_category_id (category_id);

--
-- Создать индекс `ix_sh_page_page_change_user_id` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD INDEX ix_sh_page_page_change_user_id (change_user_id);

--
-- Создать индекс `ix_sh_page_page_is_protected` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD INDEX ix_sh_page_page_is_protected (is_protected);

--
-- Создать индекс `ix_sh_page_page_menu_order` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD INDEX ix_sh_page_page_menu_order (`order`);

--
-- Создать индекс `ix_sh_page_page_status` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD INDEX ix_sh_page_page_status (status);

--
-- Создать индекс `ix_sh_page_page_user_id` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD INDEX ix_sh_page_page_user_id (user_id);

--
-- Создать индекс `ux_sh_page_page_slug_lang` для объекта типа таблица `sh_page_page`
--
ALTER TABLE sh_page_page
ADD UNIQUE INDEX ux_sh_page_page_slug_lang (slug, lang);

--
-- Создать внешний ключ
--
ALTER TABLE sh_page_page
ADD CONSTRAINT fk_sh_page_page_category_id FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_page_page
ADD CONSTRAINT fk_sh_page_page_change_user_id FOREIGN KEY (change_user_id)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_page_page
ADD CONSTRAINT fk_sh_page_page_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_notify_settings`
--
CREATE TABLE sh_notify_settings (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  my_post tinyint(1) NOT NULL DEFAULT 1,
  my_comment tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sh_notify_settings
ADD CONSTRAINT fk_sh_notify_settings_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_news_news`
--
CREATE TABLE sh_news_news (
  id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) DEFAULT NULL,
  lang char(2) DEFAULT NULL,
  create_time datetime NOT NULL,
  update_time datetime NOT NULL,
  date date NOT NULL,
  title varchar(250) NOT NULL,
  slug varchar(150) NOT NULL,
  short_text text DEFAULT NULL,
  full_text text NOT NULL,
  image varchar(300) DEFAULT NULL,
  link varchar(300) DEFAULT NULL,
  user_id int(11) DEFAULT NULL,
  status int(11) NOT NULL DEFAULT 0,
  is_protected tinyint(1) NOT NULL DEFAULT 0,
  keywords varchar(250) NOT NULL,
  description varchar(250) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 14,
AVG_ROW_LENGTH = 1260,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_news_news_category_id` для объекта типа таблица `sh_news_news`
--
ALTER TABLE sh_news_news
ADD INDEX ix_sh_news_news_category_id (category_id);

--
-- Создать индекс `ix_sh_news_news_date` для объекта типа таблица `sh_news_news`
--
ALTER TABLE sh_news_news
ADD INDEX ix_sh_news_news_date (date);

--
-- Создать индекс `ix_sh_news_news_status` для объекта типа таблица `sh_news_news`
--
ALTER TABLE sh_news_news
ADD INDEX ix_sh_news_news_status (status);

--
-- Создать индекс `ix_sh_news_news_user_id` для объекта типа таблица `sh_news_news`
--
ALTER TABLE sh_news_news
ADD INDEX ix_sh_news_news_user_id (user_id);

--
-- Создать индекс `ux_sh_news_news_alias_lang` для объекта типа таблица `sh_news_news`
--
ALTER TABLE sh_news_news
ADD UNIQUE INDEX ux_sh_news_news_alias_lang (slug, lang);

--
-- Создать внешний ключ
--
ALTER TABLE sh_news_news
ADD CONSTRAINT fk_sh_news_news_category_id FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_news_news
ADD CONSTRAINT fk_sh_news_news_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_image_image`
--
CREATE TABLE sh_image_image (
  id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) DEFAULT NULL,
  parent_id int(11) DEFAULT NULL,
  name varchar(250) NOT NULL,
  description text DEFAULT NULL,
  file varchar(250) NOT NULL,
  create_time datetime NOT NULL,
  user_id int(11) DEFAULT NULL,
  alt varchar(250) NOT NULL,
  type int(11) NOT NULL DEFAULT 0,
  status int(11) NOT NULL DEFAULT 1,
  sort int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_image_image_category_id` для объекта типа таблица `sh_image_image`
--
ALTER TABLE sh_image_image
ADD INDEX ix_sh_image_image_category_id (category_id);

--
-- Создать индекс `ix_sh_image_image_status` для объекта типа таблица `sh_image_image`
--
ALTER TABLE sh_image_image
ADD INDEX ix_sh_image_image_status (status);

--
-- Создать индекс `ix_sh_image_image_type` для объекта типа таблица `sh_image_image`
--
ALTER TABLE sh_image_image
ADD INDEX ix_sh_image_image_type (type);

--
-- Создать индекс `ix_sh_image_image_user` для объекта типа таблица `sh_image_image`
--
ALTER TABLE sh_image_image
ADD INDEX ix_sh_image_image_user (user_id);

--
-- Создать внешний ключ
--
ALTER TABLE sh_image_image
ADD CONSTRAINT fk_sh_image_image_category_id FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_image_image
ADD CONSTRAINT fk_sh_image_image_parent_id FOREIGN KEY (parent_id)
REFERENCES sh_image_image (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_image_image
ADD CONSTRAINT fk_sh_image_image_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_gallery_gallery`
--
CREATE TABLE sh_gallery_gallery (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(250) NOT NULL,
  description text DEFAULT NULL,
  status int(11) NOT NULL DEFAULT 1,
  owner int(11) DEFAULT NULL,
  preview_id int(11) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_gallery_gallery_owner` для объекта типа таблица `sh_gallery_gallery`
--
ALTER TABLE sh_gallery_gallery
ADD INDEX ix_sh_gallery_gallery_owner (owner);

--
-- Создать индекс `ix_sh_gallery_gallery_status` для объекта типа таблица `sh_gallery_gallery`
--
ALTER TABLE sh_gallery_gallery
ADD INDEX ix_sh_gallery_gallery_status (status);

--
-- Создать внешний ключ
--
ALTER TABLE sh_gallery_gallery
ADD CONSTRAINT fk_sh_gallery_gallery_gallery_preview_to_image FOREIGN KEY (preview_id)
REFERENCES sh_image_image (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_gallery_gallery
ADD CONSTRAINT fk_sh_gallery_gallery_gallery_to_category FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_gallery_gallery
ADD CONSTRAINT fk_sh_gallery_gallery_owner FOREIGN KEY (owner)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_gallery_image_to_gallery`
--
CREATE TABLE sh_gallery_image_to_gallery (
  id int(11) NOT NULL AUTO_INCREMENT,
  image_id int(11) NOT NULL,
  gallery_id int(11) NOT NULL,
  create_time datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_gallery_image_to_gallery_gallery_to_image_gallery` для объекта типа таблица `sh_gallery_image_to_gallery`
--
ALTER TABLE sh_gallery_image_to_gallery
ADD INDEX ix_sh_gallery_image_to_gallery_gallery_to_image_gallery (gallery_id);

--
-- Создать индекс `ix_sh_gallery_image_to_gallery_gallery_to_image_image` для объекта типа таблица `sh_gallery_image_to_gallery`
--
ALTER TABLE sh_gallery_image_to_gallery
ADD INDEX ix_sh_gallery_image_to_gallery_gallery_to_image_image (image_id);

--
-- Создать индекс `ux_sh_gallery_image_to_gallery_gallery_to_image` для объекта типа таблица `sh_gallery_image_to_gallery`
--
ALTER TABLE sh_gallery_image_to_gallery
ADD UNIQUE INDEX ux_sh_gallery_image_to_gallery_gallery_to_image (image_id, gallery_id);

--
-- Создать внешний ключ
--
ALTER TABLE sh_gallery_image_to_gallery
ADD CONSTRAINT fk_sh_gallery_image_to_gallery_gallery_to_image_gallery FOREIGN KEY (gallery_id)
REFERENCES sh_gallery_gallery (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_gallery_image_to_gallery
ADD CONSTRAINT fk_sh_gallery_image_to_gallery_gallery_to_image_image FOREIGN KEY (image_id)
REFERENCES sh_image_image (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_feedback_feedback`
--
CREATE TABLE sh_feedback_feedback (
  id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) DEFAULT NULL,
  answer_user int(11) DEFAULT NULL,
  create_time datetime NOT NULL,
  update_time datetime NOT NULL,
  name varchar(150) NOT NULL,
  email varchar(150) NOT NULL,
  phone varchar(150) DEFAULT NULL,
  theme varchar(250) NOT NULL,
  text text NOT NULL,
  type int(11) NOT NULL DEFAULT 0,
  answer text NOT NULL,
  answer_time datetime DEFAULT NULL,
  is_faq int(11) NOT NULL DEFAULT 0,
  status int(11) NOT NULL DEFAULT 0,
  ip varchar(20) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_feedback_feedback_answer_user` для объекта типа таблица `sh_feedback_feedback`
--
ALTER TABLE sh_feedback_feedback
ADD INDEX ix_sh_feedback_feedback_answer_user (answer_user);

--
-- Создать индекс `ix_sh_feedback_feedback_category` для объекта типа таблица `sh_feedback_feedback`
--
ALTER TABLE sh_feedback_feedback
ADD INDEX ix_sh_feedback_feedback_category (category_id);

--
-- Создать индекс `ix_sh_feedback_feedback_isfaq` для объекта типа таблица `sh_feedback_feedback`
--
ALTER TABLE sh_feedback_feedback
ADD INDEX ix_sh_feedback_feedback_isfaq (is_faq);

--
-- Создать индекс `ix_sh_feedback_feedback_status` для объекта типа таблица `sh_feedback_feedback`
--
ALTER TABLE sh_feedback_feedback
ADD INDEX ix_sh_feedback_feedback_status (status);

--
-- Создать индекс `ix_sh_feedback_feedback_type` для объекта типа таблица `sh_feedback_feedback`
--
ALTER TABLE sh_feedback_feedback
ADD INDEX ix_sh_feedback_feedback_type (type);

--
-- Создать внешний ключ
--
ALTER TABLE sh_feedback_feedback
ADD CONSTRAINT fk_sh_feedback_feedback_answer_user FOREIGN KEY (answer_user)
REFERENCES sh_user_user (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_feedback_feedback
ADD CONSTRAINT fk_sh_feedback_feedback_category FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_comment_comment`
--
CREATE TABLE sh_comment_comment (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) DEFAULT NULL,
  user_id int(11) DEFAULT NULL,
  model varchar(100) NOT NULL,
  model_id int(11) NOT NULL,
  url varchar(150) DEFAULT NULL,
  create_time datetime NOT NULL,
  name varchar(150) NOT NULL,
  email varchar(150) NOT NULL,
  text text NOT NULL,
  status int(11) NOT NULL DEFAULT 0,
  ip varchar(20) DEFAULT NULL,
  level int(11) DEFAULT 0,
  root int(11) DEFAULT 0,
  lft int(11) DEFAULT 0,
  rgt int(11) DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_comment_comment_level` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_level (level);

--
-- Создать индекс `ix_sh_comment_comment_lft` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_lft (lft);

--
-- Создать индекс `ix_sh_comment_comment_model` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_model (model);

--
-- Создать индекс `ix_sh_comment_comment_model_id` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_model_id (model_id);

--
-- Создать индекс `ix_sh_comment_comment_model_model_id` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_model_model_id (model, model_id);

--
-- Создать индекс `ix_sh_comment_comment_parent_id` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_parent_id (parent_id);

--
-- Создать индекс `ix_sh_comment_comment_rgt` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_rgt (rgt);

--
-- Создать индекс `ix_sh_comment_comment_root` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_root (root);

--
-- Создать индекс `ix_sh_comment_comment_status` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_status (status);

--
-- Создать индекс `ix_sh_comment_comment_user_id` для объекта типа таблица `sh_comment_comment`
--
ALTER TABLE sh_comment_comment
ADD INDEX ix_sh_comment_comment_user_id (user_id);

--
-- Создать внешний ключ
--
ALTER TABLE sh_comment_comment
ADD CONSTRAINT fk_sh_comment_comment_parent_id FOREIGN KEY (parent_id)
REFERENCES sh_comment_comment (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_comment_comment
ADD CONSTRAINT fk_sh_comment_comment_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_blog_blog`
--
CREATE TABLE sh_blog_blog (
  id int(11) NOT NULL AUTO_INCREMENT,
  category_id int(11) DEFAULT NULL,
  name varchar(250) NOT NULL,
  description text DEFAULT NULL,
  icon varchar(250) NOT NULL DEFAULT '',
  slug varchar(150) NOT NULL,
  lang char(2) DEFAULT NULL,
  type int(11) NOT NULL DEFAULT 1,
  status int(11) NOT NULL DEFAULT 1,
  create_user_id int(11) NOT NULL,
  update_user_id int(11) NOT NULL,
  create_time int(11) NOT NULL,
  update_time int(11) NOT NULL,
  member_status int(11) NOT NULL DEFAULT 1,
  post_status int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_blog_blog_create_date` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_create_date (create_time);

--
-- Создать индекс `ix_sh_blog_blog_create_user` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_create_user (create_user_id);

--
-- Создать индекс `ix_sh_blog_blog_lang` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_lang (lang);

--
-- Создать индекс `ix_sh_blog_blog_slug` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_slug (slug);

--
-- Создать индекс `ix_sh_blog_blog_status` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_status (status);

--
-- Создать индекс `ix_sh_blog_blog_type` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_type (type);

--
-- Создать индекс `ix_sh_blog_blog_update_date` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_update_date (update_time);

--
-- Создать индекс `ix_sh_blog_blog_update_user` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD INDEX ix_sh_blog_blog_update_user (update_user_id);

--
-- Создать индекс `ux_sh_blog_blog_slug_lang` для объекта типа таблица `sh_blog_blog`
--
ALTER TABLE sh_blog_blog
ADD UNIQUE INDEX ux_sh_blog_blog_slug_lang (slug, lang);

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_blog
ADD CONSTRAINT fk_sh_blog_blog_category_id FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_blog
ADD CONSTRAINT fk_sh_blog_blog_create_user FOREIGN KEY (create_user_id)
REFERENCES sh_user_user (id) ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_blog
ADD CONSTRAINT fk_sh_blog_blog_update_user FOREIGN KEY (update_user_id)
REFERENCES sh_user_user (id) ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_blog_user_to_blog`
--
CREATE TABLE sh_blog_user_to_blog (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  blog_id int(11) NOT NULL,
  create_time int(11) NOT NULL,
  update_time int(11) NOT NULL,
  role int(11) NOT NULL DEFAULT 1,
  status int(11) NOT NULL DEFAULT 1,
  note varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_blog_user_to_blog_blog_user_to_blog_id` для объекта типа таблица `sh_blog_user_to_blog`
--
ALTER TABLE sh_blog_user_to_blog
ADD INDEX ix_sh_blog_user_to_blog_blog_user_to_blog_id (blog_id);

--
-- Создать индекс `ix_sh_blog_user_to_blog_blog_user_to_blog_role` для объекта типа таблица `sh_blog_user_to_blog`
--
ALTER TABLE sh_blog_user_to_blog
ADD INDEX ix_sh_blog_user_to_blog_blog_user_to_blog_role (role);

--
-- Создать индекс `ix_sh_blog_user_to_blog_blog_user_to_blog_status` для объекта типа таблица `sh_blog_user_to_blog`
--
ALTER TABLE sh_blog_user_to_blog
ADD INDEX ix_sh_blog_user_to_blog_blog_user_to_blog_status (status);

--
-- Создать индекс `ix_sh_blog_user_to_blog_blog_user_to_blog_user_id` для объекта типа таблица `sh_blog_user_to_blog`
--
ALTER TABLE sh_blog_user_to_blog
ADD INDEX ix_sh_blog_user_to_blog_blog_user_to_blog_user_id (user_id);

--
-- Создать индекс `ux_sh_blog_user_to_blog_blog_user_to_blog_u_b` для объекта типа таблица `sh_blog_user_to_blog`
--
ALTER TABLE sh_blog_user_to_blog
ADD UNIQUE INDEX ux_sh_blog_user_to_blog_blog_user_to_blog_u_b (user_id, blog_id);

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_user_to_blog
ADD CONSTRAINT fk_sh_blog_user_to_blog_blog_user_to_blog_blog_id FOREIGN KEY (blog_id)
REFERENCES sh_blog_blog (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_user_to_blog
ADD CONSTRAINT fk_sh_blog_user_to_blog_blog_user_to_blog_user_id FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_blog_post`
--
CREATE TABLE sh_blog_post (
  id int(11) NOT NULL AUTO_INCREMENT,
  blog_id int(11) NOT NULL,
  create_user_id int(11) NOT NULL,
  update_user_id int(11) NOT NULL,
  create_time int(11) NOT NULL,
  update_time int(11) NOT NULL,
  publish_time int(11) NOT NULL,
  slug varchar(150) NOT NULL,
  lang char(2) DEFAULT NULL,
  title varchar(250) NOT NULL,
  quote text DEFAULT NULL,
  content text NOT NULL,
  link varchar(250) NOT NULL DEFAULT '',
  status int(11) NOT NULL DEFAULT 0,
  comment_status int(11) NOT NULL DEFAULT 1,
  create_user_ip varchar(20) NOT NULL,
  access_type int(11) NOT NULL DEFAULT 1,
  keywords varchar(250) NOT NULL DEFAULT '',
  description varchar(250) NOT NULL DEFAULT '',
  image varchar(300) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_blog_post_access_type` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_access_type (access_type);

--
-- Создать индекс `ix_sh_blog_post_blog_id` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_blog_id (blog_id);

--
-- Создать индекс `ix_sh_blog_post_comment_status` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_comment_status (comment_status);

--
-- Создать индекс `ix_sh_blog_post_create_user_id` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_create_user_id (create_user_id);

--
-- Создать индекс `ix_sh_blog_post_lang` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_lang (lang);

--
-- Создать индекс `ix_sh_blog_post_publish_date` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_publish_date (publish_time);

--
-- Создать индекс `ix_sh_blog_post_slug` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_slug (slug);

--
-- Создать индекс `ix_sh_blog_post_status` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_status (status);

--
-- Создать индекс `ix_sh_blog_post_update_user_id` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD INDEX ix_sh_blog_post_update_user_id (update_user_id);

--
-- Создать индекс `ux_sh_blog_post_lang_slug` для объекта типа таблица `sh_blog_post`
--
ALTER TABLE sh_blog_post
ADD UNIQUE INDEX ux_sh_blog_post_lang_slug (slug, lang);

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_post
ADD CONSTRAINT fk_sh_blog_post_blog FOREIGN KEY (blog_id)
REFERENCES sh_blog_blog (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_post
ADD CONSTRAINT fk_sh_blog_post_category_id FOREIGN KEY (category_id)
REFERENCES sh_category_category (id) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_post
ADD CONSTRAINT fk_sh_blog_post_create_user FOREIGN KEY (create_user_id)
REFERENCES sh_user_user (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_post
ADD CONSTRAINT fk_sh_blog_post_update_user FOREIGN KEY (update_user_id)
REFERENCES sh_user_user (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_types_order`
--
CREATE TABLE sh_types_order (
  id int(11) NOT NULL AUTO_INCREMENT,
  name_type varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_subject_order`
--
CREATE TABLE sh_subject_order (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) DEFAULT NULL,
  name_subject varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 3,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_status_order`
--
CREATE TABLE sh_status_order (
  id int(11) NOT NULL AUTO_INCREMENT,
  name_status varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
AVG_ROW_LENGTH = 1638,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_dialog_order_users`
--
CREATE TABLE sh_dialog_order_users (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT '0 - опубликован, 1 - удален',
  create_date timestamp NULL DEFAULT NULL,
  update_date timestamp NULL DEFAULT NULL,
  message text DEFAULT NULL,
  order_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  status_id tinyint(1) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_orders_platform`
--
CREATE TABLE sh_orders_platform (
  id int(11) NOT NULL AUTO_INCREMENT,
  create_date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата создания',
  update_date timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата редактирования',
  target_date timestamp NOT NULL COMMENT 'время сдачи заказа',
  title varchar(500) NOT NULL COMMENT 'Наименование',
  theme varchar(500) DEFAULT NULL COMMENT 'Тема',
  text text NOT NULL COMMENT 'Описание заказа',
  budget decimal(10, 2) DEFAULT 0.00 COMMENT 'Бюджет (предполагаемый)',
  cost decimal(10, 2) DEFAULT NULL COMMENT 'окончательная стоимость',
  file varchar(500) DEFAULT NULL,
  type_id int(11) NOT NULL COMMENT 'тип заказа',
  subject_id int(11) NOT NULL COMMENT 'предмет',
  status_id int(11) NOT NULL COMMENT 'статус заказа',
  client_id int(11) NOT NULL,
  author_id int(11) DEFAULT NULL,
  dialog_id int(11) DEFAULT NULL COMMENT 'ссылка на диалог автора с клиентом',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sh_orders_platform
ADD CONSTRAINT FK_sh_orders_platform_author FOREIGN KEY (author_id)
REFERENCES sh_user_user (id) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_orders_platform
ADD CONSTRAINT FK_sh_orders_platform_client FOREIGN KEY (client_id)
REFERENCES sh_user_user (id) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_orders_platform
ADD CONSTRAINT FK_sh_orders_platform_dialog FOREIGN KEY (dialog_id)
REFERENCES sh_dialog_order_users (id) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_orders_platform
ADD CONSTRAINT FK_sh_orders_platform_status FOREIGN KEY (status_id)
REFERENCES sh_status_order (id) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_orders_platform
ADD CONSTRAINT FK_sh_orders_platform_subject FOREIGN KEY (subject_id)
REFERENCES sh_subject_order (id) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_orders_platform
ADD CONSTRAINT FK_sh_orders_platform_type FOREIGN KEY (type_id)
REFERENCES sh_types_order (id) ON DELETE NO ACTION;

--
-- Создать таблицу `sh_balance_user`
--
CREATE TABLE sh_balance_user (
  id int(11) NOT NULL AUTO_INCREMENT,
  date timestamp NULL DEFAULT '0000-00-00 00:00:00',
  balance decimal(10, 2) NOT NULL DEFAULT 0.00,
  expense decimal(10, 2) DEFAULT 0.00 COMMENT 'расход',
  arrival decimal(10, 2) DEFAULT 0.00 COMMENT 'приход',
  comment text DEFAULT NULL,
  user_id int(11) NOT NULL,
  order_id int(11) DEFAULT NULL,
  type_id tinyint(1) NOT NULL,
  status_id tinyint(1) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sh_balance_user
ADD CONSTRAINT FK_sh_balance_user FOREIGN KEY (user_id)
REFERENCES sh_user_user (id) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_balance_user
ADD CONSTRAINT FK_sh_balance_user_order FOREIGN KEY (order_id)
REFERENCES sh_orders_platform (id) ON DELETE NO ACTION;

--
-- Создать таблицу `sh_user_user_auth_item`
--
CREATE TABLE sh_user_user_auth_item (
  name char(64) NOT NULL,
  type int(11) NOT NULL,
  description text DEFAULT NULL,
  bizrule text DEFAULT NULL,
  data text DEFAULT NULL,
  PRIMARY KEY (name)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 2340,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_user_user_auth_item_type` для объекта типа таблица `sh_user_user_auth_item`
--
ALTER TABLE sh_user_user_auth_item
ADD INDEX ix_sh_user_user_auth_item_type (type);

--
-- Создать таблицу `sh_user_user_auth_item_child`
--
CREATE TABLE sh_user_user_auth_item_child (
  parent char(64) NOT NULL,
  child char(64) NOT NULL,
  PRIMARY KEY (parent, child)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sh_user_user_auth_item_child
ADD CONSTRAINT fk_sh_user_user_auth_item_child_child FOREIGN KEY (child)
REFERENCES sh_user_user_auth_item (name) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Создать внешний ключ
--
ALTER TABLE sh_user_user_auth_item_child
ADD CONSTRAINT fk_sh_user_user_auth_itemchild_parent FOREIGN KEY (parent)
REFERENCES sh_user_user_auth_item (name) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Создать таблицу `sh_user_user_auth_assignment`
--
CREATE TABLE sh_user_user_auth_assignment (
  itemname char(64) NOT NULL,
  userid int(11) NOT NULL,
  bizrule text DEFAULT NULL,
  data text DEFAULT NULL,
  PRIMARY KEY (itemname, userid)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sh_user_user_auth_assignment
ADD CONSTRAINT fk_sh_user_user_auth_assignment_item FOREIGN KEY (itemname)
REFERENCES sh_user_user_auth_item (name) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Создать внешний ключ
--
ALTER TABLE sh_user_user_auth_assignment
ADD CONSTRAINT fk_sh_user_user_auth_assignment_user FOREIGN KEY (userid)
REFERENCES sh_user_user (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Создать таблицу `sh_menu_menu`
--
CREATE TABLE sh_menu_menu (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  code varchar(255) NOT NULL,
  description varchar(255) NOT NULL,
  status int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_menu_menu_status` для объекта типа таблица `sh_menu_menu`
--
ALTER TABLE sh_menu_menu
ADD INDEX ix_sh_menu_menu_status (status);

--
-- Создать индекс `ux_sh_menu_menu_code` для объекта типа таблица `sh_menu_menu`
--
ALTER TABLE sh_menu_menu
ADD UNIQUE INDEX ux_sh_menu_menu_code (code);

--
-- Создать таблицу `sh_menu_menu_item`
--
CREATE TABLE sh_menu_menu_item (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) NOT NULL,
  menu_id int(11) NOT NULL,
  regular_link tinyint(1) NOT NULL DEFAULT 0,
  title varchar(150) NOT NULL,
  href varchar(150) NOT NULL,
  class varchar(150) DEFAULT NULL,
  title_attr varchar(150) DEFAULT NULL,
  before_link varchar(150) DEFAULT NULL,
  after_link varchar(150) DEFAULT NULL,
  target varchar(150) DEFAULT NULL,
  rel varchar(150) DEFAULT NULL,
  condition_name varchar(150) DEFAULT '0',
  condition_denial int(11) DEFAULT 0,
  sort int(11) NOT NULL DEFAULT 1,
  status int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 38,
AVG_ROW_LENGTH = 630,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_menu_menu_item_sort` для объекта типа таблица `sh_menu_menu_item`
--
ALTER TABLE sh_menu_menu_item
ADD INDEX ix_sh_menu_menu_item_sort (sort);

--
-- Создать индекс `ix_sh_menu_menu_item_status` для объекта типа таблица `sh_menu_menu_item`
--
ALTER TABLE sh_menu_menu_item
ADD INDEX ix_sh_menu_menu_item_status (status);

--
-- Создать внешний ключ
--
ALTER TABLE sh_menu_menu_item
ADD CONSTRAINT fk_sh_menu_menu_item_menu_id FOREIGN KEY (menu_id)
REFERENCES sh_menu_menu (id) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Создать таблицу `sh_mail_mail_event`
--
CREATE TABLE sh_mail_mail_event (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(150) NOT NULL,
  name varchar(150) NOT NULL,
  description text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ux_sh_mail_mail_event_code` для объекта типа таблица `sh_mail_mail_event`
--
ALTER TABLE sh_mail_mail_event
ADD UNIQUE INDEX ux_sh_mail_mail_event_code (code);

--
-- Создать таблицу `sh_mail_mail_template`
--
CREATE TABLE sh_mail_mail_template (
  id int(11) NOT NULL AUTO_INCREMENT,
  code varchar(150) NOT NULL,
  event_id int(11) NOT NULL,
  name varchar(150) NOT NULL,
  description text DEFAULT NULL,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  theme text NOT NULL,
  body text NOT NULL,
  status int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_mail_mail_template_status` для объекта типа таблица `sh_mail_mail_template`
--
ALTER TABLE sh_mail_mail_template
ADD INDEX ix_sh_mail_mail_template_status (status);

--
-- Создать индекс `ux_sh_mail_mail_template_code` для объекта типа таблица `sh_mail_mail_template`
--
ALTER TABLE sh_mail_mail_template
ADD UNIQUE INDEX ux_sh_mail_mail_template_code (code);

--
-- Создать внешний ключ
--
ALTER TABLE sh_mail_mail_template
ADD CONSTRAINT fk_sh_mail_mail_template_event_id FOREIGN KEY (event_id)
REFERENCES sh_mail_mail_event (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_blog_tag`
--
CREATE TABLE sh_blog_tag (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ux_sh_blog_tag_tag_name` для объекта типа таблица `sh_blog_tag`
--
ALTER TABLE sh_blog_tag
ADD UNIQUE INDEX ux_sh_blog_tag_tag_name (name);

--
-- Создать таблицу `sh_blog_post_to_tag`
--
CREATE TABLE sh_blog_post_to_tag (
  post_id int(11) NOT NULL,
  tag_id int(11) NOT NULL,
  PRIMARY KEY (post_id, tag_id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_blog_post_to_tag_post_id` для объекта типа таблица `sh_blog_post_to_tag`
--
ALTER TABLE sh_blog_post_to_tag
ADD INDEX ix_sh_blog_post_to_tag_post_id (post_id);

--
-- Создать индекс `ix_sh_blog_post_to_tag_tag_id` для объекта типа таблица `sh_blog_post_to_tag`
--
ALTER TABLE sh_blog_post_to_tag
ADD INDEX ix_sh_blog_post_to_tag_tag_id (tag_id);

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_post_to_tag
ADD CONSTRAINT fk_sh_blog_post_to_tag_post_id FOREIGN KEY (post_id)
REFERENCES sh_blog_post (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sh_blog_post_to_tag
ADD CONSTRAINT fk_sh_blog_post_to_tag_tag_id FOREIGN KEY (tag_id)
REFERENCES sh_blog_tag (id) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Создать таблицу `sh_type_pay`
--
CREATE TABLE sh_type_pay (
  id int(11) NOT NULL AUTO_INCREMENT,
  name_type varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_status_pay`
--
CREATE TABLE sh_status_pay (
  id int(11) NOT NULL AUTO_INCREMENT,
  name_status varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_order_log`
--
CREATE TABLE sh_order_log (
  id int(11) NOT NULL AUTO_INCREMENT,
  date_create timestamp NOT NULL,
  date_update timestamp NOT NULL,
  order_id int(11) NOT NULL,
  client_id int(11) DEFAULT NULL,
  author_id int(11) DEFAULT NULL,
  cost decimal(10, 2) DEFAULT NULL COMMENT 'стоимость',
  text_order text DEFAULT NULL COMMENT 'текст изменного заказа',
  comment text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sh_options_order`
--
CREATE TABLE sh_options_order (
  id int(11) NOT NULL AUTO_INCREMENT,
  page_from int(11) DEFAULT NULL,
  page_to int(11) DEFAULT NULL,
  originality int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `id` для объекта типа таблица `sh_options_order`
--
ALTER TABLE sh_options_order
ADD UNIQUE INDEX id (id);

--
-- Создать таблицу `sh_migrations`
--
CREATE TABLE sh_migrations (
  id int(11) NOT NULL AUTO_INCREMENT,
  module varchar(255) NOT NULL,
  version varchar(255) NOT NULL,
  apply_time int(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 62,
AVG_ROW_LENGTH = 303,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `idx_migrations_module` для объекта типа таблица `sh_migrations`
--
ALTER TABLE sh_migrations
ADD INDEX idx_migrations_module (module);

--
-- Создать таблицу `sh_contentblock_content_block`
--
CREATE TABLE sh_contentblock_content_block (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(250) NOT NULL,
  code varchar(100) NOT NULL,
  type int(11) NOT NULL DEFAULT 1,
  content text NOT NULL,
  description varchar(255) DEFAULT NULL,
  category_id int(11) DEFAULT NULL,
  status tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 12,
AVG_ROW_LENGTH = 2048,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать индекс `ix_sh_contentblock_content_block_status` для объекта типа таблица `sh_contentblock_content_block`
--
ALTER TABLE sh_contentblock_content_block
ADD INDEX ix_sh_contentblock_content_block_status (status);

--
-- Создать индекс `ix_sh_contentblock_content_block_type` для объекта типа таблица `sh_contentblock_content_block`
--
ALTER TABLE sh_contentblock_content_block
ADD INDEX ix_sh_contentblock_content_block_type (type);

--
-- Создать индекс `ux_sh_contentblock_content_block_code` для объекта типа таблица `sh_contentblock_content_block`
--
ALTER TABLE sh_contentblock_content_block
ADD UNIQUE INDEX ux_sh_contentblock_content_block_code (code);

--
-- Создать таблицу `sh_callback`
--
CREATE TABLE sh_callback (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  phone varchar(255) DEFAULT NULL,
  time varchar(255) DEFAULT NULL,
  comment varchar(255) DEFAULT NULL,
  status int(11) DEFAULT 0,
  create_time datetime DEFAULT NULL,
  url text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Вывод данных для таблицы sh_category_category
--
INSERT INTO sh_category_category VALUES
(1, NULL, 'kak-eto-rabotaet', 'ru', 'Как это работает', NULL, '', '', 1),
(2, NULL, 'otzyvy-studentov', 'ru', 'Отзывы студентов', NULL, '', '', 1),
(3, NULL, 'nashi-preimushchestva', 'ru', 'Наши преимущества', NULL, '', '', 1);

--
-- Вывод данных для таблицы sh_user_user
--
INSERT INTO sh_user_user VALUES
(1, '2018-03-02 11:29:18', '', '', '', 'admin', 'doctordrethe@mail.ru', 0, NULL, '', '', '', 1, 1, '2018-05-08 11:40:02', '2018-03-02 11:29:18', NULL, '$2y$13$BzZb4oqHEStqiHBYyhUYK./1gY.vlBNJRM40uRuvQl98NOcJaHudi', 1, NULL, 0),
(2, '2018-05-02 13:38:11', '', '', '', 'client', 'client@test.ru', 0, NULL, '', '', '', 1, 1, '2018-05-02 16:27:41', '2018-05-02 13:35:02', NULL, '$2y$13$q4R0DK8dvd91lN35Q1vYTewpQ9ZC6LxVv7S8/v5G3AaJoLD2kw9Si', 1, '79123456789', 1),
(3, '2018-05-02 13:46:51', '', '', '', 'author', 'author@test.ru', 0, NULL, '', '', '', 1, 1, '2018-05-08 12:06:34', '2018-05-02 13:46:25', NULL, '$2y$13$3yT6F3aQARGCCqFZw768rO6HAR9X5fQw5KAoFwG.RvS7tXVOJCC3G', 1, '+79225490195', 2),
(4, '2018-05-03 06:07:37', '', '', '', 'test', 'test@test.ru', 0, NULL, '', '', '', 1, 1, '2018-05-08 12:14:44', '2018-05-02 15:10:17', '4_1525309656.png', '$2y$13$BaINOmOR0u3XYHTJyVUxyuqqcQXq51fIcsCwuk3I2bf6FNfDIqtOq', 1, '+7-922-549-0195', 1),
(5, '2018-05-02 16:10:26', '', '', '', 'author2', 'tets@asdas.aa', 0, NULL, '', '', '', 1, 1, '2018-05-02 16:10:32', '2018-05-02 16:10:26', NULL, '$2y$13$WcStOGVVsq5JovSoVGkpGeRTSkCaPdKHMR0Dbd3dr/arAnskzAF2W', 1, '+79225490195', 2);

--
-- Вывод данных для таблицы sh_types_order
--
INSERT INTO sh_types_order VALUES
(1, 'Курсовая работа'),
(2, 'Курсовой проект'),
(3, 'Диплом'),
(4, 'Контрольная работа');

--
-- Вывод данных для таблицы sh_subject_order
--
INSERT INTO sh_subject_order VALUES
(1, NULL, 'Математика'),
(2, NULL, 'Высшая математика');

--
-- Вывод данных для таблицы sh_status_order
--
INSERT INTO sh_status_order VALUES
(1, 'Создан');

--
-- Вывод данных для таблицы sh_dialog_order_users
--
-- Таблица help.sh_dialog_order_users не содержит данных

--
-- Вывод данных для таблицы sh_user_user_auth_item
--
INSERT INTO sh_user_user_auth_item VALUES
('admin', 2, 'Администратор', NULL, NULL),
('Client.ClientBackend.Index', 0, 'Index', NULL, NULL),
('Client.ClientManager', 1, 'Manage client', NULL, NULL),
('Orderplatform.OrderplatformBackend.Create', 0, 'Создать заказ', NULL, NULL),
('Orderplatform.OrderplatformBackend.Index', 0, 'Мои заказы', NULL, NULL),
('Orderplatform.OrderplatformBackend.List', 0, 'Найти заказ', NULL, NULL),
('Orderplatform.OrderplatformManager', 1, 'Manage orderplatform', NULL, NULL);

--
-- Вывод данных для таблицы sh_menu_menu
--
INSERT INTO sh_menu_menu VALUES
(2, 'Верхнее меню', 'head-menu', 'верхнее меню на главной', 1),
(3, 'Левое нижнее меню', 'left-bottom-menu', 'Левое нижнее меню', 1),
(4, 'Среднее нижнее меню', 'middle-bottom-menu', 'Среднее нижнее меню', 1),
(5, 'Правое нижнее меню', 'right-bottom-menu', 'Правое нижнее меню', 1);

--
-- Вывод данных для таблицы sh_mail_mail_event
--
-- Таблица help.sh_mail_mail_event не содержит данных

--
-- Вывод данных для таблицы sh_image_image
--
INSERT INTO sh_image_image VALUES
(1, NULL, NULL, 'logo.png', '', '5f65b6ed7e94f89204e0ca15b9c67077.png', '2018-03-02 14:52:59', 1, 'logo.png', 0, 1, 1),
(2, NULL, NULL, 'logo-account.png', '', '8ae757c89acd251c75445cfcb2f2b108.png', '2018-04-28 11:04:01', 1, 'logo-account.png', 0, 1, 2);

--
-- Вывод данных для таблицы sh_gallery_gallery
--
-- Таблица help.sh_gallery_gallery не содержит данных

--
-- Вывод данных для таблицы sh_blog_blog
--
-- Таблица help.sh_blog_blog не содержит данных

--
-- Вывод данных для таблицы sh_blog_tag
--
-- Таблица help.sh_blog_tag не содержит данных

--
-- Вывод данных для таблицы sh_blog_post
--
-- Таблица help.sh_blog_post не содержит данных

--
-- Вывод данных для таблицы sh_orders_platform
--
INSERT INTO sh_orders_platform VALUES
(4, '2018-05-08 11:32:55', '0000-00-00 00:00:00', '2018-05-10 00:00:00', 'Геометрические приложения определённого интеграла', NULL, 'тестовое описание', 100.00, NULL, '', 1, 1, 1, 4, NULL, NULL),
(5, '2018-05-08 11:58:43', '0000-00-00 00:00:00', '2018-05-24 00:00:00', 'Геометрические приложения определённого интеграла', NULL, 'тестовое описание', 200.00, NULL, '', 1, 1, 1, 4, NULL, NULL);

--
-- Вывод данных для таблицы sh_yupe_settings
--
INSERT INTO sh_yupe_settings VALUES
(1, 'yupe', 'siteDescription', 'Заказ работ', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(2, 'yupe', 'siteName', 'Студент.HELP', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(3, 'yupe', 'siteKeyWords', 'работы', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(4, 'yupe', 'email', 'doctordrethe@mail.ru', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(5, 'yupe', 'theme', 'student', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(6, 'yupe', 'backendTheme', '', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2018-03-02 11:43:33', '2018-03-02 11:43:33', 1, 1),
(9, 'homepage', 'mode', '2', '2018-03-02 16:21:58', '2018-03-02 16:21:58', 1, 1),
(10, 'homepage', 'target', '6', '2018-03-02 16:21:58', '2018-03-02 16:22:01', 1, 1),
(11, 'homepage', 'limit', '', '2018-03-02 16:21:58', '2018-03-02 16:21:58', 1, 1),
(12, 'user', 'avatarMaxSize', '5242880', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(13, 'user', 'avatarExtensions', 'jpg,png,gif,jpeg', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(14, 'user', 'defaultAvatarPath', 'images/avatar.png', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(15, 'user', 'avatarsDir', 'avatars', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(16, 'user', 'showCaptcha', '0', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(17, 'user', 'minCaptchaLength', '3', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(18, 'user', 'maxCaptchaLength', '6', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(19, 'user', 'minPasswordLength', '5', '2018-04-28 10:49:20', '2018-04-28 15:31:19', 1, 1),
(20, 'user', 'autoRecoveryPassword', '0', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(21, 'user', 'recoveryDisabled', '0', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(22, 'user', 'registrationDisabled', '0', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(23, 'user', 'notifyEmailFrom', 'test@test.ru', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(24, 'user', 'logoutSuccess', '/', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(25, 'user', 'loginSuccess', '/', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(26, 'user', 'accountActivationSuccess', '/user/account/login', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(27, 'user', 'accountActivationFailure', '/user/account/registration', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(28, 'user', 'loginAdminSuccess', '/yupe/backend/index', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(29, 'user', 'registrationSuccess', '/user/account/login', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(30, 'user', 'sessionLifeTime', '7', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(31, 'user', 'usersPerPage', '20', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(32, 'user', 'emailAccountVerification', '0', '2018-04-28 10:49:20', '2018-04-28 15:31:19', 1, 1),
(33, 'user', 'badLoginCount', '3', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(34, 'user', 'phoneMask', '+7-999-999-9999', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(35, 'user', 'phonePattern', '/^((\\+?7)(-?\\d{3})-?)?(\\d{3})(-?\\d{4})$/', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1),
(36, 'user', 'generateNickName', '0', '2018-04-28 10:49:20', '2018-04-28 10:49:20', 1, 1);

--
-- Вывод данных для таблицы sh_user_user_auth_item_child
--
INSERT INTO sh_user_user_auth_item_child VALUES
('Client.ClientManager', 'Client.ClientBackend.Index'),
('Orderplatform.OrderplatformManager', 'Orderplatform.OrderplatformBackend.Create'),
('Orderplatform.OrderplatformManager', 'Orderplatform.OrderplatformBackend.Index'),
('Orderplatform.OrderplatformManager', 'Orderplatform.OrderplatformBackend.List');

--
-- Вывод данных для таблицы sh_user_user_auth_assignment
--
INSERT INTO sh_user_user_auth_assignment VALUES
('admin', 1, NULL, NULL),
('Client.ClientBackend.Index', 4, NULL, NULL),
('Orderplatform.OrderplatformBackend.Create', 4, NULL, NULL),
('Orderplatform.OrderplatformBackend.Index', 3, NULL, NULL),
('Orderplatform.OrderplatformBackend.Index', 4, NULL, NULL),
('Orderplatform.OrderplatformBackend.List', 3, NULL, NULL);

--
-- Вывод данных для таблицы sh_user_tokens
--
INSERT INTO sh_user_tokens VALUES
(6, 2, '1CkS7ZduDB5BP6HAybmG9CN0PsV_OpFY', 1, 0, '2018-05-02 13:35:02', '2018-05-02 13:35:02', '127.0.0.1', '2018-05-03 13:35:02'),
(7, 2, 'ed7qobmmUH7QJZlbgxIcWuRwmeJBkW9z', 4, 0, '2018-05-02 13:35:08', '2018-05-02 13:35:08', '127.0.0.1', '2018-05-09 13:35:08'),
(9, 3, 'c5Mz7qtn8XWnd~oTw12KrxaGT0UTgY1n', 1, 0, '2018-05-02 13:46:25', '2018-05-02 13:46:25', '127.0.0.1', '2018-05-03 13:46:25'),
(10, 3, 'BVc15UpL82Rv2EipIRMyrG5vh567CuIM', 4, 0, '2018-05-02 13:46:26', '2018-05-02 13:46:26', '127.0.0.1', '2018-05-09 13:46:26'),
(12, 4, 'UdK1FmmVzsgS8Ck3XU6VkQRC1SAcYLFK', 1, 0, '2018-05-02 15:10:17', '2018-05-02 15:10:17', '127.0.0.1', '2018-05-03 15:10:17'),
(13, 4, '1Wvi6iIrwFwhN4RwGX3ZlvSZxz14mrpf', 4, 0, '2018-05-02 15:10:22', '2018-05-02 15:10:22', '127.0.0.1', '2018-05-09 15:10:22'),
(15, 5, 'm8jMQeiFJixjolctYgOPTCKqgiivljtd', 1, 0, '2018-05-02 16:10:26', '2018-05-02 16:10:26', '127.0.0.1', '2018-05-03 16:10:26'),
(16, 5, 'X0dgLwlc3MGbwHexQLy5DJ1J2dvmUo2F', 4, 0, '2018-05-02 16:10:32', '2018-05-02 16:10:32', '127.0.0.1', '2018-05-09 16:10:32'),
(22, 1, 'XyNied1gL8Ja9Hpg9fd1S_Sg8qYZ6IQD', 4, 0, '2018-05-08 11:12:21', '2018-05-08 11:12:21', '127.0.0.1', '2018-05-15 11:12:21');

--
-- Вывод данных для таблицы sh_type_pay
--
INSERT INTO sh_type_pay VALUES
(1, 'Пополнение баланса'),
(2, 'Списание с баланса'),
(3, 'Оплата за заказ'),
(4, 'Вывод денежных средств');

--
-- Вывод данных для таблицы sh_status_pay
--
INSERT INTO sh_status_pay VALUES
(1, 'В процессе'),
(2, 'Резерв'),
(3, 'Списано');

--
-- Вывод данных для таблицы sh_page_page
--
INSERT INTO sh_page_page VALUES
(1, NULL, 'ru', NULL, '2018-03-02 15:23:33', '2018-03-02 15:23:33', 1, 1, 'Цены и сроки', 'Цены и сроки', 'ceny-i-sroki', '<p>страница в стадии наполнения</p>', '', '', 1, 0, 1, '', ''),
(2, NULL, 'ru', NULL, '2018-03-02 15:23:45', '2018-03-02 15:23:45', 1, 1, 'О сервисе', 'О сервисе', 'o-servise', '<p>страница в стадии наполнения</p>', '', '', 1, 0, 2, '', ''),
(3, NULL, 'ru', NULL, '2018-03-02 15:24:00', '2018-03-02 15:24:00', 1, 1, 'Отзывы', 'Отзывы', 'otzyvy', '<p>страница в стадии наполнения</p>', '', '', 1, 0, 3, '', ''),
(4, NULL, 'ru', NULL, '2018-03-02 15:24:30', '2018-03-02 15:24:30', 1, 1, 'Контакты', 'Контакты', 'kontakty', '<p>страница в стадии наполнения</p>', '', '', 1, 0, 4, '', ''),
(5, NULL, 'ru', NULL, '2018-03-02 15:24:44', '2018-03-02 15:24:44', 1, 1, 'Способы оплаты', 'Способы оплаты', 'sposoby-oplaty', '<p>страница в стадии наполнения</p>', '', '', 1, 0, 5, '', ''),
(6, NULL, 'ru', NULL, '2018-03-02 16:21:50', '2018-03-02 16:21:50', 1, 1, 'Студент.HELP', 'Студент.HELP', 'help', '<p>страница в стадии наполнения</p>', '', '', 1, 0, 6, '', '');

--
-- Вывод данных для таблицы sh_order_log
--
-- Таблица help.sh_order_log не содержит данных

--
-- Вывод данных для таблицы sh_options_order
--
-- Таблица help.sh_options_order не содержит данных

--
-- Вывод данных для таблицы sh_notify_settings
--
-- Таблица help.sh_notify_settings не содержит данных

--
-- Вывод данных для таблицы sh_news_news
--
INSERT INTO sh_news_news VALUES
(1, 1, 'ru', '2018-03-02 15:09:28', '2018-03-02 15:12:22', '2018-03-02', 'Оформление заявки', 'oformlenie-zayavki', '', '<p>Вы оформляете заявку, чтобы узнать стоимость работы и сроки<br></p>', 'e8c3245344934a082ab38caea3c57c3a.png', '', 1, 1, 0, '', ''),
(2, 1, 'ru', '2018-03-02 15:10:05', '2018-03-02 15:10:05', '2018-03-02', 'Доступ в л/к', 'dostup-v-lk', '', '<p>Получаете доступ в л/к. Авторы оценивают вашу работу<br></p>', '30ef3a5a2bd3b2d832fb7a6c0095a05a.png', '', 1, 1, 0, '', ''),
(3, 1, 'ru', '2018-03-02 15:10:43', '2018-03-02 15:10:43', '2018-03-02', 'Предоплата', 'predoplata', '', '<p>Выбираете автора и вносите предоплату 25%</p>', 'f1d1795504889d22f786c53f16c4e188.png', '', 1, 1, 0, '', ''),
(4, 1, 'ru', '2018-03-02 15:11:11', '2018-03-02 15:11:11', '2018-03-02', 'В работе', 'v-rabote', '', '<p>Автор приступает к выполнению вашего заказа<br></p>', 'a17933defad592cf51944dc260e905fa.png', '', 1, 1, 0, '', ''),
(5, 1, 'ru', '2018-03-02 15:11:31', '2018-03-02 15:11:31', '2018-03-02', 'Уведомление', 'uvedomlenie', '', '<p>Получаете уведомление о готовности</p>', '947b3cc43e743496ce6d4de3fe1c4556.png', '', 1, 1, 0, '', ''),
(6, 1, 'ru', '2018-03-02 15:11:57', '2018-03-02 15:11:57', '2018-03-02', 'Оставшаяся сумма', 'ostavshayasya-summa', '', '<p>Вносите оставшуюся сумму<br></p>', '018653441e0e4a1fa0684de78ad654fc.png', '', 1, 1, 0, '', ''),
(7, 1, 'ru', '2018-03-02 15:12:14', '2018-03-02 15:12:14', '2018-03-02', 'Результат', 'rezultat', '', '<p>Получаете свою работу<br></p>', 'bb09d1b31ca7b2cbe8764fc31205a2cf.png', '', 1, 1, 0, '', ''),
(8, 3, 'ru', '2018-03-02 15:15:17', '2018-03-02 16:53:22', '2018-03-10', 'Качество', 'kachestvo', '', '<p>Каждая работа проходит строгий контроль качества перед выдачей клиенту</p>', '02065642df37f44a6c9b1db6fea17613.png', '', 1, 1, 0, '', ''),
(9, 3, 'ru', '2018-03-02 15:15:48', '2018-03-02 16:53:31', '2018-03-09', 'Договор', 'dogovor', '', '<p>Официальный договор и кассовый чек - гарантия защиты потребителя</p>', 'b709a356b011e5a0eed89eb798550e6a.png', '', 1, 1, 0, '', ''),
(10, 3, 'ru', '2018-03-02 15:16:19', '2018-03-02 16:53:39', '2018-03-08', 'Конфиденциальность', 'konfidencialnost', '', '<p>Мы гарантируем вам полную анонимность<br></p>', 'ea37866e34f639a5e4fa05ca29a35bc9.png', '', 1, 1, 0, '', ''),
(11, 3, 'ru', '2018-03-02 15:16:56', '2018-03-02 16:53:43', '2018-03-07', 'Своевременность', 'svoevremennost', '', '<p>Личный менеджер следит за соблюдением сроков сдачи работы<br></p>', '10a22e58a871cf6e56b2c3015e2bced9.png', '', 1, 1, 0, '', ''),
(12, 3, 'ru', '2018-03-02 15:17:36', '2018-03-02 16:53:48', '2018-03-06', 'Заказать напрямую у исполнителей', 'zakazat-napryamuyu-u-ispolniteley', '', '<p>Вы сами выбираете понравившегося вам автора и цену<br></p>', '086cf44fd6b756ee570a4ed539455508.png', '', 1, 1, 0, '', ''),
(13, 2, 'ru', '2018-03-02 16:39:10', '2018-03-02 16:39:10', '2018-03-02', 'Кристина', 'kristina', '', '<p>Идейные соображения высшего порядка, а также реализация намеченных плановых заданий играет важную роль в формировании форм развития. Не следует, однако забывать, что постоянное информационно-пропагандистское обеспечение .</p>', 'f6072000a5d812987624e82072b6978f.png', '', 1, 1, 0, '', '');

--
-- Вывод данных для таблицы sh_migrations
--
INSERT INTO sh_migrations VALUES
(1, 'user', 'm000000_000000_user_base', 1519971331),
(2, 'user', 'm131019_212911_user_tokens', 1519971331),
(3, 'user', 'm131025_152911_clean_user_table', 1519971334),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1519971336),
(5, 'user', 'm131106_111552_user_restore_fields', 1519971337),
(6, 'user', 'm131121_190850_modify_tokes_table', 1519971338),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1519971340),
(8, 'user', 'm150416_113652_rename_fields', 1519971341),
(9, 'user', 'm151006_000000_user_add_phone', 1519971342),
(10, 'yupe', 'm000000_000000_yupe_base', 1519971344),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1519971344),
(12, 'yupe', 'm150416_125517_rename_fields', 1519971345),
(13, 'yupe', 'm160204_195213_change_settings_type', 1519971345),
(14, 'category', 'm000000_000000_category_base', 1519971347),
(15, 'category', 'm150415_150436_rename_fields', 1519971347),
(16, 'image', 'm000000_000000_image_base', 1519971350),
(17, 'image', 'm150226_121100_image_order', 1519971351),
(18, 'image', 'm150416_080008_rename_fields', 1519971351),
(19, 'mail', 'm000000_000000_mail_base', 1519971354),
(20, 'comment', 'm000000_000000_comment_base', 1519971358),
(21, 'comment', 'm130704_095200_comment_nestedsets', 1519971365),
(22, 'comment', 'm150415_151804_rename_fields', 1519971366),
(23, 'notify', 'm141031_091039_add_notify_table', 1519971367),
(24, 'blog', 'm000000_000000_blog_base', 1519971383),
(25, 'blog', 'm130503_091124_BlogPostImage', 1519971384),
(26, 'blog', 'm130529_151602_add_post_category', 1519971386),
(27, 'blog', 'm140226_052326_add_community_fields', 1519971388),
(28, 'blog', 'm140714_110238_blog_post_quote_type', 1519971388),
(29, 'blog', 'm150406_094809_blog_post_quote_type', 1519971389),
(30, 'blog', 'm150414_180119_rename_date_fields', 1519971390),
(31, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1519971392),
(32, 'page', 'm000000_000000_page_base', 1519971398),
(33, 'page', 'm130115_155600_columns_rename', 1519971398),
(34, 'page', 'm140115_083618_add_layout', 1519971399),
(35, 'page', 'm140620_072543_add_view', 1519971400),
(36, 'page', 'm150312_151049_change_body_type', 1519971401),
(37, 'page', 'm150416_101038_rename_fields', 1519971401),
(38, 'callback', 'm150926_083350_callback_base', 1519971402),
(39, 'callback', 'm160621_075232_add_date_to_callback', 1519971405),
(40, 'callback', 'm161125_181730_add_url_to_callback', 1519971406),
(41, 'callback', 'm161204_122528_update_callback_encoding', 1519971406),
(42, 'contentblock', 'm000000_000000_contentblock_base', 1519971407),
(43, 'contentblock', 'm140715_130737_add_category_id', 1519971408),
(44, 'contentblock', 'm150127_130425_add_status_column', 1519971408),
(45, 'feedback', 'm000000_000000_feedback_base', 1519971411),
(46, 'feedback', 'm150415_184108_rename_fields', 1519971412),
(47, 'gallery', 'm000000_000000_gallery_base', 1519971414),
(48, 'gallery', 'm130427_120500_gallery_creation_user', 1519971415),
(49, 'gallery', 'm150416_074146_rename_fields', 1519971416),
(50, 'gallery', 'm160514_131314_add_preview_to_gallery', 1519971417),
(51, 'gallery', 'm160515_123559_add_category_to_gallery', 1519971418),
(52, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1519971418),
(53, 'menu', 'm000000_000000_menu_base', 1519971424),
(54, 'menu', 'm121220_001126_menu_test_data', 1519971424),
(55, 'menu', 'm160914_134555_fix_menu_item_default_values', 1519971430),
(56, 'news', 'm000000_000000_news_base', 1519971433),
(57, 'news', 'm150416_081251_rename_fields', 1519971434),
(58, 'rbac', 'm140115_131455_auth_item', 1524894511),
(59, 'rbac', 'm140115_132045_auth_item_child', 1524894513),
(60, 'rbac', 'm140115_132319_auth_item_assign', 1524894515),
(61, 'rbac', 'm140702_230000_initial_role_data', 1524894515);

--
-- Вывод данных для таблицы sh_menu_menu_item
--
INSERT INTO sh_menu_menu_item VALUES
(12, 0, 2, 1, 'Цены и сроки', '/ceny-i-sroki', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 1, 1),
(13, 0, 2, 1, 'О сервисе', '/o-servise', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 2, 1),
(14, 0, 2, 1, 'Отзывы', '/otzyvy', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 3, 1),
(15, 0, 2, 1, 'Контакты', '/kontakty', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 4, 1),
(16, 0, 2, 1, 'Способы оплаты', '/sposoby-oplaty', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 5, 1),
(17, 0, 3, 1, 'Цены и сроки', '#', '', '', '', '', '', '', '', 0, 6, 1),
(18, 0, 3, 1, 'Контрольные работы', '#', '', '', '', '', '', '', '', 0, 7, 1),
(19, 0, 3, 1, 'Дипломные работы', '#', '', '', '', '', '', '', '', 0, 8, 1),
(20, 0, 3, 1, 'Курсовые работы', '#', '', '', '', '', '', '', '', 0, 9, 1),
(21, 0, 3, 1, 'Рефераты', '#', '', '', '', '', '', '', '', 0, 10, 1),
(22, 0, 3, 1, 'Отчеты по практике', '#', '', '', '', '', '', '', '', 0, 11, 1),
(23, 0, 3, 1, 'Эссе', '#', '', '', '', '', '', '', '', 0, 12, 1),
(24, 0, 3, 1, 'Все услуги', '#', '', '', '', '', '', '', '', 0, 13, 1),
(25, 0, 4, 1, 'Клиентам', '#', '', '', '', '', '', '', '', 0, 14, 1),
(26, 0, 4, 1, 'Способы оплаты', '#', '', '', '', '', '', '', '', 0, 15, 1),
(27, 0, 4, 1, 'Вопросы и ответы', '#', '', '', '', '', '', '', '', 0, 16, 1),
(28, 0, 4, 1, 'Публичная оферта', '#', '', '', '', '', '', '', '', 0, 17, 1),
(29, 0, 4, 1, 'Политика конфиденциальности', '#', '', '', '', '', '', '', '', 0, 18, 1),
(30, 0, 5, 1, 'О компании', '#', '', '', '', '', '', '', '', 0, 19, 1),
(31, 0, 5, 1, 'Отзывы', '#', '', '', '', '', '', '', '', 0, 20, 1),
(32, 0, 5, 1, 'Новости', '#', '', '', '', '', '', '', '', 0, 21, 1),
(33, 0, 5, 1, 'Партнерам', '#', '', '', '', '', '', '', '', 0, 22, 1),
(34, 0, 5, 1, 'Партнерам', '#', '', '', '', '', '', '', '', 0, 23, 1),
(35, 0, 5, 1, 'Авторам', '#', '', '', '', '', '', '', '', 0, 24, 1),
(36, 0, 5, 1, 'Контакты', '#', '', '', '', '', '', '', '', 0, 25, 1),
(37, 0, 5, 1, 'Лента работ', '#', '', '', '', '', '', '', '', 0, 26, 1);

--
-- Вывод данных для таблицы sh_mail_mail_template
--
-- Таблица help.sh_mail_mail_template не содержит данных

--
-- Вывод данных для таблицы sh_gallery_image_to_gallery
--
-- Таблица help.sh_gallery_image_to_gallery не содержит данных

--
-- Вывод данных для таблицы sh_feedback_feedback
--
-- Таблица help.sh_feedback_feedback не содержит данных

--
-- Вывод данных для таблицы sh_contentblock_content_block
--
INSERT INTO sh_contentblock_content_block VALUES
(1, 'logo', 'logo', 3, '<a href="/"><img src="/uploads/image/5f65b6ed7e94f89204e0ca15b9c67077.png"></a>', '', NULL, 1),
(2, 'link-top', 'link-top', 3, '<a href="#">Студентам</a> / <a href="#">Авторам</a>', '', NULL, 1),
(3, 'phone', 'phone', 3, '<p><i class="fa fa-phone"></i> +7 (987) 797-77-97\r\n</p>', '', NULL, 1),
(4, 'head-text', 'head-text', 3, '<h3>Онлайн - эксперты <br /> помогут с любой<br /> задачей</h3>', '', NULL, 1),
(5, 'order-title', 'order-title', 3, '<h3>У нас вы можете заказать</h3>', '', NULL, 1),
(6, 'how-works', 'how-works', 3, '<h3>Как это работает</h3>', '', NULL, 1),
(7, 'author-title', 'author-title', 3, '<h3>Вашим заказом займутся</h3>', '', NULL, 1),
(8, 'comments', 'comments', 3, '<h3>Отзывы студентов</h3>', '', NULL, 1),
(9, 'pluses', 'pluses', 3, '<h3>Наши преимущества</h3>', '', NULL, 1),
(10, 'copyright', 'copyright', 3, '<p>«Студент.HELP»</p>', '', NULL, 1),
(11, 'logo-account', 'logo-account', 3, '<a href="/"><img src="/uploads/image/8ae757c89acd251c75445cfcb2f2b108.png"></a>', '', NULL, 1);

--
-- Вывод данных для таблицы sh_comment_comment
--
-- Таблица help.sh_comment_comment не содержит данных

--
-- Вывод данных для таблицы sh_callback
--
-- Таблица help.sh_callback не содержит данных

--
-- Вывод данных для таблицы sh_blog_user_to_blog
--
-- Таблица help.sh_blog_user_to_blog не содержит данных

--
-- Вывод данных для таблицы sh_blog_post_to_tag
--
-- Таблица help.sh_blog_post_to_tag не содержит данных

--
-- Вывод данных для таблицы sh_balance_user
--
-- Таблица help.sh_balance_user не содержит данных

--
-- Восстановить предыдущий режим SQL (SQL mode)
--
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

--
-- Включение внешних ключей
--
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;