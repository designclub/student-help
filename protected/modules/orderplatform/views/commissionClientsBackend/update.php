<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Комиссии для клиентов') => ['/orderplatform/commissionClients/index'],
    $model->id => ['/orderplatform/commissionClients/view', 'id' => $model->id],
    Yii::t('OrderplatformModule.orderplatform', 'Редактирование'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Комиссии для клиентов - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Комиссиями для клиентов'), 'url' => ['/orderplatform/commissionClients/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Комиссию для клиентов'), 'url' => ['/orderplatform/commissionClients/create']],
    ['label' => Yii::t('OrderplatformModule.orderplatform', 'Комиссия для клиентов') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Редактирование Комиссии для клиентов'), 'url' => [
        '/orderplatform/commissionClients/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Просмотреть Комиссию для клиентов'), 'url' => [
        '/orderplatform/commissionClients/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Удалить Комиссию для клиентов'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/orderplatform/commissionClients/delete', 'id' => $model->id],
        'confirm' => Yii::t('OrderplatformModule.orderplatform', 'Вы уверены, что хотите удалить Комиссию для клиентов?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Редактирование') . ' ' . Yii::t('OrderplatformModule.orderplatform', 'Комиссии для клиентов'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>