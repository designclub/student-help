<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

	<link href="https://fonts.googleapis.com/css?family=Arimo:400,700|Ubuntu:300,400,500,700&amp;subset=cyrillic-ext" rel="stylesheet">

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php endif; ?>

    <?php

		Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.css');
		Yii::app()->getClientScript()->registerCssFile($this->mainAssets.'/css/jquery.Jcrop.min.css');

		Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.li-translit.js');
		Yii::app()->getClientScript()->registerScriptFile($this->mainAssets.'/js/file-api/FileAPI/FileAPI.min.js');
		Yii::app()->getClientScript()->registerScriptFile($this->mainAssets.'/js/file-api/jquery.fileapi.min.js');
		Yii::app()->getClientScript()->registerScriptFile($this->mainAssets.'/js/file-api/jquery.Jcrop.min.js');
    ?>

    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>


    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>
 <div class="container-site">
 	<div class="header-account">
 		<div class="content-site">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-xs-12 logo">
					<?php if (Yii::app()->hasModule('contentblock')): ?>
						<?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ['code'=>'logo-account']); ?>
					<?php endif; ?>
				</div>
				<div class="col-lg-3 col-md-3 col-xs-12">
					<div class="link-header">
						<?= CHtml::link('Как это работает', '#', ['class' => 'play']); ?>
					</div>
				</div>			
				<div class="col-lg-3 col-md-3 col-xs-12">
					<div class="link-header">
						<?= CHtml::link('Личный кабинет', Yii::app()->createUrl('/yupe/backend/index')); ?>
					</div>
				</div>
			</div>
		</div>
 	</div><!-- end header -->

 	<div class="content-page">
		<div class="content-account">
			<?= $content; ?>
		</div>
	</div>
	
 	<?php $this->renderPartial('//layouts/_footer'); ?>

 </div><!-- end container-site -->

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

</body>
</html>