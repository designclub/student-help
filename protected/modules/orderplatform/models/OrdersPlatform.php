<?php

/**
 * This is the model class for table "{{orders_platform}}".
 *
 * The followings are the available columns in table '{{orders_platform}}':
 * @property integer $id
 * @property string $create_date
 * @property string $update_date
 * @property string $target_date
 * @property string $title
 * @property string $theme
 * @property string $text
 * @property string $budget
 * @property string $cost
 * @property string $file
 * @property integer $type_id
 * @property integer $subject_id
 * @property integer $status_id
 * @property integer $client_id
 * @property integer $author_id
 * @property integer $dialog_id
 * @property integer $count_page_from
 * @property integer $count_page_to
 *
 * The followings are the available model relations:
 * @property UserUser $author
 * @property UserUser $client
 * @property DialogOrderUsers $dialog
 * @property StatusOrder $status
 * @property SubjectOrder $subject
 * @property TypesOrder $type
 */
class OrdersPlatform extends yupe\models\YModel
{
	public $files = [];
	
	public $fileInstances = [];
	
	public $idUser = null;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{orders_platform}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
            ['title, text, type_id, subject_id, target_date, uniqueness, uniq_check_id', 'required'],
            ['type_id, subject_id, status_id, client_id, author_id, dialog_id, uniqueness, uniq_check_id', 'numerical', 'integerOnly'=>true],
            ['title, theme, file', 'length', 'max'=>500],
            ['budget, cost', 'length', 'max'=>10],
            ['uniqueness', 'numerical', 'max'=>100, 'min'=>0],
            ['count_page_from', 'numerical', 'min' => 1],
            ['count_page_to', 'numerical', 'min' => 1],
            ['create_date, update_date', 'safe'],
            ['files', 'fileValidator'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_date, update_date, target_date, title, theme, text, budget, cost, file, type_id, subject_id, status_id, client_id, author_id, dialog_id, uniqueness, uniq_check_id', 'safe', 'on'=>'search'],
        ];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
			'author'          => [self::BELONGS_TO, 'User', 'author_id'],
			'client'          => [self::BELONGS_TO, 'User', 'client_id'],
			'dialog'          => [self::HAS_MANY, 'DialogOrderUsers', 'order_id'],
			'status'          => [self::BELONGS_TO, 'StatusOrder', 'status_id'],
			'subject'         => [self::BELONGS_TO, 'SubjectOrder', 'subject_id'],
			'type'            => [self::BELONGS_TO, 'TypesOrder', 'type_id'],
			'uniqCheck'       => [self::BELONGS_TO, 'UniqueCheck', 'uniq_check_id'],
			'rates'           => [self::HAS_MANY, 'RatesOrder', 'order_id'],
			'solutionsOrders' => [self::HAS_MANY, 'SolutionsOrder', 'order_id'],
			'correctionsOrder' => [self::HAS_MANY, 'OrderCorrection', 'order_id', 'condition' =>'status = 0'],
			'filesOrders'     => [self::HAS_MANY, 'FilesOrder', 'order_id'],
			'filesForOrders'     => [self::HAS_MANY, 'OrdersPlatformFile', 'order_id'],
			'orderLogs'       => [self::HAS_MANY, 'OrderLog', 'order_id'],
		];
	}

	//количество диалогов
	public function getCountDialog()
	{
		return count($this->dialog([
			'condition' => "user_to=:id",
			'params' => [
				':id'     => Yii::app()->user->id,
			]
		]));
	}

	public function fileValidator($attribute, $value){
		$module = Yii::app()->getModule('orderplatform');
		$files = CUploadedFile::getInstances($this, $attribute);

		$count = 0;

		if (!$this->isNewRecord) {
			$count = OrdersPlatformFile::model()->count('order_id = :id', [':id' => $this->id]);
		}

		if ($count + count($files) > 5) {
			$this->addError('files', 'Максимальное кол-во файлов для загрузки 5');
			return false;
		}

		$this->files = $files;

		return true;
	}

	public function beforeSave()
	{
		if(empty($this->budget)){
			$this->budget = 0;
		}
		
		if (is_null( $this->idUser)){
			$this->idUser = Yii::app()->getUser()->getId();
		}
		
		$user = $this->loadClient($this->idUser);
 	
        Yii::app()->db->createCommand('SET time_zone = "'.$user->timeZone->name_zone.'"')->execute();
		
		if ($this->isNewRecord){
			$this->client_id   = Yii::app()->getUser()->getId();
			$this->create_date = new CDbExpression('NOW()');
			$this->target_date = date('Y-m-d H:i:s', strtotime($this->target_date . ' 23:59:59'));
			$this->status_id   = StatusOrder::STATUS_AUCTION; //отправляем заказ сразу в аукцион
		}
		else{
			$this->update_date = new CDbExpression('NOW()');
		}

		return parent::beforeSave();
	}

	public function afterSave(){

		$module = Yii::app()->getModule('orderplatform');

		foreach ($this->files as $key => $file){
			$fileName = uniqid().'.'.$file->getExtensionName();
			$fileModel = new OrdersPlatformFile;
			$path = $module->getUploadPath().'/'.$fileName;
			if ($file->saveAs($path)) {
				$fileModel->order_id = $this->id;
				$fileModel->fileName = $fileName;
				$fileModel->name = $file->getName();
				$fileModel->save();
			}
		}
		return parent::afterSave();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id'            => 'ID',
			'create_date'   => 'Дата создания заказа',
			'update_date'   => 'Дата изменения заказа',
			'target_date'   => 'Срок сдачи до',
			'title'         => 'Наименование работы',
			'theme'         => 'Тема заказа',
			'text'          => 'Описание заказа',
			'budget'        => 'Планируемый бюджет',
			'cost'          => 'Конечная стоимость',
			'file'          => 'Файл',
			'files[]'          => 'Загрузка файлов',
			'type_id'       => 'Тип работы',
			'subject_id'    => 'Предмет работы',
			'status_id'     => 'Статус заказа',
			'client_id'     => 'Клиент',
			'author_id'     => 'Автор',
			'dialog_id'     => 'Диалог',
			'uniqueness'    => 'Уникальность',
			'uniq_check_id' => 'Программа проверки',
			'count_page_to' => 'Кол-во страниц (до)',
			'count_page_from' => 'Кол-во страниц (от)',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('target_date',$this->target_date,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('dialog_id',$this->dialog_id);
		$criteria->compare('uniqueness',$this->uniqueness);
		$criteria->compare('count_page_to',$this->count_page_to);
		$criteria->compare('count_page_from',$this->count_page_from);
		$criteria->compare('uniq_check_id',$this->uniq_check_id);

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	//заказы клиентов или авторов
	public function ListOrdersAll($user)
	{
		if($user->type_account==1){
			$user = 'client_id';
		}
		elseif($user->type_account==2){
			$user = 'author_id';
		}
		else{
			$user = 'author_id';
		}
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->condition = $user. ' = :idUser';
		$criteria->params = [':idUser' => Yii::app()->getUser()->getId()];
		$criteria->order = 'create_date DESC';

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	//клиентские заказы
	public function ListOrdersClients()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->condition = 'client_id = :client_id AND status_id <= :status AND status_id <> :delete';
		$criteria->params = [':client_id' => Yii::app()->getUser()->getId(), ':status' => StatusOrder::STATUS_FINISH_AUTO, ':delete' => StatusOrder::STATUS_DELETE];
		$criteria->order = 'create_date DESC';

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	//авторские заказы
	public function ListOrdersAuthor(){

		$criteria=new CDbCriteria;
		$criteria->with = ['dialog'];
		$criteria->together = true;
		$criteria->condition = 't.author_id = :author_id OR dialog.user_to = :author_id';
		$criteria->params = [':author_id' => Yii::app()->getUser()->getId()];
		$criteria->group = 't.id';
		$criteria->order = 't.create_date DESC';

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	//заказы в аукционе
	public function Auction(){
		
		$criteria=new CDbCriteria;
		$criteria->condition = 'status_id = :status_id';
		$criteria->params = [':status_id' => StatusOrder::STATUS_AUCTION];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}
	
	//клиентские заказы на аукционе
	public function listOrdersAuction(){
		
		$criteria=new CDbCriteria;
		$criteria->condition = 'status_id = :status_id AND client_id = :client';
		$criteria->params = [':client' => Yii::app()->getUser()->getId(), ':status_id' => StatusOrder::STATUS_AUCTION];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}
	
	//клиентские заказы на доработке
	public function listOrdersCorrection(){
		
		$criteria=new CDbCriteria;
		$criteria->condition = 'status_id = :status_id AND (client_id = :user OR author_id = :user)';
		$criteria->params = [':user' => Yii::app()->getUser()->getId(), ':status_id' => StatusOrder::STATUS_CORRECTION];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}
	
	//клиентские заказы отправленные автору
	public function listOrdersForSendAuthors(){

		$criteria=new CDbCriteria;
		$criteria->condition = 'status_id = :status_id AND (client_id = :client OR author_id = :author)';
		$criteria->params = [':client' => Yii::app()->getUser()->getId(), ':author' => Yii::app()->getUser()->getId(), ':status_id' => StatusOrder::STATUS_AUTHOR];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}	
	
	//заказы отправленные на проверку
	public function listOrdersCheck(){

		$criteria=new CDbCriteria;
		$criteria->condition = 'status_id = :status_id AND (client_id = :client OR author_id = :author)';
		$criteria->params = [':client' => Yii::app()->getUser()->getId(), ':author' => Yii::app()->getUser()->getId(), ':status_id' => StatusOrder::STATUS_CHECK];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}	
	
	//клиентские заказы в работе у автора
	public function listOrdersForAuthors(){

		$criteria=new CDbCriteria;
		$criteria->condition = 'status_id = :status_id AND (client_id = :client OR author_id = :author)';
		$criteria->params = [':client' => Yii::app()->getUser()->getId(), ':author' => Yii::app()->getUser()->getId(), ':status_id' => StatusOrder::STATUS_WORK];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}	
	
	//заказы в архиве (удаленные, завершенные)
	public function listOrdersArchive(){

		$criteria=new CDbCriteria;
		$criteria->condition = '(status_id = :delete OR status_id = :archive OR status_id = :finish) AND (client_id = :client OR author_id = :author)';
		$criteria->params = [':client' => Yii::app()->getUser()->getId(), ':author' => Yii::app()->getUser()->getId(), ':delete' => StatusOrder::STATUS_DELETE, ':archive' => StatusOrder::STATUS_ARCHIVE, ':finish' => StatusOrder::STATUS_FINISH];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	public function listOrdersClient()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('target_date',$this->target_date,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('author_id',$this->author_id);
		$criteria->compare('dialog_id',$this->dialog_id);
        $criteria->compare('uniqueness',$this->uniqueness);
        $criteria->compare('uniq_check_id',$this->uniq_check_id);
		$criteria->compare('count_page_to',$this->count_page_to);
		$criteria->compare('count_page_from',$this->count_page_from);
		
		$criteria->condition = 'client_id = :id_client';
		$criteria->params = [':id_client' => Yii::app()->getUser()->getId()];

		return new CActiveDataProvider($this, [
			'criteria'=>$criteria,
		]);
	}

	//размещение заказа на биржу
	public function toAuction(){
		$this->status_id = 2;
		$this->save();
	}
	
	//утверждение ставки автора по заказу
	public function confirmRate($rate){
		
		$rate = RatesOrder::model()->findByPk($rate);
		
		$order = $this->loadOrder($rate->order_id);
		$order->author_id = $rate->author_id;
		$order->status_id = StatusOrder::STATUS_AUTHOR;
		$order->cost = $rate->rate;
		
		if($order->cost > UserBalance::model()->getBalance(Yii::app()->getUser()->getId()))
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Недостаточно баланса.' ) );
		
		if($order->save()){
			UserBalance::model()->reserveBalanceOrder($order->id, $order->cost + $order->cost * (UserBalance::model()->getCommission()/100));
/*			Yii::app()->eventManager->fire(
				$event,
				new ScheduleEvent($lesson, $user)
			);*/
			OrderLog::model()->addEvent(OrderLog::ORDER_TO_AUTHOR, $order);
			return(true);
		}
		else
			return(false);
	}		
	
	//подтверждение автором заказа
	public function deleteOrder($id){
		
		$order = $this->loadOrder($id);
		$order->status_id = StatusOrder::STATUS_DELETE; 
		
		if($order->save()){
			return(true);
		}
		else
			return(false);
	}		
	
	//подтверждение автором заказа
	public function confirmOrder($id){
		
		$order = $this->loadOrder($id);
		$order->status_id = StatusOrder::STATUS_WORK; 
		
		if($order->save()){
/*			Yii::app()->eventManager->fire(
				$event,
				new ScheduleEvent($lesson, $user)
			);*/
			return(true);
		}
		else
			return(false);
	}
	
	/*
	* успешное подтверждение клиентом  заказа
	* переводим деньги автору
	* утверждаем баланс клиента
	* 
	*/
	public function acceptOrder($id){
		$order = $this->loadOrder($id);
		$order->status_id = StatusOrder::STATUS_FINISH;
		
		if($order->save()){
			UserBalance::model()->transferMoneyAuthor($order);
			UserBalance::model()->acceptMoneyClient($order->client_id, $id);
		}
		else
			return(false);
	}
	/*
	* отказ автора от заказа
	* удаляем назначение автора под этот заказ
	* размещаем заново заказ на аукционе
	* удаляем все ставки отказавшегося автора
	*/
	public function cancelOrder($id){
		
		$order = $this->loadOrder($id);
		
		$rates = RatesOrder::model()->deleteAllByAttributes(['author_id'=>$order->author_id, 'order_id'=>$order->id]);
		
		$order->author_id = null;
		$order->cost = null;
		$order->status_id = StatusOrder::STATUS_AUCTION;
		
		if($order->save()){
/*			Yii::app()->eventManager->fire(
				$event,
				new ScheduleEvent($lesson, $user)
			);*/
			return(true);
		}
		else
			return(false);
	}
	
	/*
	* поиск "просроченных" заказов
	* присвоенеие им статуса "просрочен"
	*/
	public function OrdersToDelay(){
		
		$criteria=new CDbCriteria;
		$criteria->condition = '(status_id = :work OR status_id = :auction) AND target_date < NOW()';
		$criteria->params = [':work' => StatusOrder::STATUS_WORK, ':auction' => StatusOrder::STATUS_AUCTION];
		
		$orders = self::model()->findAll($criteria);
		
		if (count($orders) > 0){
			foreach($orders as $order){

				if(is_null($order->author_id)){
					$order->status_id = StatusOrder::STATUS_DELAY;
				}
				else{
					$order->status_id = StatusOrder::STATUS_DELETE;
				}
				
				$rates = RatesOrder::model()->deleteAllByAttributes(['order_id'=>$order->id]);
				$order->author_id = null;
				$order->cost = null;
				$order->save();
			}
		}

	}
	
	public function solutions($id_order, $id_author){
		
		$criteria=new CDbCriteria;
		$criteria->with = ['order'];
		$criteria->together = true;
		$criteria->condition = 'order.status_id = :status AND t.author_id = :author AND t.order_id = :order';
		$criteria->params = [':status' => StatusOrder::STATUS_CHECK, ':author' => $id_author, ':order' => $id_order];
		$criteria->order = 'date_finish DESC';
 
		$solutions = SolutionsOrder::model()->findAll($criteria);

		return($solutions);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdersPlatform the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function loadOrder( $id ) {
		$model = self::model()->findByPk( $id );
		if ( $model === null )
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.' ) );

		return $model;
	}
	
	private function loadClient( $id ) {
		$model = User::model()->findByPk( $id );
		if ( $model === null )
			throw new CHttpException( 404, Yii::t( 'OrderplatformModule.orderplatform', 'Запрошенная страница не найдена.' ) );

		return $model;
	}

	public function getUnreadMessages($userId)
	{
		$data = $this->dialog([
			'condition' => 'is_read=:is_read and user_from=:user_from',
			'params' => [
				':is_read'   => 0,
				':user_from' => $userId,
			]
		]);

		return count($data);
	}
}
