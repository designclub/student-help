<?php

class m180614_122724_table_rates_order extends yupe\components\DbMigration
{
	public function safeUp()
	{
		//таблица ставок
        $this->createTable('{{rates_order}}', [
            "id" => "pk",
            "date_rate" => "timestamp NOT NULL",
            "order_id" => "INTEGER(11) NOT NULL",
            "author_id" => "INTEGER(11) NOT NULL",
            "rate" => "DECIMAL(10,2) NOT NULL",
        ], $this->getOptions());
	}

	public function safeDown()
	{
		$this->dropTable('{{rates_order}}');
	}
}