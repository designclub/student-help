<?php

/**
 * Class TinkoffMerchantAPI
 *
 * @author Shuyskiy Sergey s.shuyskiy@tinkoff.ru
 * @version 1.00
 * @property bool orderId
 * @property bool Count
 */
class TinkoffPayment extends CApplicationComponent
{

    private $terminalKey;
    private $secretKey;
	public   $api_url;
	private $_params = [];
	
	public $language = "ru";
 	private $paymentId;
    private $status;
    private $error;
    private $response;
    private $paymentUrl;
 
    public function init(){
		$this->_params['TerminalKey'] = $this->terminalKey;
        parent::init();
    }
	
	public function setTerminalKey($terminalKey)
    {
        $this->terminalKey = $terminalKey;
    }

    public function setSecretKey($secretKey)
    {
		$this->secretKey = $secretKey;
    }

    protected function _setOptions()
    {
        $this->_params['api_url'] = Yii::app()->createAbsoluteUrl($this->api_url);
        $this->_params['language'] = $this->language;
    }
	
	protected function convertParams()
    {
        return json_encode($this->_params);
    }
	
	protected function connect($url)
    {

		if ($curl = curl_init()) {
			curl_setopt($curl, CURLOPT_URL, $url);

            // curl_setopt($curl, CURLOPT_PROXY, $proxy);
            // curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyAuth);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $this->convertParams());
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            ));

            $out = curl_exec($curl);
            $this->response = $out;
            $json = json_decode($out);

            if ($json) {
                if (@$json->ErrorCode !== "0") {
                    $this->error = @$json->Details;
                } else {
                    $this->paymentUrl = @$json->PaymentURL;
                    $this->paymentId = @$json->PaymentId;
                    $this->status = @$json->Status;
                }
            }

            curl_close($curl);

            return $out;
		        } else {
            throw new HttpException('Can not create connection to ' . $api_url . ' with args ' . $args, 404);
        }
    }
	
	
	
	public function InitDo($orderNumber, $amount, $data)
    {
        $this->_setOptions();
        $this->_params['OrderId'] = $orderNumber;
        $this->_params['Amount'] = $amount;
        $this->_params['DATA'] = ['fio'=>$data->nick_name, 'email'=>$data->email];

        $result = $this->connect($this->api_url.'Init');

        try{
            $res=json_decode($result);
            return $res;
        }
        catch(\Exception $e){
            print_r($result);
        }
    }
}