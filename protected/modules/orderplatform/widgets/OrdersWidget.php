<?php

Yii::import('application.modules.orderplatform.models.*');

/**
 * Class DialogWidget
 */
class OrdersWidget extends \yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'orders';

    /**
     * @throws CException
     */
    public function init()
    {

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $module = Yii::app()->getModule('orderplatform');
		$orders = new OrdersPlatform;
		
		$criteria=new CDbCriteria;
		if (Yii::app()->user->getProfile()->type_account == 1){
			$orders = OrdersPlatform::model()->listOrdersClients();
		}
		elseif(Yii::app()->user->getProfile()->type_account == 2){
			$orders = OrdersPlatform::model()->listOrdersAuthor();
			$this->view = 'author';
		}

        $this->render($this->view, ['orders' => $orders]);
    }
}