<?php
/**
 * OrderplatformModule основной класс модуля orderplatform
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2018 amyLabs && Yupe! team
 * @package yupe.modules.orderplatform
 * @since 0.1
 */

class OrderplatformModule  extends yupe\components\WebModule
{
    const VERSION = '0.9.8';

    public $countDaysAuction = 1;

    public $minSumAuthor = 100;
	
    public $countDaysBalanceTransferAuthor = 3;

    public $folderUpload = 'orderplatform';
    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return parent::getDependencies();
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array или false
     */
    public function checkSelf()
    {
        return parent::checkSelf();
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('OrderplatformModule.orderplatform', 'Площадка заказов');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels(){

        return[
            'countDaysAuction' => 'Кол-во дней суток на аукционе',
            'minSumAuthor' => 'Минимальная сумма вывода для автора (руб.)',
            'countDaysBalanceTransferAuthor' => 'Кол-во дней на обработку вывода средств',
        ];
        return parent::getParamsLabels();
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {

        return[
            'countDaysAuction',
            'minSumAuthor',
            'countDaysBalanceTransferAuthor',
        ];

        return parent::getEditableParams();
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return parent::getEditableParamsGroups();
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('OrderplatformModule.orderplatform', self::VERSION);
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('OrderplatformModule.orderplatform', 'http://yupe.ru');
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('OrderplatformModule.orderplatform', 'Площадка заказов');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('OrderplatformModule.orderplatform', 'Описание модуля "Площадка заказов"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('OrderplatformModule.orderplatform', 'yupe team');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('OrderplatformModule.orderplatform', 'team@yupe.ru');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/orderplatform/orderplatformBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-orderplatform";
    }

    /**
      * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
      *
      * @return bool
      **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'orderplatform.models.*',
                'orderplatform.components.*',
            ]
        );
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Subject Order'),
                'url' => ['/orderplatform/subjectOrderBackend/index'],
            ],
            [
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Types Order'),
                'url' => ['/orderplatform/typesOrderBackend/index'],
            ],
            [
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Unique check'),
                'url' => ['/orderplatform/uniqueCheckBackend/index'],
            ],
            [
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Add commission for author'),
                'url' => ['/orderplatform/commissionAuthorBackend/create'],
            ],
            [
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Commissions for clients'),
                'url' => ['/orderplatform/CommissionClientsBackend/index'],
            ],
            [
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Add commission for client'),
                'url' => ['/orderplatform/CommissionClientsBackend/create'],
            ],            
			[
                'label' => Yii::t('OrderplatformModule.orderplatform', 'Claims'),
                'url' => ['/orderplatform/AdminBackend/index'],
            ],         
			[
                'label' => Yii::t('OrderplatformModule.orderplatform', 'AuthorMoney'),
                'url' => ['/orderplatform/AdminBackend/authorToMoney'],
            ],
        ];
    }
	
    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Orderplatform.OrderplatformManager',
                'description' => Yii::t('OrderplatformModule.orderplatform', 'Manage orderplatform'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.Auction',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Аукцион заказов')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.Index',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Мои заказы')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.List',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Список заказов')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.Rely',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Сделать ставку')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.RelyAccept',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Принять ставку')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.ConfirmOrder',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Взять в работу')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.AcceptOrder',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Принять заказ')
                    ],	                    
					[
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.CorrectionOrder',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Отправить на доработку')
                    ],						
					[
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.CancelOrder',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Отказаться')
                    ],							
					[
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.OrderplatformBackend.MakeOrder',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Выполнить заказ')
                    ],					
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.Create',
                        'description' => 'Создать заказ',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
                    [
                        'name'        => 'Orderplatform.OrderplatformBackend.Update',
                        'description' => 'Редактировать заказ',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
                    [
                        'name'        => 'Orderplatform.OrderplatformBackend.Delete',
                        'description' => 'Удалить заказ',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
                    [
                        'name'        => 'Orderplatform.OrderplatformBackend.ToAuction',
                        'description' => 'Разместить заказ на аукционе',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],					
			
			
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.AddBalance',
                        'description' => 'Пополнение баланса',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.History',
                        'description' => 'История баланса',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],					
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.Statistics',
                        'description' => 'Статистика',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.SuccessPayment',
                        'description' => 'Успешное пополнение баланса',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.FailPayment',
                        'description' => 'Ошибка пополнения баланса',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.GetMoney',
                        'description' => 'Вывод денег',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.Transfer',
                        'description' => 'Перевод денег',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.ReturnBalance',
                        'description' => 'Вывод денег',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
										
					[
                        'name'        => 'Orderplatform.OrderplatformBackend.Claim',
                        'description' => 'Направить претензию по заказу',
                        'type'        => AuthItem::TYPE_OPERATION,
                    ],
					
					[
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.DialogBackendController.DialogOrder',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Сообщения по заказу')
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Orderplatform.DialogBackendController.Send',
                        'description' => Yii::t('OrderplatformModule.orderplatform', 'Отправить сообщение в чат')
                    ],
                ]
            ]
        ];
    }

    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot.uploads').'/'.$this->folderUpload;
    }
}
