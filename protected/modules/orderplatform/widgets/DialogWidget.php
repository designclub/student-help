<?php

Yii::import('application.modules.orderplatform.models.*');

/**
 * Class DialogWidget
 */
class DialogWidget extends \yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'dialogAuthor';

    /**
     * @throws CException
     */
    public function init()
    {

        parent::init();
    }

    /**
     * @throws CException
     */
    public function run()
    {
        $module = Yii::app()->getModule('orderplatform');
		$dialog = new DialogOrderUsers;

        $this->render($this->view, ['dialog' => $dialog]);
    }
}