<?php Yii::import('application.modules.news.NewsModule'); ?>

<?php if (isset($models) && $models != []): ?>
	<?php foreach ($models as $model): ?>
		<div class="item-block">
			<div class="inner-block">
				<div class="img-block">
					<?= CHtml::image($model->getImageUrl(100, 100)); ?>
				</div>
				<div class="txt-block">
					<?= $model->full_text; ?>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?> 