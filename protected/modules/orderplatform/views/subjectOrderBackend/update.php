<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderplatformModule.orderplatform', 'Предметы работ') => ['/orderplatform/subjectOrderBackend/index'],
    $model->name_subject => ['/orderplatform/subjectOrderBackend/view', 'id' => $model->id],
    Yii::t('OrderplatformModule.orderplatform', 'Редактирование'),
];

$this->pageTitle = Yii::t('OrderplatformModule.orderplatform', 'Предметы работ - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Управление Предметами работ'), 'url' => ['/orderplatform/subjectOrderBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Добавить Предмет работы'), 'url' => ['/orderplatform/subjectOrderBackend/create']],
    ['label' => Yii::t('OrderplatformModule.orderplatform', 'Предмет работы') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Редактирование Предмета работы'), 'url' => [
        '/orderplatform/subjectOrderBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Просмотреть Предмет работы'), 'url' => [
        '/orderplatform/subjectOrderBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('OrderplatformModule.orderplatform', 'Удалить Предмет работы'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/orderplatform/subjectOrderBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('OrderplatformModule.orderplatform', 'Вы уверены, что хотите удалить Предмет работы?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OrderplatformModule.orderplatform', 'Редактирование') . ' ' . Yii::t('OrderplatformModule.orderplatform', 'Предмета работы'); ?>        <br/>
        <small>&laquo;<?=  $model->name_subject; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>