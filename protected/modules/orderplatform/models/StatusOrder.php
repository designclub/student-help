<?php

/**
 * This is the model class for table "{{status_order}}".
 *
 * The followings are the available columns in table '{{status_order}}':
 * @property integer $id
 * @property string $name_status
 *
 * The followings are the available model relations:
 * @property OrdersPlatform[] $ordersPlatforms
 */
class StatusOrder extends yupe\models\YModel
{
	const STATUS_CREATE = 1; //создан
	const STATUS_EDIT = 2;	//редактирование
	const STATUS_DELETE = 3; //удален
	const STATUS_ARCHIVE = 4; //в архиве
	const STATUS_AUCTION = 5; //в аукционе
	const STATUS_AUTHOR = 6; //отправлен автору
	const STATUS_WORK = 7; //в работе у автора
	const STATUS_CORRECTION = 10; //на доработке у автора
	const STATUS_CANCEL = 11; //отказ заказчика от заказа
	const STATUS_CANCEL_AUTHOR = 12; //отказ автора от заказа
	const STATUS_DELAY = 13; //заказ просрочен
	const STATUS_CHECK = 14; //отправлен автором на проверку
	const STATUS_DRAFT = 14; //черновик
	const STATUS_FINISH = 16; //принят/завершен
	const STATUS_FINISH_AUTO = 17; //принят автоматически
	 
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{status_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_status', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ordersPlatforms' => array(self::HAS_MANY, 'OrdersPlatform', 'status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_status' => 'Name Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_status',$this->name_status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StatusOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
