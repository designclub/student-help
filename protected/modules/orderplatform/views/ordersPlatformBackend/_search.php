<?php
/**
 * Отображение для _search:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'action'      => Yii::app()->createUrl($this->route),
        'method'      => 'get',
        'type'        => 'vertical',
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

<fieldset>
    <div class="row">
        <div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'id', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('id'),
                        'data-content' => $model->getAttributeDescription('id')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'create_date', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('create_date'),
                        'data-content' => $model->getAttributeDescription('create_date')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'update_date', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('update_date'),
                        'data-content' => $model->getAttributeDescription('update_date')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'target_date', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('target_date'),
                        'data-content' => $model->getAttributeDescription('target_date')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'title', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('title'),
                        'data-content' => $model->getAttributeDescription('title')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'theme', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('theme'),
                        'data-content' => $model->getAttributeDescription('theme')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textAreaGroup($model, 'text', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('text'),
                    'data-content' => $model->getAttributeDescription('text')
                ]
            ]]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'budget', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('budget'),
                        'data-content' => $model->getAttributeDescription('budget')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'cost', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('cost'),
                        'data-content' => $model->getAttributeDescription('cost')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->textFieldGroup($model, 'file', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('file'),
                        'data-content' => $model->getAttributeDescription('file')
                    ]
                ]
            ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->dropDownListGroup($model, 'type_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(TypesOrder::model()->findAll(), 'id', 'name_type')
                    ]
                ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->dropDownListGroup($model, 'subject_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(SubjectOrder::model()->findAll(), 'id', 'name_subject')
                    ]
                ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->dropDownListGroup($model, 'status_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(StatusOrder::model()->findAll(), 'id', 'name_status')
                    ]
                ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->dropDownListGroup($model, 'client_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(User::model()->findAll(), 'id', 'first_name')
                    ]
                ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->dropDownListGroup($model, 'author_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(User::model()->findAll(), 'id', 'first_name')
                    ]
                ]); ?>
        </div>
		<div class="col-sm-3">
            <?=  $form->dropDownListGroup($model, 'dialog_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(DialogOrderUsers::model()->findAll(), 'id', 'id')
                    ]
                ]); ?>
        </div>
		    </div>
</fieldset>

    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'context'     => 'primary',
            'encodeLabel' => false,
            'buttonType'  => 'submit',
            'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t('OrderplatformModule.orderplatform', 'Искать 1'),
        ]
    ); ?>

<?php $this->endWidget(); ?>