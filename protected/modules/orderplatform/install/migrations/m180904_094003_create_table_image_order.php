<?php

class m180904_094003_create_table_image_order extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->createTable('{{orders_platform_file}}', [
            'id'                 => 'pk',
            'orders_platform_id' => 'integer',
            'fileName'           => 'string',
            'name'               => 'string',
            'alt'                => 'string',
        ], $this->getOptions());

        $this->addForeignKey(
            'fk_{{orders_platform_file}}_orders_platform_id',
            '{{orders_platform_file}}',
            'orders_platform_id',
            '{{orders_platform}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_{{orders_platform_file}}_orders_platform_id', '{{orders_platform_file}}');
        $this->dropTable('{{orders_platform_file}}');
    }
}