<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model OrdersPlatform
 *   @var $form TbActiveForm
 *   @var $this OrdersPlatformBackendController
 **/
$form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm', [
		'id' => 'orders-solution-form',
		'enableAjaxValidation' => false,
		'enableClientValidation' => true,
		'htmlOptions' => [ 'class' => 'well', 'enctype' => 'multipart/form-data' ],
	]
);
?>

<?= $form->errorSummary($model) ?>

<div class="row">
	<div class="col-sm-12">
		<?=  $form->textAreaGroup($model, 'comment', [
            'widgetOptions' => [
                'htmlOptions' => [
                    'class' => 'popover-help',
                    'rows' => 6,
                    'cols' => 50,
                    'data-original-title' => $model->getAttributeLabel('comment'),
                    'data-content' => $model->getAttributeDescription('comment')
                ]
            ]]); ?>
	</div>
</div>


<div class='row'>
	<?php if (count($model->filesOrders()) > 0): ?>
		<?php foreach($model->filesOrders() as $file): ?>
		<div class="col-sm-6">
			<?= CHtml::link($file->title_file, Yii::app()->getModule('orderplatform')->getUploadPath().'/'.$file->name_file, ['target' => '_blank']); ?>
		</div>
		<div class="col-sm-2">
			<?= CHtml::link('<span class="icon-cross"></span>', Yii::app()->createUrl('/orderplatform/ordersPlatformBackend/deleteFile', ['id'=>$file->id]), ['class'=>'btn delete-file', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Удалить файл"]); ?>
		</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>

<div class='row'>
	<div class="col-sm-12">
		<?= $form->fileFieldGroup(
                $model,
                'files[]',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'multiple' => true
                        ],
                    ],
                ]
            ); ?>
		<p>Загрузить можно не более 5-ти файлов</p>

	</div>
</div>

<?php
$this->widget(
	'bootstrap.widgets.TbButton', [
		'buttonType' => 'submit',
		'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index', 'class' => 'btn btn-success' ],
		'label' => Yii::t( 'OrderplatformModule.orderplatform', 'Выполить заказ и отправить на проверку заказчику' ),
	]
);
?>

<?php $this->endWidget(); ?>