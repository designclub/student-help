<?php
/**
 * Отображение для GalleryWidget/gallerywidget:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$url = Yii::app()->getTheme()->getAssetsUrl();

Yii::app()->getClientScript()->registerScriptFile($url . '/js/owl.carousel.min.js', CClientScript::POS_END);

?>

<?php
	$this->widget('application.modules.gallery.widgets.OwlCarouselNewsWidget', array(
				'id'=>'carousel-teachers', 
				'options' => [
					'items'					=> 1,
					'loop'					=> true,	
					'autoplay'				=> true,
					'autoplaySpeed'			=> 1300,
					'autoplayTimeout'	=> 3000,
					'autoplayHoverPause'	=> true,
					'smartSpeed'			=> 350,
					'fluidSpeed'			=> 150,
					'dragEndSpeed'			=> 150,
					'animateOut'			=> 'fadeOut',
					'animateIn'				=> 'fadeIn',
					'nav'					=> true,
					'navText'				=> ["<i class='fa fa-angle-left' aria-hidden='true'></i>
	", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
					'dots'					=> false,
					'responsiveClass'		=> true,
				]
)); ?>
		
<div class="row carousel-teachers">
		<?php $this->widget(
			'bootstrap.widgets.TbListView',
			array(
				'dataProvider'  => $dataProvider,
				'itemView'      => '_teacher',
				'template'      => "{items}",
				'itemsCssClass' => 'owl-carousel owl-carousel-carousel-teachers',
				'itemsTagName'  => 'div',
				'emptyText'		=> 'Пока ничего не добавлено'
			)
		); ?>
</div>
